import express from 'express';
import CommentNodesController from '../controllers/CommentNodesController';

const router = express.Router();

router.post('/create', CommentNodesController.create);

router.get('/comments', CommentNodesController.nodeComments);

router.delete('/delete/:id', CommentNodesController.delete);

router.get('/actions-count/:id/:nodeId', CommentNodesController.actionsCount);


export default router;
