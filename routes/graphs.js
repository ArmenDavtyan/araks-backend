import express from 'express';
import GraphController from '../controllers/GraphController';
import accessTokenAuth from '../middlewares/accessTokenAuth';

const router = express.Router();

router.get('/', GraphController.getList);

router.get('/nodes', GraphController.getNodesList);

router.get('/single/:id', GraphController.getSingle);

router.get('/single/token/:id', accessTokenAuth, GraphController.getSingle);

router.get('/info/:id', GraphController.getInfo);

router.get('/embed/:id/:token', GraphController.getEmbed);

router.get('/actions-count/:id', GraphController.actionsCount);

router.post('/create', GraphController.create);

router.put('/update/:id', GraphController.update);

router.put('/update-positions/:graphId', GraphController.updatePositions);

router.patch('/thumbnail/:id', GraphController.setThumbnail);

router.delete('/delete/:id', GraphController.delete);

router.put('/update-data/:id', GraphController.updateData);

router.put('/compare/:id1/:id2', GraphController.compare);

router.post('/data/copy/:id', GraphController.copy);

router.post('/data/past-compare/:id', GraphController.pastCompare);

router.post('/data/past/:id', GraphController.past);

router.get('/getAllTabs/:id', GraphController.getAllTabsByGraphId);

export default router;
