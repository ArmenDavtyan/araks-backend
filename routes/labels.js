import express from 'express';
import LabelController from '../controllers/LabelController';

const router = express.Router();

router.post('/create/:graphId', LabelController.create);

router.put('/update/:graphId', LabelController.update);

router.put('/toggle/:graphId', LabelController.labelToggle);

router.put('/update-positions/:graphId', LabelController.updatePosition);  // deprecated

router.delete('/delete/:graphId', LabelController.delete);

router.get('/copy/:graphId/:labelId', LabelController.copy);

router.post('/past-compare/:graphId', LabelController.pastCompare);

router.post('/past/:graphId', LabelController.past);

router.get('/:graphId/:labelId', LabelController.getLabelNodes);

// router.post('/custom-fields/:graphId', NodeController.delete);

export default router;
