import express from 'express';
import GraphLabelsEmbedController from '../controllers/GraphLabelsEmbedController';

const router = express.Router();

router.get('/single/:labelId', GraphLabelsEmbedController.single);

router.post('/create', GraphLabelsEmbedController.create);

router.delete('/delete', GraphLabelsEmbedController.delete);

export default router;
