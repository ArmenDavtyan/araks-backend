import express from 'express';
import UserFriendsController from '../controllers/UserFriendsController';

const router = express.Router();

router.get('/', UserFriendsController.getList);

router.post('/add', UserFriendsController.addFriend);

router.put('/accept/:id', UserFriendsController.accept);

router.put('/reject/:id', UserFriendsController.reject);

router.put('/cancel/:id', UserFriendsController.cancel);

router.put('/remove/:id', UserFriendsController.remove);

router.get('/my-friends', UserFriendsController.myFriends);

export default router;
