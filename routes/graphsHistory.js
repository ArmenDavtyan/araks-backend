import express from 'express';
import GraphHistoryController from '../controllers/GraphHistoryController'; 

const router = express.Router();
 

router.get('/node-history/:graphsId/:nodeId', GraphHistoryController.searchGraphsByNode); 

router.get('/graph-history/:graphsId', GraphHistoryController.searchGraphHistory); 

export default router;
