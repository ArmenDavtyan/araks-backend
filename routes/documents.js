import express from 'express';
import DocumentController from '../controllers/DocumentController';

const router = express.Router();

router.get('/get-pictures-by-tag', DocumentController.findPictures);

router.get('/get-documents-by-tag', DocumentController.findDocuments);

router.get('/get-documents', DocumentController.getDocumentsByGraphId);

router.post('/copy-documents', DocumentController.copyDocumentForGraph);

export default router;
