import express from 'express';
import PublicController from '../controllers/PublicController';
import pdfUpload from '../middlewares/pdfUploader';
import DownloadController from '../controllers/DownloadController';

const router = express.Router();

router.get('/markers/:name', PublicController.marker);

router.get('/gravatar/:name', PublicController.gravatar);

router.get('/uploads/thumbnails/a/:userId/:thumbnail', PublicController.thumbnail);

router.get('/uploads/thumbnails/:userId/*', PublicController.thumbnailStatic);

export default router;
