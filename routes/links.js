import express from 'express';
import LinkController from '../controllers/LinkController';

const router = express.Router();

router.post('/create/:graphId', LinkController.create);

router.put('/update/:graphId', LinkController.update);

router.delete('/delete/:graphId', LinkController.delete);

export default router;
