import express from 'express';
import ShareController from '../controllers/ShareController';

const router = express.Router();


router.get('/', ShareController.getList);

router.post('/create', ShareController.create);

router.put('/update/:id', ShareController.update);

router.delete('/delete/:id', ShareController.delete);

router.get('/users', ShareController.getUsers);

router.post('/update-status', ShareController.updateStatus);

router.delete('/label-delete', ShareController.labelDelete);

export default router;
