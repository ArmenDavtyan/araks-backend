import express from 'express';
import multer from 'multer';
import NodeController from '../controllers/NodeController';

const router = express.Router();

const upload = multer({ storage: multer.memoryStorage() });

router.post('/create/:graphId', NodeController.create);

router.put('/update/:graphId', NodeController.update);

router.put('/update-positions/:graphId', NodeController.updatePositions); // deprecated

router.get('/get-fields/:graphId/:nodeId', NodeController.getCustomFields);

router.put('/update-fields/:graphId', NodeController.updateCustomFields);

router.delete('/delete/:graphId', NodeController.delete);

router.post('/upload/icon/:graphId', upload.single('icon'), NodeController.uploadIcon);

router.post('/upload/file/:graphId', upload.single('file'), NodeController.uploadFile);

export default router;
