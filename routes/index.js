import express from 'express';
import users from './users';
import graphs from './graphs';
import shareGraphs from './shareGraphs';
import documents from './documents';
import commentGraphs from './commentGraphs';
import notifications from './notifications';
import convert from './convert';
import publicRoute from './public';
import helpers from './helpers';
import userFriends from './userFriends';
import graphLabelsEmbed from './graphLabelsEmbed';
import commentNodes from './commentNodes';
import share from './share';
import nodes from './nodes';
import links from './links';
import labels from './labels';
import customFields from './customFields';
// import graphsHistory from './graphsHistory';

const router = express.Router();

router.get('/', (req, res, next) => {
  try {
    res.json({
      status: 'ok',
      test2: 'ok',
    });
  } catch (e) {
    next(e);
  }
});

router.use('/users', users);

router.use('/graphs', graphs);

router.use('/nodes', nodes);

router.use('/links', links);

router.use('/labels', labels);

router.use('/custom-fields', customFields);

router.use('/convert', convert);

router.use('/public', publicRoute);

router.use('/helpers', helpers);

router.use('/share-graphs', shareGraphs);

router.use('/comment-graphs', commentGraphs);

router.use('/notifications', notifications);

router.use('/user-friends', userFriends);

router.use('/graph-labels-embed', graphLabelsEmbed);

router.use('/comment-nodes', commentNodes);

router.use('/share', share);

router.use('/document', documents);

// router.use('/graph-history', graphsHistory);

export default router;
