import express from 'express';
import CustomFieldsController from '../controllers/CustomFieldsController';

const router = express.Router();

router.post('/create/:graphId', CustomFieldsController.create);

router.put('/update/:graphId', CustomFieldsController.update);

router.delete('/delete/:graphId', CustomFieldsController.delete);

export default router;
