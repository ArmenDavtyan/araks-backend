export const IMAGE_MIME_TYPES = {
  'image/png': 'png',
  'image/jpeg': 'jpg',
  'image/jpeg ': 'jpeg',
  'image/gif': 'gif',
  'image/svg+xml': 'svg',
};
