 
// import GraphsHistory from '../models/GraphsHistory'
// import Users from '../models/Users'
// import EventType from '../helpers/EventType';
// import _ from 'lodash';
// import moment from 'moment';
// // import GraphHistory from '../models/GraphsHistory';

// class GraphHistoryController {  
//   /**
//    * Insert data in index elasticsearch
//    * @param {*} data 
//    */
//   static insertData = async function(data){
//     try {
//       const result = await GraphsHistory.insertData(data);     
       
//     } catch (err) {
//       console.log(err, 'elasticerr')       
//     }
//   }
//   /**
//    * Select Node history data
//    * @param {*} req 
//    * @param {*} res 
//    * @param {*} next 
//    */
//   static searchGraphsByNode = async (req, res, next) => { 
//     let {graphsId, nodeId} = req.params;
       
//     try {
//       const result = await GraphsHistory.filterGraphsByNodeMade(graphsId, nodeId);      
//       const resultPositions = await GraphsHistory.filterGraphsByNodePositions(graphsId, nodeId);  
//       const resultTabsView = await GraphsHistory.filterGraphsByNodeTabsView(graphsId, nodeId);  
//       const countPositions = !_.isEmpty(resultPositions.body) ? resultPositions.body.count : ''     
//       const countTabsView = !_.isEmpty(resultTabsView.body) ? resultTabsView.body.count : ''     
//       const data = await Promise.all(result.body.hits.hits.map(async (graphs) =>{      
//       const  users =   await Users.getCustomDataById(graphs._source.userId );
      
//         return {
//          id: graphs._id, 
//           //graphsid: graphs._source.graphsId,
//           userId: graphs._source.userId,
//           user: users,
//           eventDate: graphs._source.eventDate, 
//           eventType: EventType.getTypeShortName(graphs._source.eventType),
//           name: graphs._source.data.name,
//           type: graphs._source.data.type, 
//           //data: graphs._source.data,
//         }
//       }));  
//         res.json({ status_code: 200, success: true, singleNodeHistory: {data, countPositions, countTabsView} , message: "Filter Graphs by graphs and node  made data fetched successfully" });
//     } catch (err) {
//       console.log(err, 'elasticerr')
//        res.json({ status_code: 500, success: false, singleNodeHistory: [data = [], countPositions= [], countTabsView= []],  message: err});
//     }
//   } 

//   static searchGraphHistory = async (req, res, next) => { 
//     let { graphsId } = req.params;
       
//     try {
//       const result = await GraphsHistory.search(graphsId);        
//       const data = await Promise.all(result.body.hits.hits.map(async (graphs) =>{ 
//       const  users =   await Users.getCustomDataById(graphs._source.userId );
//       const  event =   EventType.getTypeName(graphs._source.eventType, graphs._source.dataDetails);
      


//         return {
//             id: graphs._id, 
//       //    // graphsid: graphs._source.graphsId,
//       //   //  userId: graphs._source.userId,
//       //  //   user: users,
//       //     title: graphs._source.eventDate ? moment.unix(graphs._source.eventDate).format("DD/MM/YYYY") : '', 
//       //    // name: graphs._source.data[0].name,
//       //    //cardTitle: EventType.getTypeName(graphs._source.eventType, graphs._source.dataDetails), 
//       //    contentTitle: EventType.getTypeName(graphs._source.eventType, graphs._source.dataDetails), 
//       //    cardSubtitle: `${users.firstName} ${users.firstName}`,
//       //   //  contentText:  `${users.firstName} ${users.firstName}`,
//       //    contentDetailedText:
//       //        ` ${moment.unix(graphs._source.eventDate).format("MMM D, YYYY, HH:mmA")}`,

//       //    cardDetailedText: [`${moment.unix(graphs._source.eventDate).format("MMM D, YYYY, HH:mmA")}`, "V"], 

//        eventDate: graphs._source.eventDate ? moment.unix(graphs._source.eventDate).format("DD/MM/YYYY") : '',
//        cardTitle: `${users.firstName} ${users.lastName} ${event}`,
//        cardSubtitle: `${moment.unix(graphs._source.eventDate).format("MMM D, YYYY, HH:mmA")}`,
//        cardDetailedText: ' ', 

//         } 
//       })); 
      
//       const dataGroup  = GraphHistory.groupByEventDate(data); 
      


//         res.json({ status_code: 200, success: true, data : dataGroup , message: "Filter Graphs by graphs  made data fetched successfully" });
//     } catch (err) {
//       console.log(err, 'elasticerr')
//        res.json({ status_code: 500, success: false, data,  message: err});
//     }
//   }
// }

// export default GraphHistoryController;
