import HttpErrors from 'http-errors';
import Promise from 'bluebird';
import moment from 'moment';
import _ from 'lodash';
import validate from '../services/validate';
import { Graphs } from '../models';
import Utils from '../services/Utils';
import Socket from '../services/Socket';
import nodes from '../routes/nodes';

class NodeController {
  static create = async (req, res, next) => {
    try {
      res.json({
        status: 'ok',
      });
    } catch (e) {
      next(e);
    }
  };

  static update = async (req, res, next) => {
    try {
      await validate(req.body, {
        customFields: 'object',
      });
      const { userId } = req;
      const { graphId } = req.params;
      const { customFields, files } = req.body;
      const graph = await Graphs.findUserAssociatedGraph(graphId, userId, {
        full: true,
        userRole: ['edit', 'admin', 'edit_inside'],
      });
      if (!graph) {
        throw HttpErrors(403);
      }

      // const uploadedFiles = Utils.uploadNodeFiles(files, graph.id);
      //
      // customFields.forEach((customField) => {
      //   const {
      //     nodeType, fieldName, order = 0, nodeId,
      //   } = customField;
      //   const value = Utils.template(customField.value || '', uploadedFiles);
      //   _.set(graph.customFields, [nodeType, fieldName, 'values', nodeId], value);
      //   _.set(graph.customFields, [nodeType, fieldName, 'order'], order);
      // });

      // await Graphs.update({
      //   customFields: graph.customFields,
      // }, {
      //   where: {
      //     id: graphId,
      //   },
      // });

      res.json({
        status: 'ok',
        a: Utils.differenceNested(customFields, graph.customFields),
        b: Utils.differenceNested(graph.customFields, customFields),
      });
    } catch (e) {
      next(e);
    }
  };

  static delete = async (req, res, next) => {
    try {
      res.json({
        status: 'ok',
      });
    } catch (e) {
      next(e);
    }
  };
}

export default NodeController;
