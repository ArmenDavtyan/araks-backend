import HttpErrors from 'http-errors';
import { literal, Op } from 'sequelize';
import SqlString from 'sequelize/lib/sql-string';
import validate from '../services/validate';
import {
  Graphs, Share, Notifications, Users, GraphMeta,
} from '../models';
import Mail from '../services/Mail';
import helpers from '../helpers';
import Socket from '../services/Socket';

class ShareGraphsController {
  static create = async (req, res, next) => {
    try {
      const { graphId, userId } = req.body;

      const graph = await Graphs.findByPk(graphId);

      if (!graph) {
        throw HttpErrors(404);
      }

      let shareGraph = await Share.findOne({ where: { graphId, userId } });

      if (shareGraph) {
        shareGraph.status = 'shared';
        shareGraph.role = 'view';
        shareGraph.objectId = null;
        shareGraph.type = 'graph';
        await shareGraph.save();
      } else {
        shareGraph = await Share.create({
          userId,
          graphId,
        });
      }

      const data = await Share.getListData(graphId);
      res.json(data);
    } catch (e) {
      next(e);
    }
  };

  static update = async (req, res, next) => {
    try {
      await validate(req.body, {
        role: 'required|string',
      });
      const {
        emailTexts: { permissionUpdateSubject, permissionUpdateText },
        common: { getUserFullName },
      } = helpers;
      const {
        role,
      } = req.body;
      const { id } = req.params;
      const { userId: sharerId } = req;

      await Share.update({
        role,
      }, {
        where: {
          id,
        },
      });

      const shareGraph = await Share.findByPk(id, {
        include: [
          {
            model: Graphs,
            as: 'graph',
          },
          {
            model: Users,
            as: 'user',
          },
        ],
      });

      if (!shareGraph) {
        throw HttpErrors(404);
      }
       
      Socket.emitAll('shareList',   { graphId : shareGraph.graphId }); 

      /*if (shareGraph.status !== 'new') { 
        const { user } = shareGraph;
        const userShare = await Users.findByPk(userId) 
        
        await Notifications.create({
          graphId: shareGraph.graphId,
          shareGraphId: shareGraph.id,
          actionType: 'share-update',
          userId: shareGraph.userId,
          text: `<img class="avatar" src=${userShare.avatar} alt=${userShare.firstName} />
           <p><strong><a href="/profile/${userShare.id}" > ${userShare.firstName} ${userShare.lastName} </a></strong> has changed your role in the  graph <strong><a href="/graphs/view/${shareGraph.graphId}">${shareGraph.graph.title}</a> </strong>  to <strong>"${shareGraph.role}"</strong></p>`,          
        });

        Socket.emitAll(
          `notificationsListGraphShared-${shareGraph.userId}`,
          {
            graphId: shareGraph.graphId,
            shareGraphId: shareGraph.id,
            actionType: 'share-update',
            userId: shareGraph.userId,
            text: `<img class="avatar" src=${userShare.avatar} alt=${userShare.firstName} /> 
            <p><strong><a href="/profile/${userShare.id}" > ${userShare.firstName} ${userShare.lastName} </a></strong> has changed your role in the  graph <strong><a href="/graphs/view/${shareGraph.graphId}">${shareGraph.graph.title}</a> </strong>  to <strong>"${shareGraph.role}"</strong></p>`,
          },
        );

        const data = await Share.getListData(shareGraph.graphId);

         const sharer = await Users.findByPk(sharerId);

        await Mail.send(
          user.email,
          permissionUpdateSubject(getUserFullName(sharer)),
          permissionUpdateText(
            getUserFullName(sharer), getUserFullName(user), data.shareGraphs[0].graphId, `${req.protocol}://${req.get('host')}`,
          ),
        );
      }*/

      const data = await Share.getListData(id);
      res.json(data);
    } catch (e) {
      next(e);
    }
  };

  // static updateStatus = async (req, res, next) => {
  //   try {
  //     await validate(req.body, {
  //       graphId: 'required|integer',
  //     });
  //     const {
  //       emailTexts: { permissionCreateSubject, permissionCreateText },
  //       common: { getUserFullName },
  //     } = helpers;

  //     const {
  //       graphId,
  //     } = req.body;
  //     const { userId: sharerId } = req;

  //     const shareGraphsList = await Share.findAll({
  //       where: {
  //         status: 'new',
  //         graphId,
  //       },
  //       include: [
  //         {
  //           model: Graphs,
  //           as: 'graph',
  //         },
  //         {
  //           model: Users,
  //           as: 'user',
  //         },
  //       ],
  //     });

  //     await Share.update({
  //       status: 'shared',
  //     }, {
  //       where: {
  //         status: 'new',
  //         graphId,
  //       },
  //     });

  //     const user = await Users.findByPk(userId);

  //     const first = user.firstName;

  //     const avatar = user.avatar;

  //     const last = user.lastName;

  //     if (shareGraphsList) {
  //       shareGraphsList.forEach(async (item) => {

  //         await Notifications.create({
  //           graphId,
  //           shareGraphId: item.id,
  //           actionType: 'share-add',
  //           userId: item.userId,
  //           text: `<img class="avatar" src=${avatar} alt=${first} /><span class="notificationUserName">${first} ${last}</span>
  //           <span class="notificationContent"> Shared with you a graph </span>
  //           <span class="notificationContent"> Access type - ${item.role} </span>
  //           <span class="notificationFooter"> <a href="/graphs/view/${graphId}">${item.graph.title}</a> </span>`,
  //         });

  //         Socket.emitAll(
  //           `notificationsListGraphShared-${item.userId}`,
  //           {
  //             graphId,
  //             shareGraphId: item.id,
  //             actionType: 'share-add',
  //             userId: item.userId,
  //             text: `<img class="avatar" src=${avatar} alt=${first} /><span class="notificationUserName">${first} ${last}</span>
  //             <span class="notificationContent"> Shared with you a graph </span>
  //             <span class="notificationContent"> Access type - ${item.role} </span>
  //             <span class="notificationFooter"> <a href="/graphs/view/${graphId}">${item.graph.title}</a> </span>`,
  //           },
  //         );

  //         const { user } = item;
  //         const sharer = await Users.findByPk(sharerId);

  //         await Mail.send(
  //           user.email,
  //           permissionCreateSubject(getUserFullName(sharer)),
  //           permissionCreateText(getUserFullName(sharer), getUserFullName(user), graphId),
  //         );
  //       });
  //     }
  //     const data = await Share.getListData(graphId);
  //     res.json(data);
  //   } catch (e) {
  //     next(e);
  //   }
  // };

  static delete = async (req, res, next) => {
    try {
      const {
        emailTexts: { permissionDeleteSubject, permissionDeleteText },
        common: { getUserFullName },
      } = helpers;
      const { id } = req.params;
      const { page = 1 } = req.body;
      const { userId: sharerId} = req; 
      const {notification} = req.query;
      
      const shareGraph = await Share.findByPk(id, {
        include: [{
          model: Graphs,
          as: 'graph',
        }],
      });

      if (!shareGraph) {
        throw HttpErrors(404);
      }
      const { graphId, userId, graph: { title } } = shareGraph;

      if (shareGraph.status === 'new') {
        await shareGraph.destroy();
      } else {
        await shareGraph.update({
          status: 'deleted',
        }, {
          where: {
            id,
          },
        });
       
        if(notification === 'true' ) {  
          await Notifications.create({
            graphId,
            actionType: 'share-delete',
            userId,
            text: `Your permissions were removed from graph ${title}`,
          });
  
          Socket.emitAll(
            `notificationsListGraphShared-${userId}`,
            {
              graphId,
              actionType: 'share-delete',
              userId,
              text: `Your permissions were removed from graph ${title}`,
            },
          );
          const user = await Users.findByPk(userId);
          const sharer = await Users.findByPk(sharerId);
  
          await Mail.send(
            user.email,
            permissionDeleteSubject(getUserFullName(sharer)),
            permissionDeleteText(getUserFullName(sharer), getUserFullName(user), graphId),
          );
        }


      }

      const data = await Share.getListData(graphId, page);
      res.json(data);
    } catch (e) {
      next(e);
    }
  };

  static getList = async (req, res, next) => {
    try {
      await validate(req.body, {
        page: 'integer',
        graphId: 'required|integer',
      });
      const { page = 1, graphId } = req.body;
      const data = await Share.getListData(graphId, page);
      res.json(data);
    } catch (e) {
      next(e);
    }
  };

  static userGraphs = async (req, res, next) => {
    try {
      const { userId } = req;

      const userGraphs = await Share.findAll({
        where: {
          userId,
          status: 'shared',
        },
        include: [
          {
            model: Users,
            as: 'user',
          },
          {
            model: Graphs,
            as: 'graph',
            include: [
              {
                model: Users,
                as: 'user',
              },
            ],
          },
        ],
        order: [
          ['updatedAt', 'DESC'],
        ],
      });

      res.json({
        status: 'ok',
        userGraphs,
      });
    } catch (e) {
      next(e);
    }
  }

  static graphUsers = async (req, res, next) => {
    try {
      const {
        common: { getUserFullName, getUserId},
      } = helpers;
      const { graphId } = req.body;

      const sharedGraphs = await Share.findAll({
        where: {
          graphId,
          status: 'shared',
        },
        include: [
          {
            model: Users,
            as: 'user',
          },
        ],
        order: [
          ['updatedAt', 'DESC'],
        ],
      });
      //const graphUsers = sharedGraphs.map((item) => getUserFullName(item.user));
      const graphUsers = sharedGraphs.map((item) => [item.role, item.user]);       
      //const graphUsers = sharedGraphs.map((item) => item.user);  

      res.json({
        status: 'ok',
        result: {
          [graphId]: sharedGraphs, 
        },
      });
    } catch (e) {
      next(e);
    }
  }

  static searchInMyList = async (req, res, next) => {
    try {
      await validate(req.query, {
        page: 'integer',
        s: 'required|string',
      });

      const { userId } = req;
      const { page = 1, status = 'active', s } = req.query;
      const limit = 15;
      const offset = (page - 1) * limit;

      const search = SqlString.escape(`%${s}%`);
      const shareGraphs = await Share.findAll({
        where: {
          userId,          
          [Op.not]: { status: 'deleted' },
        },
        include: [{
          model: Graphs,
          as: 'graph',
          include: [{
            model: Users,
            as: 'user',
          }],
          where: {
            status,
            $and: [{
              $or: [
                { title: { $like: `%${s}%` } },
                { description: { $like: `%${s}%` } },
                { $or: literal(`nodes->"$[*].name" COLLATE utf8mb4_GENERAL_CI like  ${search}`) },
                { $or: literal(`nodes->"$[*].type" COLLATE utf8mb4_GENERAL_CI like  ${search}`) },
                { $or: literal(`links->"$[*].type" COLLATE utf8mb4_GENERAL_CI like  ${search}`) },
              ],
            }],
          },
          order: [
            ['updatedAt', 'DESC'],
          ],
          attributes: {
            exclude: ['nodes', 'links', 'labels', 'customFields'],
            include: [
              [literal('JSON_LENGTH(nodes)'), 'nodesCount'],
              [literal('JSON_LENGTH(links)'), 'linksCount'],
              [literal('JSON_LENGTH(labels)'), 'labelsCount'],
            ],
          },
        }],
        limit,
        offset,
      });

      const total = await Graphs.count({ where: { userId } });

      const totalPages = Math.ceil(total / limit);

      res.json({
        status: 'ok',
        shareGraphs,
        page,
        total,
        totalPages,
      });
    } catch (e) {
      next(e);
    }
  }
}

export default ShareGraphsController;
