import HttpErrors from 'http-errors';
import validate from '../services/validate';
import {
  GraphLabelsEmbed, Graphs, ShareGraphs, Users,
} from '../models';
import Socket from '../services/Socket';

class GraphLabelsEmbedController {
  static create = async (req, res, next) => {
    try {
      await validate(req.body, {
        graphId: 'required|numeric',
        sourceId: 'required|numeric',
        labelId: 'required|string',
      });
      const { userId } = req;
      const { sourceId, labelId, graphId } = req.body;

      if (+sourceId === +graphId) {
        throw HttpErrors(422, 'Can\'t past label to this graph');
      }

      const graph = await Graphs.findByPk(sourceId);

      if (!graph) {
        throw HttpErrors(404);
      }

      const { userId: ownerId } = graph;

      let [labelEmbed] = await GraphLabelsEmbed.findOrCreate({
        where: {
          graphId,
          sourceId,
          ownerId,
          userId,
          labelId,
        },
        defaults: {
          graphId,
          sourceId,
          ownerId,
          userId,
          labelId,
        },
      });

      Socket.emit(ownerId, 'labelEmbedCopy', labelEmbed, sourceId);

      const sharedGraphs = await ShareGraphs.findAll({
        where: {
          graphId: sourceId,
          status: 'shared',
        },
      });
      sharedGraphs.forEach((d) => {
        Socket.emit(d.userId, 'labelEmbedCopy', labelEmbed, sourceId);
      });

      labelEmbed = await GraphLabelsEmbed.findWithGraph({
        where: {
          userId,
          labelId,
        },
      });

      res.json({
        status: 'ok',
        labelEmbed,
      });
    } catch (e) {
      next(e);
    }
  };

  static single = async (req, res, next) => {
    try {
      const { userId } = req;
      const { labelId } = req.params;
      const labelEmbed = await GraphLabelsEmbed.findWithGraph({
        where: {
          id: labelId,
          userId,
        },
      });
      if (!labelEmbed) {
        throw HttpErrors(404);
      }

      res.json({
        status: 'ok',
        labelEmbed,
      });
    } catch (e) {
      next(e);
    }
  };

  static delete = async (req, res, next) => {
    try {
      await validate(req.query, {
        graphId: 'required|numeric',
        sourceId: 'required|numeric',
        labelId: 'required|string',
      });
      const { userId } = req;
      const { sourceId, labelId, graphId } = req.query;

      const graph = await Graphs.findUserAssociatedGraph(graphId, userId);

      if (graph) {
        graph.labels = [...graph.labels].filter((l) => l.id !== labelId);
        await Graphs.update({
          labels: graph.labels,
        }, {
          where: {
            id: graphId,
          },
        });

        await GraphLabelsEmbed.destroy({
          where: {
            sourceId,
            labelId,
            graphId,
          },
        });

        await Socket.emitSharedUsers(graphId, graph.userId, userId, 'label.delete', {
          graphId,
          labels: [{
            id: labelId,
            sourceId,
          }],
          userId,
        });
      }

      res.json({
        status: 'ok',
      });
    } catch (e) {
      next(e);
    }
  };
}

export default GraphLabelsEmbedController;
