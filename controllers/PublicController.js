import fs from 'fs';
import path from 'path';
import axios from 'axios';
import md5 from 'md5';
import { v4 as uuidv4 } from 'uuid';
import HttpErrors from 'http-errors';
import Utils from '../services/Utils';
import Converter from '../services/Converter';
import { AccessTokens } from '../models';

class PublicController {
  static marker = async (req, res, next) => {
    try {
      const { name } = req.params;
      const colorHex = name.replace('.svg', '');
      const color = Utils.hexToRgb(colorHex);
      const colorDark = `rgb(${Utils.hexToRgb(colorHex, true).map((c) => c - (c * 0.2))
        .join(',')})`;
      const svgDir = path.join(__dirname, '../public/marker.svg');
      let svg = fs.readFileSync(svgDir, 'utf-8');
      svg = svg.replace('#fe5857', color);
      svg = svg.replace('#db0253', colorDark);
      res.setHeader('Content-Disposition', `attachment; filename=${colorHex}.svg`);
      res.setHeader('Content-type', 'image/svg+xml');
      res.send(svg);
    } catch (e) {
      next(e);
    }
  };

  static gravatar = async (req, res, next) => {
    try {
      const { name } = req.params;
      const email = decodeURIComponent(name).replace(/\.png$/, '');
      const { data } = await axios.get(`https://www.gravatar.com/avatar/${md5(email)}?s=256&d=identicon`, {
        responseType: 'arraybuffer',
      });
      res.setHeader('Content-type', 'image/png');
      res.send(data);
    } catch (e) {
      next(e);
    }
  };

  static thumbnail = async (req, res, next) => {
    try {
      const { userId, thumbnail } = req.params;
      const { referer = 'https://graphs.analysed.ai/' } = req.headers;
      const graphId = Utils.getGraphIdFromThumbnail(thumbnail);
      const accessToken = await AccessTokens.create({
        userId,
        graphId,
        token: uuidv4(),
      });
      const thumbnailPath = Utils.getGraphThumbnail(graphId, userId, 'path');
      console.log(`${referer}graphs/thumbnail/${graphId}/${userId}?token=${accessToken.token}`, 222222222);

      if (!fs.existsSync(thumbnailPath)) {
        const url = `${referer}graphs/thumbnail/${graphId}/${userId}?token=${accessToken.token}`;
        const file = await Converter.screenshot(url, 'file', {
          scaleFactor: 0.9,
          waitForElement: '#graph .nodes .node',
        });
        const dir = path.dirname(thumbnailPath);
        if (!fs.existsSync(dir)) {
          fs.mkdirSync(dir, { recursive: true });
        }
        fs.renameSync(file, thumbnailPath);
      }

      await accessToken.destroy();
      res.setHeader('Cache-Control', 'public, max-age=7200');
      res.sendFile(thumbnailPath);
    } catch (e) {
      next(e);
    }
  };

  static thumbnailStatic = async (req, res, next) => {
    try {
      let file = path.join(__dirname, '../', req.originalUrl);
      if (!fs.existsSync(file)) {
        const fileSmall = file.replace(/\.large$/, '');
        if (file.endsWith('.large') && fs.existsSync(fileSmall)) {
          file = fileSmall;
        } else {
          file = path.join(__dirname, '../public/no-thumbnail.jpg');
        }
      }
      res.setHeader('Cache-Control', 'public, max-age=7200');
      res.sendFile(file);
    } catch (e) {
      next(e);
    }
  };
}

export default PublicController;
