import HttpErrors from 'http-errors';
import {
  Graphs, CommentNodes, Users,
} from '../models';
import helpers from '../helpers';

class CommentNodesController {
  static create = async (req, res, next) => {
    try {
      const { graphId, nodeId, text, parentId } = req.body;
      const { userId } = req;
      const graph = await Graphs.findByPk(graphId);

      if (!graph) {
        throw HttpErrors(404);
      }
      
      const comment = await CommentNodes.create({
        userId,
        graphId: +graphId,
        nodeId,
        text,
        parentId,
      });
      
      const nodeComments = await CommentNodes.findByPk(comment.id, {
        include: [
          {
            model: Users,
            as: 'user',
          },
          {
            model: CommentNodes,
            as: 'parent',
          },
        ],
      });

      res.json({
        status: 'ok',
        nodeComments,
      });
    } catch (e) {
      next(e);
    }
  };

  static nodeComments = async (req, res, next) => {
    try {
      const {
        common: { listToTree },
      } = helpers;
      const { graphId, nodeId } = req.query;
      const nodeComments = await CommentNodes.findAll({
        where: {
          graphId,
          nodeId
        },
        include: [
          {
            model: Users,
            as: 'user',
          },
          {
            model: CommentNodes,
            as: 'parent',
            include: [
              {
                model: Users,
                as: 'user',
              },
            ],
          },
        ],
        order: [
          ['updatedAt', 'DESC'],
        ],
      });

      const comments = nodeComments.map((comment) => comment.toJSON()).reverse();

      res.json({
        status: 'ok',
        nodeComments: listToTree(comments),
      });
    } catch (e) {
      next(e);
    }
  };

  static delete = async (req, res, next) => { 
    try {
      const { id } = req.params;

      await CommentNodes.destroy({
        where: {
          $or: [
            { id },
            { parentId: id },
          ],
        },
      });

      res.json({
        status: 'ok',
        id,
      });
    } catch (e) {
      next(e);
    }
  };

  static actionsCount = async (req, res, next) => {
    try {
      const { id, nodeId } = req.params;
      const { userId } = req;
      console.log(id, nodeId, 'commentsCount');

      const graph = await Graphs.findOne({
        where: {
          id,
        },
      });

      if (!graph) {
        throw HttpErrors(404);
      }
      const commentsCount = await CommentNodes.count({
        where: {
          graphId: id,
          nodeId: nodeId
        },
      });
      res.json({
        status: 'ok',
        result: {
          commentsCount: commentsCount,
        },
      });
    } catch (e) {
      next(e);
    }
  };

}

export default CommentNodesController;
