import HttpErrors from 'http-errors';
import {
  UserFriends, Users, Notifications,
} from '../models';
import Socket from '../services/Socket';

class UserFriendsController {
  static getList = async (req, res, next) => {
    try {
      let { userId } = req.query;

      if (!userId) {
        userId = req.userId;
      }

      const friendsList = await UserFriends.findAll({
        where: {
          $or: [
            { senderUserId: userId },
            { receiverUserId: userId },
          ],
          status: {
            $not: [
              'rejected',
            ],
          },
        },
        include: [
          {
            model: Users,
            as: 'senderUser',
          },
          {
            model: Users,
            as: 'receiverUser',
          },
        ],
      });

      res.json({
        status: 'ok',
        friendsList,
      });
    } catch (e) {
      next(e);
    }
  };

  static addFriend = async (req, res, next) => {
    try {
      const { userId: senderUserId } = req;

      const { receiverUserId } = req.body;

      await UserFriends.create({
        senderUserId,
        receiverUserId,
      });

      const user = await Users.findByPk(senderUserId);
     
      await Notifications.notify(res, {
        userId: receiverUserId,
        actionType: 'add-friend',
        text: `You get new friend request from <a href="/profile/${senderUserId}">${`${user.firstName} ${user.lastName}`}</a>`,
      }, receiverUserId); 
      
      const friends = await UserFriends.getListData(senderUserId);

      const receiverFriends = await UserFriends.getListData(receiverUserId);
      Socket.emitAll(
        `updateUserfriend-${receiverUserId}`,
        receiverFriends,
      );

      res.json({ status: 'ok', data: friends });
    } catch (e) {
      next(e);
    }
  };

  static accept = async (req, res, next) => {
    try {
      const { userId: receiverUserId } = req;
      const { id } = req.params;

      const friend = await UserFriends.findOne({
        where: {
          id,
          receiverUserId,
          status: 'pending',
        },
      });

      if (!friend) {
        throw HttpErrors(404);
      }

      friend.status = 'accepted';
      await friend.save();

      const friends = await UserFriends.getListData(receiverUserId);

      const senderFriends = await UserFriends.getListData(friend.senderUserId);
      Socket.emitAll(
        `updateUserfriend-${friend.senderUserId}`,
        senderFriends,
      );

      res.json({ status: 'ok', data: friends });
    } catch (e) {
      next(e);
    }
  };

  static reject = async (req, res, next) => {
    try {
      const { userId: receiverUserId } = req;
      const { id } = req.params;

      const friend = await UserFriends.findOne({
        where: {
          id,
          receiverUserId,
          status: 'pending',
        },
      });

      if (!friend) {
        throw HttpErrors(404);
      }

      friend.status = 'rejected';
      await friend.save();

      res.json({ status: 'ok' });
    } catch (e) {
      next(e);
    }
  };

  static cancel = async (req, res, next) => {
    try {
      const { userId: receiverUserId } = req;
      const { id } = req.params;

      const friend = await UserFriends.findOne({
        where: {
          id,
          receiverUserId,
          status: 'pending',
        },
      });
     

      if (!friend) {
        throw HttpErrors(404);
      }

      await friend.destroy();

      res.json({ status: 'ok' });
    } catch (e) {
      next(e);
    }
  };

  static remove = async (req, res, next) => {
    try {
      const { userId } = req;
      const { id } = req.params;
      let notifyUserId;

      const friend = await UserFriends.findOne({
        where: {
          id,
          $or: [
            {
              receiverUserId: userId,
            },
            {
              senderUserId: userId,
            },
          ],
        },
      });

      if (!friend) {
        throw HttpErrors(404);
      }

      notifyUserId = friend.receiverUserId;
      if (friend.receiverUserId === userId) {
        notifyUserId = friend.senderUserId;
      }

      await friend.destroy();
      const friends = await UserFriends.getListData(userId);

      const senderFriends = await UserFriends.getListData(notifyUserId);
      Socket.emitAll(
        `updateUserfriend-${notifyUserId}`,
        senderFriends,
      );

      res.json({ status: 'ok', data: friends });
    } catch (e) {
      next(e);
    }
  };

  static myFriends = async (req, res, next) => {
    try {
      const { userId } = req;
      const friends = await UserFriends.getListData(userId);

      res.json({ status: 'ok', data: friends });
    } catch (e) {
      next(e);
    }
  }
}

export default UserFriendsController;
