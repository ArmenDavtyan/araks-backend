import moment from 'moment';
import _ from 'lodash';
import path from 'path';
import axios from 'axios';
import { v4 as uuidv4 } from 'uuid';
import Zip from 'node-zip';
import AdmZip from 'adm-zip';
import HttpError from 'http-errors';
import Promise from 'bluebird';
import fs from 'fs';
import { JSDOM } from 'jsdom';
import validate from '../services/validate';
import Csv from '../services/Csv';
import Converter from '../services/Converter';
import Utils from '../services/Utils';
import Directories from '../services/Directories';
import { Graphs } from '../models';

class DownloadController {
  // deprecated
  static nodesToCsv = async (req, res, next) => {
    try {
      const now = moment()
        .format('YY-MM-DDThh-mm-ss');
      await validate(req.body, {
        nodes: 'required|array',
        'nodes.*.fx': 'required|numeric',
        'nodes.*.fy': 'required|numeric',
        'nodes.*.name': 'required',
        'nodes.*.type': 'required',
      });
      const { nodes } = req.body;

      const nodesArr = nodes.map((d) => Csv.nodesObjToArray(d));
      nodesArr.unshift(Csv.nodesHeader());

      const csv = Csv.stringify(nodesArr);
      if (req.return) {
        return csv;
      }

      res.setHeader('Access-Control-Expose-Headers', 'content-disposition');
      res.setHeader('Content-Disposition', `attachment; filename=nodes-${now}.csv`);
      res.setHeader('Content-type', 'text/csv');
      res.send(csv);
    } catch (e) {
      next(e);
    }
  };

  // deprecated
  static linksToCsv = async (req, res, next) => {
    try {
      const now = moment()
        .format('YY-MM-DDThh-mm-ss');
      await validate(req.body, {
        links: 'required|array',
        'links.*.source': 'required',
        'links.*.target': 'required',
        'links.*.value': 'required|numeric',
      });
      const { links } = req.body;
      const linksArr = links.map((d) => Csv.linksObjToArray(d));
      linksArr.unshift(Csv.linksHeader());
      const csv = Csv.stringify(linksArr);
      if (req.return) {
        return csv;
      }
      res.setHeader('Access-Control-Expose-Headers', 'content-disposition');
      res.setHeader('Content-Disposition', `attachment; filename=links-${now}.csv`);
      res.setHeader('Content-type', 'text/csv');
      res.send(csv);
    } catch (e) {
      next(e);
    }
  };

  // deprecated
  /* static graphToZip = async (req, res, next) => {
     try {
       req.return = true;
       const now = moment()
         .format('YY-MM-DDThh-mm-ss');
       const nodes = await this.nodesToCsv(req, res, next);
       const links = await this.linksToCsv(req, res, next)
         .catch(() => '');

       const zip = new Zip();
       zip.file('nodes.csv', nodes);
       if (links) {
         zip.file('links.csv', links);
       }
       const data = zip.generate({
         base64: false,
         compression: 'DEFLATE',
       });

       res.setHeader('Access-Control-Expose-Headers', 'content-disposition');
       res.setHeader('Content-Disposition', `attachment; filename=graph-${now}.zip`);
       res.setHeader('Content-type', 'application/zip');
       res.send(Buffer.from(data, 'binary'));
     } catch (e) {
       next(e);
     }
   }; */

  // deprecated
  static csvToGraph = async (req, res, next) => {
    try {
      const { files } = req;
      if (_.isEmpty(files)) {
        throw HttpError(422);
      }
      const data = [];
      files.forEach((file) => {
        data.push(Csv.parse(file.buffer.toString()));
      });
      let nodes;
      let links;
      if ('name' in data[0][0]) {
        [nodes, links] = data;
      } else {
        [links, nodes] = data;
      }

      res.json({
        status: 'ok',
        nodes,
        links,
      });
    } catch (e) {
      next(e);
    }
  };

  // deprecated
  static zipToGraph = async (req, res, next) => {
    try {
      const { file } = req;
      if (!file) {
        throw HttpError(422);
      }
      const zip = new Zip(file.buffer, { base64: false });

      let nodesCsv = zip.files['nodes.csv'];
      nodesCsv = new TextDecoder('utf-8').decode(nodesCsv._data.getContent());
      const nodes = Csv.parse(nodesCsv);

      let linksCsv = zip.files['links.csv'];
      let links = [];
      if (linksCsv) {
        linksCsv = new TextDecoder('utf-8').decode(linksCsv._data.getContent());
        links = Csv.parse(linksCsv);
      }

      res.json({
        status: 'ok',
        nodes,
        links,
      });
    } catch (e) {
      next(e);
    }
  };

  static graphToZip = async (req, res, next) => {
    try {
      await validate(req.body, {
        nodesId: 'required|array',
        'nodes.*': 'string',
        links: 'array',
        'links.*': 'string',
        labels: 'array',
        'labels.*': 'string',
      });
      const now = moment().format('YY-MM-DDThh-mm-ss');
      const { userId } = req;
      const {
        nodesId, linksId, labelsId, graphId,
      } = req.body;
      const graph = await Graphs.findUserAssociatedGraph(graphId, userId, {
        full: false,
        userRole: ['edit', 'admin', 'edit_inside'],
      });

      let nodes = graph.nodes.filter((n) => nodesId.includes(n.id));
      const links = graph.links.filter((l) => linksId.includes(l.id));
      const labels = graph.labels.filter((l) => labelsId.includes(l.id));

      const tempDir = Directories.TEMP_DIR(uuidv4());
      fs.mkdirSync(tempDir, { recursive: true });

      nodes = await Promise.map(nodes, async (n) => {
        n.icon = Utils.downloadFile(n.icon, path.join(tempDir, 'icon'), ['png', 'jpg', 'jpeg', 'svg', 'gif']);
        n.description = Utils.downloadFilesFromHtml(n.description, path.join(tempDir, 'files'));
        n.customFields = n.customFields.map((f) => {
          f.value = Utils.downloadFilesFromHtml(f.value, path.join(tempDir, 'files'));
          return f;
        });
        n.customFields = await Promise.object(n.customFields, 'value', 20);
        console.log( n.customFields)
        return n;
      });
      nodes = await Promise.object(nodes, 'icon', 20);
      nodes = await Promise.object(nodes, 'description', 20);

      const data = Converter.graphToXlsx(nodes, links, labels);

      fs.writeFileSync(path.join(tempDir, 'graph.xlsx'), data, 'buffer');

      const zip = new AdmZip();
      zip.addLocalFolder(tempDir, '/');

      const zipBuffer = zip.toBuffer();

      Utils.unlinkDirSync(tempDir);

      res.setHeader('Access-Control-Expose-Headers', 'content-disposition');
      res.setHeader('Content-Disposition', `attachment; filename=graph-${now}.zip`);
      res.setHeader('Content-type', 'application/zip');
      res.send(zipBuffer);
    } catch (e) {
      next(e);
    }
  };

  static graphToXlsx = async (req, res, next) => {
    try {
      await validate(req.body, {
        nodesId: 'required|array',
        'nodes.*': 'string',
        links: 'array',
        'links.*': 'string',
        labels: 'array',
        'labels.*': 'string',
      });
      const now = moment().format('YY-MM-DDThh-mm-ss');
      const { userId } = req;
      const {
        nodesId, linksId, labelsId, graphId,
      } = req.body;
      const graph = await Graphs.findUserAssociatedGraph(graphId, userId, {
        full: false,
        userRole: ['edit', 'admin', 'edit_inside'],
      });

      const nodes = graph.nodes.filter((n) => nodesId.includes(n.id));
      const links = graph.links.filter((l) => linksId.includes(l.id));
      const labels = graph.labels.filter((l) => labelsId.includes(l.id));

      const tempDir = Directories.TEMP_DIR(uuidv4());
      fs.mkdirSync(tempDir, { recursive: true });

      const data = Converter.graphToXlsx(nodes, links, labels);

      res.setHeader('Access-Control-Expose-Headers', 'content-disposition');
      res.setHeader('Content-Disposition', `attachment; filename=graph-${now}.xlsx`);
      res.setHeader('Content-type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
      res.send(data);
    } catch (e) {
      next(e);
    }
  };

  static graphFromZip = async (req, res, next) => {
    try {
      await validate(req.body, {
        graphId: 'required|numeric',
      });
      const { file } = req;
      const { graphId } = req.body;
      const graph = await Graphs.findByPk(graphId);
      if (!graph) {
        throw HttpError(404);
      }
      if (!file) {
        throw HttpError(422);
      }
      const zip = new AdmZip(req.file.buffer);
      const tempDir = Directories.TEMP_DIR(uuidv4());

      zip.extractAllTo(tempDir, true);

      const {
        nodes,
        links,
        customFields,
        labels,
        warnings,
      } = await Converter.xlsxToGraph(tempDir, graphId);

      res.json({
        status: 'ok',
        nodes,
        links,
        customFields,
        warnings,
        labels,
      });
    } catch (e) {
      next(e);
    }
  };

  static xlsxToGraph = async (req, res, next) => {
    try {
      await validate(req.body, {
        graphId: 'required|numeric',
      });
      const { file } = req;
      const { graphId } = req.body;
      const graph = await Graphs.findByPk(graphId);
      if (!graph) {
        throw HttpError(404);
      }
      if (!file) {
        throw HttpError(422);
      }

      const {
        nodes,
        links,
        warnings,
        labels,
      } = await Converter.xlsxToGraph(req.file.buffer, graphId);

      res.json({
        status: 'ok',
        nodes,
        links,
        warnings,
        labels,
      });
    } catch (e) {
      next(e);
    }
  };

  static svgToPing = async (req, res, next) => {
    try {
      let { svg } = req.body;
      if (!svg) {
        throw HttpError(422);
      }
      const now = moment().format('YY-MM-DDThh-mm-ss');
      svg = Utils.setGraphSvgStyles(svg);
      const png = await Converter.svgToPng(svg);

      res.setHeader('Access-Control-Expose-Headers', 'content-disposition');
      res.setHeader('Content-Disposition', `attachment; filename=graph-${now}.png`);
      res.setHeader('Content-type', 'image/png');
      res.send(png);
    } catch (e) {
      console.log(e);
      next(e);
    }
  };

  static htmlToPdf = async (req, res, next) => {
    try {
      const { html } = req.body;
      if (!html) {
        throw HttpError(422);
      }
      const now = moment().format('YY-MM-DDThh-mm-ss');

      const pdf = await Converter.htmlToPdf(html);

      res.setHeader('Access-Control-Expose-Headers', 'content-disposition');
      res.setHeader('Content-Disposition', `attachment; filename=graph-${now}.pdf`);
      res.setHeader('Content-type', 'application/pdf');
      res.send(pdf);
    } catch (e) {
      next(e);
    }
  };

  static svgToPdf = async (req, res, next) => {
    try {
      let { svg } = req.body;
      if (!svg) {
        throw HttpError(422);
      }
      const now = moment().format('YY-MM-DDThh-mm-ss');
      svg = Utils.setGraphSvgStyles(svg);
      const pdf = await Converter.svgToPdf(svg);

      res.setHeader('Access-Control-Expose-Headers', 'content-disposition');
      res.setHeader('Content-Disposition', `attachment; filename=graph-${now}.pdf`);
      res.setHeader('Content-type', 'application/pdf');
      res.send(pdf);
    } catch (e) {
      next(e);
    }
  };

  static pdfToNode = async (req, res, next) => {
    try {
      const { file } = req;
      if (!file) throw HttpError(422);

      const node = await Converter.pdfToNode(file);

      res.json({
        status: 'ok',
        node,
      });
    } catch (e) {
      console.log(e);
      next(e);
    }
  };

  static googleSheetsToGraph = async (req, res, next) => {
    try {
      await validate(req.body, {
        url: 'required|url',
        graphId: 'required|numeric',
      });
      const { url, graphId } = req.body;
      const [, key] = /spreadsheets\/d\/([^/]+)/.exec(url) || [];
      if (!key) {
        throw HttpError(422, { errors: { url: 'Invalid url' } });
      }

      const downloadUrl = `https://docs.google.com/feeds/download/spreadsheets/Export?key=${key}&exportFormat=xlsx`;
      const { data: buffer } = await axios(downloadUrl, {
        responseType: 'arraybuffer',
      }).catch((e) => e);

      if (!buffer) {
        throw HttpError(422, { errors: { url: 'something went wrong' } });
      }

      const {
        nodes,
        links,
        customFields,
        labels,
        warnings,
      } = await Converter.xlsxToGraph(buffer, graphId);

      res.json({
        status: 'ok',
        nodes,
        links,
        customFields,
        labels,
        warnings,
      });
    } catch (e) {
      next(e);
    }
  };

  static svgNodeToPng = async (req, res, next) => {
    try {
      await validate(req.body, {
        svgNode: 'required|string',
      });
      const { svgNode } = req.body;

      const svg = `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="1228" height="878" viewBox="0,0,1228,878">${svgNode}</svg>`;
      const svgDom = new JSDOM(svg);
      const width = '';
      const svgDocument = svgDom.window.document;
      svgDocument.querySelector('.infography').setAttribute('transform', 'translate(100% 100%)');
      const png = await Converter.svgToPng(svgDocument.querySelector('svg').outerHTML);

      res.setHeader('Access-Control-Expose-Headers', 'content-disposition');
      res.setHeader('Content-Disposition', 'attachment; filename=graph-node.png');
      res.setHeader('Content-type', 'image/png');
      res.send(png);
    } catch (e) {
      next(e);
    }
  };
}

export default DownloadController;
