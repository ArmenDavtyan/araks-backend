import HttpErrors from 'http-errors';
import Promise from 'bluebird';
import moment from 'moment';
import _ from 'lodash';
import mime from 'mime-types';
import path from 'path';
import Sequelize, { literal } from 'sequelize';
import validate from '../services/validate';
import { Documents, Graphs } from '../models';
import Utils from '../services/Utils';
import Socket from '../services/Socket';
// import GraphsHistory from './GraphHistoryController';
// import GraphsHistoryModel from '../models/GraphsHistory';
import eventType from '../helpers/EventType';

class NodeController {
  static create = async (req, res, next) => {
    try {
      await validate(req.body, {
        'nodes.*.id': 'required|string',
        'nodes.*.fx': 'numeric',
        'nodes.*.fy': 'numeric',
        'nodes.*.name': 'required|string',
        'nodes.*.type': 'string',
        'nodes.*.status': 'string',
        'nodes.*.nodeType': 'string',
        'nodes.*.icon': 'string',
        'nodes.*.link': 'string',
        'nodes.*.keywords': 'array',
        'nodes.*.keywords.*': 'string',
        'nodes.*.color': 'string',
        'nodes.*.readOnly': 'string',
        'nodes.*.sourceId': 'string',
        'nodes.*.labels': 'array',
        'nodes.*.labels.*': 'string',
        'nodes.*.d': 'array',
        'nodes.*.d[0].*': 'numeric',
        'nodes.*.d[1].*': 'numeric',
        'nodes.*.infographyId': 'string',
      });
      const { userId } = req;
      let { nodes } = req.body;
      const { graphId } = req.params;
      const graph = await Graphs.findUserAssociatedGraph(graphId, userId, {
        full: true,
        userRole: ['edit', 'admin', 'edit_inside'],
      });
      if (!graph) {
        throw HttpErrors(403);
      }
      nodes = nodes.filter((n) => n.name);
      nodes = await Promise.map(nodes, async (node) => {
        if (graph.nodes.some((d) => d.id === node.id)) {
          return undefined;
        }
        delete node.hidden;
        node.createdAt = moment().utc().unix();
        node.updatedAt = moment().utc().unix();
        node.updatedUser = userId;
        node.createdUser = userId;
        node.createdUser = userId;
        node.customFields = node.customFields || [];

        [node.description, node.icon] = await Promise.all([
          Utils.template(node.description, {}),
          Utils.uploadIcon(node.icon, graphId),
        ]);

        // history
        // GraphsHistory.insertData({
        //   graphsId: graphId,
        //   eventId: eventType.NodeCreate,
        //   eventType: eventType.NodeCreate,
        //   userId,
        //   data: node,
        //   dataDetails: { nodeName: [node.name] },
        //   eventDate: moment().utc().unix(),
        // });
        // end history

        return node;
      });
      nodes = _.compact(nodes);
      graph.nodes.push(...nodes);

      await Socket.emitSharedUsers(graphId, graph.userId, userId, 'node.create', { graphId: graph.id, nodes, userId });

      await Graphs.update({
        nodes: graph.nodes,
      }, {
        where: {
          id: graphId,
        },
        individualHooks: true,
      });

      await Socket.emitEmbeddedUsers(graph);

      res.json({
        status: 'ok',
        nodes,
      });
    } catch (e) {
      next(e);
    }
  };

  static update = async (req, res, next) => {
    try {
      if (!req.body.name) {
        await validate(req.body, {
          'nodes.*.id': 'required|string',
          'nodes.*.fx': 'required|numeric',
          'nodes.*.fy': 'required|numeric',
        });
      } else {
        await validate(req.body, {
          'nodes.*.id': 'required|string',
          'nodes.*.fx': 'numeric',
          'nodes.*.fy': 'numeric',
          'nodes.*.name': 'string',
          'nodes.*.type': 'string',
          'nodes.*.status': 'string',
          'nodes.*.nodeType': 'required|string',
          'nodes.*.icon': 'string',
          'nodes.*.link': 'string',
          'nodes.*.keywords': 'array',
          'nodes.*.keywords.*': 'string',
          'nodes.*.color': 'string',
          'nodes.*.readOnly': 'string',
          'nodes.*.sourceId': 'string',
          'nodes.*.labels': 'array',
          'nodes.*.labels.*': 'string',
          'nodes.*.d': 'array',
          'nodes.*.d[0].*': 'numeric',
          'nodes.*.d[1].*': 'numeric',
          'nodes.*.infographyId': 'string',
        });
      }

      const { userId } = req;
      let { nodes } = req.body;
      const { graphId } = req.params;
      const graph = await Graphs.findUserAssociatedGraph(graphId, userId, {
        full: true,
        userRole: ['edit', 'admin', 'edit_inside'],
      });
      if (!graph) {
        throw HttpErrors(403);
      }
      nodes = nodes.filter((n) => n.name);
      nodes = await Promise.map(nodes, async (node) => {
        const nodeId = node.id;
        const nodeIndex = graph.nodes.findIndex((n) => n.id === nodeId);
        if (nodeIndex === -1) {
          throw HttpErrors(422, 'invalid node');
        }
        const originalNode = graph.nodes[nodeIndex];

        if (!node.name) {
          node = {
            ...originalNode,
            fx: node.fx,
            fy: node.fy,
          };
        } else if (_.isUndefined(node.fx)) {
          node = {
            ...node,
            fx: originalNode.fx,
            fy: originalNode.fy,
          };
        }
        node.customFields = _.uniqBy([...(node.customFields || []), ...originalNode.customFields], 'name');

        // normalize node data
        delete node.hidden;
        node.id = nodeId;
        node.updatedAt = moment().utc().unix();
        node.updatedUser = userId;
        [
          node.description,
          node.icon,
        ] = await Promise.all([
          Utils.template(node.description, {}),
          Utils.uploadIcon(node.icon, graphId),
        ]);

        // forbid other users to change node status
        if (+originalNode.createdUser !== +userId && originalNode.status !== node.status) {
          node.status = originalNode.status;
        }

        graph.nodes[nodeIndex] = node;
        // history
        // GraphsHistory.insertData({
        //   graphsId: graphId,
        //   eventId: eventType.NodeUpdate,
        //   eventType: eventType.NodeUpdate,
        //   userId,
        //   data: node,
        //   dataDetails: { nodeName: [originalNode.name, node.name] },
        //   eventDate: moment().utc().unix(),
        // });
        // end history

        return node;
      });

      await Socket.emitSharedUsers(graphId, graph.userId, userId, 'node.update', { graphId: graph.id, nodes, userId });


      await Graphs.update({
        nodes: graph.nodes,
      }, {
        where: {
          id: graphId,
        },
        individualHooks: true,
      });
      res.json({
        status: 'ok',
      });
    } catch (e) {
      next(e);
    }
  };

  // deprecated
  static updatePositions = async (req, res, next) => {
    try {
      await validate(req.body, {
        'nodes.*.id': 'required|string',
        'nodes.*.fx': 'required|numeric',
        'nodes.*.fy': 'required|numeric',
        'nodes.*.labels': 'array',
      });
      const { userId } = req;
      let { nodes } = req.body;
      const { graphId } = req.params;
      const graph = await Graphs.findUserAssociatedGraph(graphId, userId, {
        full: true,
        userRole: ['edit', 'admin', 'edit_inside'],
      });
      if (!graph) {
        throw HttpErrors(403);
      }
      const sql = [];

      nodes = nodes.map((d) => {
        const i = graph.nodes.findIndex((n) => n.id === d.id);
        const node = graph.nodes[i];
        if (node) {
          node.fx = d.fx;
          node.fy = d.fy;
          node.labels = d.labels || node.labels;

          // history
          // GraphsHistory.insertData({
          //   graphsId: graphId,
          //   eventId: eventType.updatePositions,
          //   eventType: eventType.updatePositions,
          //   userId,
          //   data: node,
          //   dataDetails: { nodeName: [node.name] },
          //   eventDate: moment().utc().unix(),
          // });
          // history

          sql.push(`'$[${i}].fx', ${node.fx}`);
          sql.push(`'$[${i}].fy', ${node.fy}`);
          sql.push(`'$[${i}].labels', JSON_EXTRACT('${JSON.stringify(node.labels)}', '$')`);
        }
        return node || d;
      });

      const values = {};

      if (sql.length) {
        values.nodes = Sequelize.literal(`JSON_REPLACE(nodes, ${sql.join(', ')})`);
      }

      await Graphs.update(values, {
        where: {
          id: graphId,
        },
        individualHooks: true,
      });

      // const a = await Graphs.update({
      //   nodes: graph.nodes,
      // }, {
      //   where: {
      //     id: graphId,
      //   },
      // });
      const s = await Socket.emitSharedUsers(graphId, graph.userId, userId, 'node.update-positions', {
        graphId: graph.id, nodes, userId,
      });

      await Socket.emitEmbeddedUsers(graph);

      res.json({
        status: 'ok',
        s,
      });
    } catch (e) {
      next(e);
    }
  };

  static updateCustomFields = async (req, res, next) => {
    try {
      await validate(req.body, {
        'nodes.*.id': 'required|string',
        'nodes.*.customFields': 'array',
        'nodes.*.customFields.*.name': 'required|string',
        'nodes.*.customFields.*.value': 'string',
        'nodes.*.customFields.*.subtitle': 'string',
      });
      const { userId } = req;
      const { nodes } = req.body;
      const { graphId } = req.params;
      const graph = await Graphs.findUserAssociatedGraph(graphId, userId, {
        full: true,
        userRole: ['edit', 'admin', 'edit_inside'],
      });
      if (!graph) {
        throw HttpErrors(403);
      }

      const files = [];
      graph.nodes = graph.nodes.map((node) => {
        const d = nodes.find((n) => n.id === node.id);
        if (d) {
          node.customFields = _.uniqBy(d.customFields, 'name');
          node.customFields.forEach((f) => {
            files.push(...Utils.extractFilesFromHtml(f.value));
          });
        }
        // history
        // GraphsHistory.insertData({
        //   graphsId: graphId,
        //   eventId: eventType.NodeUpdateCustomFields,
        //   eventType: eventType.NodeUpdateCustomFields,
        //   userId,
        //   data: node,
        //   dataDetails: { nodeName: [node.name] },
        //   eventDate: moment().utc().unix(),
        // });
        // end history
        return node;
      });

      await Socket.emitSharedUsers(graphId, graph.userId, userId, 'node.update-fields', {
        graphId: graph.id, nodes, userId,
      });

      const doc = await Documents.findAll({
        where: {
          graphId,
          nodeId: { $in: nodes.map((n) => n.id) },
        },
      });

      // const newFiles = _.difference(files, doc.map((d) => d.data)).map(file => ({
      //   userId,
      //   nodeId: d.id,
      //   nodeType: d.type,
      //   nodeName: d.name,
      //   graphId,
      //   tabName: field.name,
      //   data: file,
      //   description: '',
      //   altText: '',
      //   tags: [],
      //   type: mime.lookup(path.extname(file)) || 'video',
      // }));

      console.log(files, 333);

      await Documents.destroy({
        where: {
          graphId,
          nodeId: { $in: nodes.map((n) => n.id) },
          data: { $notIn: files },
        },
      });

      await Graphs.update({
        nodes: graph.nodes,
      }, {
        where: {
          id: graphId,
        },
        individualHooks: true,
      });
      res.json({
        status: 'ok',
      });
    } catch (e) {
      next(e);
    }
  };

  static getCustomFields = async (req, res, next) => {
    try {
      const { userId } = req;
      const { graphId, nodeId } = req.params;
      const graph = await Graphs.findUserAssociatedGraph(graphId, userId, {
        full: false,
        userRole: ['edit', 'admin', 'edit_inside'],
      });
      if (!graph) {
        throw HttpErrors(403);
      }

      const node = graph.nodes.find((n) => n.id === nodeId);

      if (!node) {
        throw HttpErrors(404, 'Node node found');
      }

      const { customFields = [] } = node;

      graph.nodes.forEach((n) => {
        if (n.type === node.type && n.customFields) {
          n.customFields.forEach((f) => {
            if (!customFields.some((c) => c.name === f.name)) {
              customFields.push({ ...f, value: undefined });
            }
          });
        }
      });
      // history
      // GraphsHistory.insertData({
        // graphsId: graphId,
        // eventId: eventType.NodeUpdateCustomFieldsView,
        // eventType: eventType.NodeUpdateCustomFieldsView,
        // userId,
        // data: node,
        // dataDetails: { nodeName: [node.name] },
        // eventDate: moment().utc().unix(),
      // });
      // end history
      res.json({
        status: 'ok',
        customFields,
        v: 1,
      });
    } catch (e) {
      next(e);
    }
  };

  static delete = async (req, res, next) => {
    try {
      const { userId } = req;
      const { graphId } = req.params;
      let { nodes } = req.body;
      const graph = await Graphs.findUserAssociatedGraph(graphId, userId, {
        full: true,
        userRole: ['edit', 'admin', 'edit_inside'],
      });
      if (!graph) {
        throw HttpErrors(403);
      }

      nodes = await Promise.map(nodes, async (node) => {
        const nodeId = node.id || node;
        const nodeIndex = graph.nodes.findIndex((n) => n.id === nodeId);
        if (nodeIndex === -1) {
          throw HttpErrors(422, 'invalid node');
        }
        Utils.deleteIcon(graphId, nodeId);
        const deletedNode = graph.nodes[nodeIndex];
        graph.nodes.splice(nodeIndex, 1);
        // history
        // GraphsHistory.insertData({
        //   graphsId: graphId,
        //   eventId: eventType.NodeDelete,
        //   eventType: eventType.NodeDelete,
        //   userId,
        //   data: deletedNode,
        //   dataDetails: { nodeName: [deletedNode.name] },
        //   eventDate: moment().utc().unix(),
        // });
        // end history
        return deletedNode;
      });

      await Socket.emitSharedUsers(graphId, graph.userId, userId, 'node.delete', { graphId: graph.id, nodes, userId });

      await Documents.destroy({
        where: {
          graphId,
          nodeId: { $in: nodes.map((n) => n.id) },
        },
      });


      await Graphs.update({
        nodes: graph.nodes,
      }, {
        where: {
          id: graphId,
        },
      });

      await Socket.emitEmbeddedUsers(graph);

      res.json({
        status: 'ok',
      });
    } catch (e) {
      next(e);
    }
  };

  static uploadIcon = async (req, res, next) => {
    try {
      await validate(req.body, {
        'node.id': 'required|string',
      });
      const { userId, file } = req;
      const { id } = req.body;
      const { graphId } = req.params;

      const graph = await Graphs.findUserAssociatedGraph(graphId, userId, {
        full: true,
        userRole: ['edit', 'admin', 'edit_inside'],
      });
      if (!graph) {
        throw HttpErrors(403);
      }

      const icon = await Utils.uploadIcon(file, graphId, id);

      res.json({
        status: 'ok',
        icon,
      });
    } catch (e) {
      next(e);
    }
  };

  static uploadFile = async (req, res, next) => {
    try {
      await validate(req.body, {
        'node.id': 'required|string',
        'node.type': 'string',
        'node.name': 'string',
        tabName: 'string',
        alt: 'string',
        description: 'string',
        tags: 'array',
        createDocument: 'boolean',
        'tags.*': 'string',
      });
      const { userId, file } = req;
      const {
        node, tags, description, alt, createDocument,
      } = req.body;
      const { graphId } = req.params;
      if (!file) {
        throw HttpErrors(422);
      }
      const graph = await Graphs.findUserAssociatedGraph(graphId, userId, {
        full: true,
        userRole: ['edit', 'admin', 'edit_inside'],
      });
      if (!graph) {
        throw HttpErrors(403);
      }

      const [uploadedFile] = Utils.uploadFiles(file, graphId, node.id);
      if (uploadedFile && createDocument) {
        await Documents.create({
          userId,
          nodeId: node.id,
          nodeType: node.type,
          nodeName: node.name,
          graphId,
          tabName: '',
          data: uploadedFile,
          description,
          altText: alt,
          tags,
          type: file.mimetype,
        });
      }

      res.json({
        status: 'ok',
        file: uploadedFile,
      });
    } catch (e) {
      next(e);
    }
  };
}

export default NodeController;
