import HttpErrors from 'http-errors';
import { literal, Op } from 'sequelize';
import SqlString from 'sequelize/lib/sql-string';
import {
  Graphs, Share, ShareGraphs, Users, Notifications,
} from '../models';
import validate from '../services/validate';
import Socket from '../services/Socket';
import helpers from '../helpers';
import Mail from '../services/Mail';

class ShareController {
  static create = async (req, res, next) => {
    try {
      const {
        graphId, userId, role, objectId = null, type = 'graph',
      } = req.body;
      await validate(req.body, {
        graphId: 'required|numeric',
        userId: 'required|numeric',
        role: 'in:view,edit,owner,admin,edit_inside',
        objectId: 'string',
        type: 'in:graph,label',
      });
      const { userId: ownerId } = req;

      const graph = Graphs.findUserAssociatedGraph(graphId, ownerId, { userRole: ['admin'] });

      if (!graph) {
        throw HttpErrors(403);
      }
      let share = await Share.findOne({
        where: {
          graphId,
          userId,
        },
      });

      if (share) {
        share.status = 'shared';
        share.type = 'label';
        share.objectId = objectId;
        await share.save();
      } else {
        share = await Share.create({
          graphId,
          userId,
          objectId,
          type,
          role,
        });
      }
      res.json({
        status: 'ok',
        share,
      });
    } catch (e) {
      next(e);
    }
  };

  static update = async (req, res, next) => {
    try {
      const { role } = req.body;
      await validate(req.body, {
        role: 'in:view,edit,owner,admin,edit_inside',
      });
      const { id } = req.params;
      const { userId: ownerId } = req;

      const share = await Share.findByPk(id);

      if (!share) {
        throw HttpErrors(404);
      }

      const graph = Graphs.findUserAssociatedGraph(share.graphId, ownerId, { userRole: ['admin'] });

      if (!graph) {
        throw HttpErrors(403);
      }
      share.role = role;
      await share.save();
      /* if (share.status !== 'new') {

        const graphs = await Graphs.findByPk(share.graphId);
        const user = await Users.findByPk(ownerId);

        await Notifications.create({
          graphId: share.graphId,
          shareGraphId: share.id,
          actionType: 'share-update',
          userId: share.userId,
          text: `<img class="avatar" src=${user.avatar} alt=${user.firstName} />
          <p><strong><a href="/profile/${user.id}" > ${user.firstName} ${user.lastName} </a></strong> has opened for you role to label in graph <strong><a href="/graphs/view/${graphs.id}">${graphs.title}</a></strong> access type is <strong>"${share.role}"</strong></p>`,
        });
        Socket.emitAll(
          `notificationsListGraphShared-${share.userId}`,
          {
            graphId: share.graphId,
            shareGraphId: share.id,
            actionType: 'share-update',
            userId: share.userId,
            text: `<img class="avatar" src=${user.avatar} alt=${user.firstName} />
            <p><strong><a href="/profile/${user.id}" > ${user.firstName} ${user.lastName} </a></strong> has opened for you role to label in graph <strong><a href="/graphs/view/${graphs.id}">${graphs.title}</a></strong> access type is <strong>"${share.role}"</strong></p>`,
          },
        );

      } */

      res.json({
        status: 'ok',
        share,
      });
    } catch (e) {
      next(e);
    }
  };

  static delete = async (req, res, next) => {
    try {
      const { id } = req.params;
      const { userId: ownerId } = req;

      const share = await Share.findByPk(id);

      if (!share) {
        throw HttpErrors(404);
      }

      const graph = Graphs.findUserAssociatedGraph(share.graphId, ownerId, { userRole: ['admin'] });

      if (!graph) {
        throw HttpErrors(403);
      }

      if (share.status === 'new') {
        await share.destroy();
      } else {
        await share.update({
          status: 'deleted',
        }, {
          where: {
            id,
          },
        });
      }

      res.json({
        status: 'ok',
      });
    } catch (e) {
      next(e);
    }
  };

  static getUsers = async (req, res, next) => {
    try {
      await validate(req.query, {
        graphId: 'required|numeric',
        objectId: 'string',
        type: 'in:graph,label',
      });
      const { graphId, objectId, type = 'graph' } = req.query;
      const { userId: ownerId } = req;
      const graph = Graphs.findUserAssociatedGraph(graphId, ownerId, { userRole: ['admin'] });
      if (!graph) {
        throw HttpErrors(403);
      }

      const users = await Users.findAll({
        include: [{
          model: Share,
          as: 'share',
          where: {
            graphId,
            objectId,
            type,
            [Op.not]: { status: 'deleted' },
          },
        }],
      });

      res.json({
        status: 'ok',
        users,
      });
    } catch (e) {
      next(e);
    }
  };

  static getList = async (req, res, next) => {
    try {
      await validate(req.query, {
        page: 'integer',
        search: 'string',
        files: 'in:active,draft,template',
      });
      const { userId } = req;
      const { page = 1 } = req.query;
      const limit = 15;
      const offset = (page - 1) * limit;

      const where = {
        userId,
        [Op.not]: { status: 'deleted' },
      };
      const shareGraphs = await Graphs.findAll({
        include: [{
          model: Users,
          as: 'user',
        }, {
          model: Share,
          as: 'share',
          where,
        }],
        limit,
        offset,
        order: [
          [{ model: Share, as: 'share' }, 'updatedAt', 'DESC'],
        ],
        attributes: {
          exclude: ['nodes', 'links', 'labels', 'customFields'],
          include: [
            [literal('JSON_LENGTH(nodes)'), 'nodesCount'],
            [literal('JSON_LENGTH(links)'), 'linksCount'],
            [literal('JSON_LENGTH(labels)'), 'labelsCount'],
          ],
        },
      });

      /* let shareGraphsDeprecated = await Graphs.findAll({
        include: [{
          model: Users,
          as: 'user',
        }, {
          model: ShareGraphs,
          as: 'shareGraph',
          where: {
            userId,
            status: 'shared',
          },
        }],
        limit,
        offset,
        order: [
          [ { model: ShareGraphs, as: 'shareGraph' } , 'updatedAt', 'DESC' ]
        ],
        attributes: {
          exclude: ['nodes', 'links', 'labels', 'customFields'],
          include: [
            [literal('JSON_LENGTH(nodes)'), 'nodesCount'],
            [literal('JSON_LENGTH(links)'), 'linksCount'],
            [literal('JSON_LENGTH(labels)'), 'labelsCount'],
          ],
        },
      });

      shareGraphsDeprecated = shareGraphsDeprecated.map((s) => {
        s.setDataValue('share', s.shareGraph);
        return s;
      }); */

      const total = await Graphs.count({
        include: [{
          model: Share,
          as: 'share',
          where,
        }],
      });

      const totalPages = Math.ceil(total / limit);
      res.json({
        status: 'ok',
        shareGraphs,
        totalPages,
        total,
      });
    } catch (e) {
      next(e);
    }
  };

  /**
   *
   * @param {*} req
   * @param {*} res
   * @param {*} next
   * Change stutus in share data.
   * Send Mail
   * Send Notifications
   */
  static updateStatus = async (req, res, next) => {
    try {
      await validate(req.body, {
        graphId: 'required|integer',
      });
      const {
        emailTexts: { permissionCreateSubject, permissionCreateText },
        common: { getUserFullName },
      } = helpers;

      const {
        graphId,
      } = req.body;
      const { userId: sharerId } = req;

      await Share.update({
        status: 'shared',
      }, {
        where: {
          status: 'new',
          graphId,
        },
      });
      const shareGraphsList = await Share.findAll({
        where: {
          status: 'shared',
          graphId,
        },
        include: [
          {
            model: Graphs,
            as: 'graph',
          },
          {
            model: Users,
            as: 'user',
          },
        ],
      });
      const shareUser = await Users.findByPk(sharerId);

      if (shareGraphsList) {
        shareGraphsList.forEach(async (item) => {
          await Notifications.create({
            graphId,
            shareGraphId: item.id,
            actionType: 'share-add',
            userId: item.userId,
            text: `<img class="avatar" src=${shareUser.avatar} alt=${shareUser.firstName} /> 
            <p><strong><a href="/profile/${shareUser.id}" > ${shareUser.firstName} ${shareUser.lastName} </a></strong> has opened for you role to label in graph <strong><a href="/graphs/view/${item.graph.id}">${item.graph.title}</a></strong> access type is <strong>"${item.role}"</strong></p>`,
          });

          Socket.emitAll(
            `notificationsListGraphShared-${item.userId}`,
            {
              graphId,
              shareGraphId: item.id,
              actionType: 'share-add',
              userId: item.userId,
              text: `<img class="avatar" src=${shareUser.avatar} alt=${shareUser.firstName} /> 
              <p><strong><a href="/profile/${shareUser.id}" > ${shareUser.firstName} ${shareUser.lastName} </a></strong> has opened for you role to label in graph <strong><a href="/graphs/view/${item.graph.id}">${item.graph.title}</a></strong> access type is <strong>"${item.role}"</strong></p>`,
            },
          );

          const { user } = item;
          const sharer = await Users.findByPk(sharerId);

          await Mail.send(
            user.email,
            permissionCreateSubject(getUserFullName(sharer)),
            permissionCreateText(getUserFullName(sharer), getUserFullName(user), graphId),
          );
        });
      }
      const data = await Share.getListData(graphId);
      res.json(data);
    } catch (e) {
      next(e);
    }
  };

  /**
   * Delete label in share list and call socket
   * @param {*} req
   * @param {*} res
   * @param {*} next
   */
  static labelDelete = async (req, res, next) => {
    try {
      await validate(req.query, {
        graphId: 'required|numeric',
        labelId: 'required|string',
      });
      const { userId } = req;
      const { labelId, graphId } = req.query;

      const graph = Graphs.findUserAssociatedGraph(graphId, userId, { userRole: ['admin'] });

      if (!graph) {
        throw HttpErrors(403);
      }

      const shareList = await Share.findAll({
        where: {
          objectId: labelId,
          graphId,
          [Op.not]: { status: 'deleted' },
        },
      });

      if (shareList) {
        shareList.forEach(async (share) => {
          if (share.status === 'new') {
            await share.destroy();
          } else {
            await share.update({
              status: 'deleted',
            }, {
              where: {
                id: share.id,
              },
            });
          }
          Socket.emitAll('shareList', { graphId });
        });
      }
      res.json({
        status: 'ok',
      });
    } catch (e) {
      next(e);
    }
  };
}

export default ShareController;
