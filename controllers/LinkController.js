import HttpErrors from 'http-errors';
import _ from 'lodash';
import validate from '../services/validate';
import {Graphs} from '../models';
import Socket from '../services/Socket';
import Utils from '../services/Utils';

class NodeController {
  static create = async (req, res, next) => {
    try {
      await validate(req.body, {
        'links.*.id': 'required|string',
        'links.*.source': 'required|string',
        'links.*.target': 'required|string',
        'links.*.value': 'required|numeric',
        'links.*.linkType': 'string',
        'links.*.type': 'string',
        'links.*.direction': 'boolean',
        'links.*.color': 'string',
        'links.*.status': 'string',
        'links.*.sourceId': 'string',
        'links.*.readOnly': 'string',
      });
      const { userId } = req;
      const { graphId } = req.params;
      let { links } = req.body;
      const graph = await Graphs.findUserAssociatedGraph(graphId, userId, {
        full: true,
        userRole: ['edit', 'admin', 'edit_inside'],
      });
      if (!graph) {
        throw HttpErrors(403);
      }

      links = links.map((link) => {
        delete link.hidden;
        delete link.create;

        link.direction = !!link.direction;

        if (!Utils.isLinkUnique(link, graph.links)) {
          return undefined;
        }

        return link;
      });

      graph.links.push(..._.compact(links));

      await Socket.emitSharedUsers(graphId, graph.userId, userId, 'link.create', { graphId: graph.id, links, userId });
      await Graphs.update({
        links: graph.links,
      }, {
        where: {
          id: graphId,
        },
        individualHooks: true,
      });

      await Socket.emitEmbeddedUsers(graph);

      res.json({
        status: 'ok',
      });
    } catch (e) {
      next(e);
    }
  };

  static update = async (req, res, next) => {
    try {
      await validate(req.body, {
        'links.*.id': 'required|string',
        'links.*.source': 'required|string',
        'links.*.target': 'required|string',
        'links.*.value': 'required|numeric',
        'links.*.linkType': 'string',
        'links.*.type': 'string',
        'links.*.direction': 'boolean',
        'links.*.color': 'string',
        'links.*.status': 'string',
        'links.*.sourceId': 'string',
        'links.*.readOnly': 'string',
      });
      const { userId } = req;
      let { links } = req.body;
      const { graphId } = req.params;

      const graph = await Graphs.findUserAssociatedGraph(graphId, userId, {
        full: true,
        userRole: ['edit', 'admin', 'edit_inside'],
      });
      if (!graph) {
        throw HttpErrors(403);
      }

      links = links.map((link) => {
        const linkIndex = graph.links.findIndex((n) => n.id === link.id);
        if (linkIndex === -1) {
          throw HttpErrors(404);
        }
        delete link.hidden;
        delete link.create;

        graph.links[linkIndex] = link;
        return link;
      });

      await Socket.emitSharedUsers(graphId, graph.userId, userId, 'link.update', { graphId: graph.id, links, userId });

      await Graphs.update({
        links: graph.links,
      }, {
        where: {
          id: graphId,
        },
        individualHooks: true,
      });

      await Socket.emitEmbeddedUsers(graph);

      res.json({
        status: 'ok',
      });
    } catch (e) {
      next(e);
    }
  };

  static delete = async (req, res, next) => {
    try {
      const { userId } = req;
      const { graphId } = req.params;
      let { links } = req.body;
      const graph = await Graphs.findUserAssociatedGraph(graphId, userId, {
        full: true,
        userRole: ['edit', 'admin', 'edit_inside'],
      });
      if (!graph) {
        throw HttpErrors(403);
      }

      links = links.map((link) => {
        const linkId = link.id || link;
        const linkIndex = graph.links.findIndex((n) => n.id === linkId);
        if (linkIndex === -1) {
          throw HttpErrors(422, 'invalid link');
        }
        const linkDeleted = graph.links[linkIndex];
        graph.links.splice(linkIndex, 1);
        return linkDeleted;
      });

      await Socket.emitSharedUsers(graphId, graph.userId, userId, 'link.delete', { graphId: graph.id, links, userId });

      await Graphs.update({
        links: graph.links,
      }, {
        where: {
          id: graphId,
        },
      });

      await Socket.emitEmbeddedUsers(graph);

      res.json({
        status: 'ok',
      });
    } catch (e) {
      next(e);
    }
  };
}

export default NodeController;
