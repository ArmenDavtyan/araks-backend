﻿import HttpErrors from 'http-errors';
import _ from 'lodash';
import fs from 'fs';
import mime from 'mime-types';
import path from 'path';
import moment from 'moment';
import Sequelize, { literal, Op } from 'sequelize';
import SqlString from 'sequelize/lib/sql-string';
import Promise from 'bluebird';
import validate from '../services/validate';
import {
  Graphs, Users, ShareGraphs, CommentGraphs, GraphMeta, GraphLabelsEmbed, Documents, Share,
} from '../models';
import Utils from '../services/Utils';
import Directories from '../services/Directories';
import Socket from '../services/Socket';
import WorkerThread from '../services/WorkerThread';
import schema from '../services/schema';
import { v4 as uuidv4 } from 'uuid';
import eventType from '../helpers/EventType';
// import GraphsHistory from './GraphHistoryController';

WorkerThread.call('generateThumbnail', {});

class GraphController {
  static getList = async (req, res, next) => {
    try {
      await validate(req.query, {
        page: 'integer',
        search: 'string',
        files: 'in:active,draft,template',
      });
      const { userId } = req;
      const {
        page = 1, status = 'active', s, onlyTitle, limit = 15, mysearch
      } = req.query;
      const offset = (page - 1) * limit;

      const where = {
        userId,
        status,
      };
      
      let order = [['updatedAt', 'DESC']];
      if (s) {
        const search = SqlString.escape(`%${s}%`);
        order = [[literal(`CASE WHEN title LIKE ${search} THEN 1 ELSE 0 END`), 'DESC']];
        where.$and = [{
          $or: [
            { id: { $like: s } },
            { title: { $like: `%${s}%` } },
            { description: { $like: `%${s}%` } },
          ],
          userId,
        }];
        if (!onlyTitle) {
          where.$and[0].$or.push(
            { $or: literal(`nodes->"$[*].name" COLLATE utf8mb4_GENERAL_CI like  ${search}`) },
            { $or: literal(`nodes->"$[*].type" COLLATE utf8mb4_GENERAL_CI like  ${search}`) },
            { $or: literal(`links->"$[*].type" COLLATE utf8mb4_GENERAL_CI like  ${search}`) },
          );
        }
      }

      let graphs = await Graphs.findAll({
        where,
        include: [{
          model: Users,
          as: 'user',
        }, {
          model: GraphMeta,
          as: 'graphMeta',
          required: false,
          where: {
            key: 'usersView',
          },
        }],
        order,
        attributes: {
          exclude: ['nodes', 'links', 'labels', 'customFields'],
          include: [
            [literal('JSON_LENGTH(nodes)'), 'nodesCount'],
            [literal('JSON_LENGTH(links)'), 'linksCount'],
            [literal('JSON_LENGTH(labels)'), 'labelsCount'],
          ],
        },
        limit,
        offset,
      });

      graphs = graphs.map((d) => {
        d.dataValues.views = d.meta && d.meta.usersView ? d.meta.usersView.length : 0;
        delete d.dataValues.meta;

        if (d.infoGraphics && d.infoGraphics.length) {
          d.nodes = [...d.nodes, ...d.infoGraphics];
        }
        return d;
      });

      const total = await Graphs.count({ where });

      const totalPages = Math.ceil(total / limit);

      res.json({
        status: 'ok',
        graphs,
        page,
        total,
        totalPages,
      });
    } catch (e) {
      next(e);
    }
  };

  static getNodesList = async (req, res, next) => {
    try {
      await validate(req.query, {
        page: 'integer',
        search: 'string',
      });
      const { userId } = req;
      const {
        page = 1, status = 'active', s, graphId,
      } = req.query;
      const limit = 15;
      const offset = (page - 1) * limit;

      const where = {
        userId,
        status,
      };

      if (s) {
        const search = SqlString.escape(`%${s}%`);
        where.$and = {
          $and: literal(`nodes->"$[*].name" COLLATE utf8mb4_GENERAL_CI like  ${search}`),
          userId,
          id: { $not: graphId },
        };
      }

      const graphAllShares = await Share.findAll({
        where: {
          userId,
        },
        include: [
          {
            model: Graphs,
            as: 'graph',
          },
          {
            model: Users,
            as: 'user',
            attributes: ['email'],
          },
        ],
      });

      let graphs = await Graphs.findAll({
        where,
        attributes: {
          exclude: ['links', 'labels'],
        },
        limit,
        offset,
      });

      graphs = graphs.concat(graphAllShares.map((p) => p.graph));

      for (const graph of graphs) {
        for (let i = 0; i < graph.nodes.length; i++) {
          const node = graph.nodes[i];
          if (node.name.toLowerCase().includes(s.toLowerCase())) {
            const user = await Users.findByPk(node.updatedUser);
            if (user) {
              node.userName = `${user.firstName} ${user.lastName}`;
            }
          } else {
            graph.nodes.splice(i, 1);
            i--;
          }
        }
      }

      const total = await Graphs.count({ where });

      const totalPages = Math.ceil(total / limit);

      res.json({
        status: 'ok',
        graphs,
        page,
        total,
        totalPages,
      });
    } catch (e) {
      next(e);
    }
  };

  static getSingle = async (req, res, next) => {
    try {
      const { id } = req.params;
      const { full } = req.query;
      const { userId } = req;
      const graph = await Graphs.findUserAssociatedGraph(id, userId, {
        userRole: ['edit', 'edit_inside', 'admin', 'view'],
      });
      if (!graph) {
        throw HttpErrors(404);
      }
      const info = {
        totalNodes: graph.nodes.length,
        totalLinks: graph.links.length,
        totalLabels: graph.labels.length,
      };

      const usersView = await GraphMeta.findOne({
        where: {
          graphId: id,
          key: 'usersView',
        },
      });

      if (!full) {
        graph.labels.forEach((l) => {
          l.nodes = [];
        });
        graph.nodes = _.compact(graph.nodes.map((d) => {
          d.uId = Utils.nodeIdEncode(d.id);
          delete d.customFields;
          if (_.get(d, 'labels[0]', '').startsWith('f_')) {
            const i = graph.labels.findIndex((l) => l.id === d.labels[0]);
            if (i > -1) {
              graph.labels[i].nodes.push(d.id);
              if (!graph.labels[i].open) {
                return undefined;
              }
            }
          }
          return d;
        }));
        graph.labels.forEach((label) => {
          label.nodes = _.uniq(label.nodes);
          if (!label.id.startsWith('f_')) {
            return;
          }
          const fakeId = `fake_${label.id}`;
          if (!label.open) {
            label.nodes.forEach((nodeId) => {
              graph.links.forEach((link) => {
                if (link.source === nodeId) {
                  link._source = link.source;
                  link.source = fakeId;
                  link.fake = true;
                } else if (link.target === nodeId) {
                  link._target = link.target;
                  link.target = fakeId;
                  link.fake = true;
                }
              });
            });
          }
          graph.nodes.push({
            id: fakeId,
            fx: label.d[0][0] + 30,
            fy: label.d[0][1] + 30,
            fake: true,
            labels: [label.id],
          });
        });
      }

      graph.links = _.chain(graph.links)
        .filter((l) => l.source !== l.target)
        .groupBy((l) => {
          if (l.fake) {
            return JSON.stringify([l.source, l.target].sort());
          }
          if (l.direction) {
            return JSON.stringify({
              1: l.name, 2: l.type, 3: l.source, 4: l.target,
            });
          }
          return JSON.stringify({
            1: l.name, 2: l.type, 3: [l.source, l.target].sort(),
          });
        })
        .map((values) => ({
          ...values[0],
          total: values[0].fake ? values.length : undefined,
        }))
        .value();

      const userIds = _.chain([...graph.nodes, ...graph.links])
        .map((d) => [d.createdUser, d.updatedUser])
        .flatten(1)
        .uniq()
        .value();

      userIds.push(graph.userId);

      const users = await Users.findAll({
        where: { id: { $in: userIds } },
      });
      graph.setDataValue('users', users);

      const views = usersView ? usersView.value || [] : [];
      if (!views.includes(userId)) {
        views.push(userId);
        await GraphMeta.createOrUpdate({
          defaults: {
            graphId: id,
            key: 'usersView',
            value: views,
          },
          where: {
            graphId: id,
            key: 'usersView',
          },
        });
      }

      graph.setDataValue('views', views.length);

      if (graph.infoGraphics && graph.infoGraphics.length) {
        graph.nodes = [...graph.nodes, ...graph.infoGraphics];
      }

      const hasInEmbed = await GraphLabelsEmbed.findAll({
        where: {
          sourceId: id,
        },
        group: ['labelId'],
        attributes: ['labelId'],
      });

      hasInEmbed.forEach((label) => {
        const i = graph.labels.findIndex((l) => l.id === label.labelId);
        if (i > -1) {
          graph.labels[i].hasInEmbed = true;
        }
      });

      graph.setDataValue('hasInEmbed', hasInEmbed);

      let userIdList = await ShareGraphs.findAll({
        where: {
          graphId: id,
        },
        attributes: ['userId'],
      });
      userIdList = userIdList ? userIdList.map((d) => d.userId) : [];
      userIdList.push(userId);
      const embedLabelIds = graph.labels.filter((l) => l.sourceId).map((l) => l.id);
      let embedLabels = await Promise.map(embedLabelIds, (labelId) => (
        GraphLabelsEmbed.findWithGraph({
          where: {
            // userId: { $in: userIdList },
            labelId,
          },
        })
      ));
      embedLabels = _.compact(embedLabels);

      res.json({
        status: 'ok',
        graph,
        embedLabels,
        info,
      });
    } catch (e) {
      next(e);
    }
  };

  static getInfo = async (req, res, next) => {
    try {
      const { id } = req.params;
      const { userId } = req;
      const graph = await Graphs.findUserAssociatedGraph(id, userId, {
        userRole: ['edit', 'edit_inside', 'admin', 'view'],
      });
      if (!graph) {
        throw HttpErrors(404);
      }
      const { nodes, links, labels } = graph;
      const nodeTypes = _.chain(nodes)
        .groupBy('type')
        .map((d, key) => ({ length: d.length, type: key }))
        .orderBy('length', 'desc')
        .value();

      const nodeStatus = _.chain(nodes)
        .map((d) => {
          d.status = d.status || 'approved';
          return d;
        })
        .groupBy('status')
        .map((d, key) => ({ length: d.length, status: key }))
        .orderBy('length', 'desc')
        .value();

      const linkTypes = _.chain(links)
        .groupBy('type')
        .map((d, key) => ({ length: d.length, type: key }))
        .orderBy('length', 'desc')
        .value();

      const linksValue = _.chain(links)
        .groupBy('value')
        .map((d, key) => ({ length: d.length, value: +key }))
        .orderBy('value', 'desc')
        .value();

      const labelsInfo = _.chain(labels)
        .map((l) => ({
          id: l.id,
          color: l.color,
          name: l.name || l.color,
          length: nodes.filter((d) => (d.labels || []).includes(l.id)).length,
        }))
        .orderBy('length', 'desc')
        .value();

      const labelsStatus = _.chain(labels)
        .groupBy('status')
        .map((d, key) => ({ length: d.length, status: key }))
        .orderBy('length', 'desc')
        .value();

      const keywords = _.chain(nodes)
        .map((d) => d.keywords)
        .flatten(1)
        .groupBy()
        .map((d, key) => ({ length: d.length, keyword: key }))
        .value();

      const empty = nodes.filter((d) => _.isEmpty(d.keywords)).length;
      if (empty && keywords.length) {
        keywords.push({
          length: empty,
          keyword: '[ No Keyword ]',
        });
      }

      let nodeConnections = {};
      links.forEach((l) => {
        nodeConnections[l.source] = l.source in nodeConnections ? nodeConnections[l.source] + 1 : 1;
        nodeConnections[l.target] = l.target in nodeConnections ? nodeConnections[l.target] + 1 : 1;
      });

      nodeConnections = _.chain(nodeConnections)
        .map((count, name) => ({ count, name }))
        .groupBy('count')
        .map((d, count) => ({
          count: +count,
          length: d.length,
        }))
        .orderBy('count')
        .value();

      res.json({
        info: {
          totalNodes: nodes.length,
          totalLinks: links.length,
          totalLabels: labels.length,
        },
        filter: {
          nodeTypes,
          nodeConnections,
          nodeStatus,
          linkTypes,
          labelsInfo,
          labelsStatus,
          linksValue,
          keywords,
        },
      });
    } catch (e) {
      next(e);
    }
  };

  static getEmbed = async (req, res, next) => {
    try {
      const { id, token } = req.params;

      const graph = await Graphs.findOne({
        where: {
          id,
          token,
        },
        include: [{
          model: Users,
          as: 'user',
        }],
      });

      if (!graph) {
        throw HttpErrors(404);
      }

      res.json({
        status: 'ok',
        graph,
      });
    } catch (e) {
      next(e);
    }
  };

  static create = async (req, res, next) => {
    try {
      await validate(req.body, {
        nodes: 'array',
        'nodes.*.fx': 'required|numeric',
        'nodes.*.fy': 'required|numeric',
        'nodes.*.name': 'required|string',
        'nodes.*.labels': 'array',
        'nodes.*.labels.*': 'string',
        links: 'array',
        'links.*.source': 'required|string',
        'links.*.target': 'required|string',
        'links.*.value': 'required|numeric',
        files: 'object',
        status: 'in:active,draft,template',
        tags: 'array',
        'tags.*': 'string',
        customFields: 'object',
      });
      const {
        title, description, status, tags, customFields,
      } = req.body;
      let {
        links = [], nodes = [], labels = [],
      } = req.body;
      const { userId } = req;

      links = Utils.uniqueLinks(links);

      const graph = await Graphs.create({
        userId,
        title: title || 'untitled',
        nodes,
        links,
        labels,
        description,
        thumbnail: null,
        status: status || 'active',
        tags,
        customFields,
      });

      const thumbPath = path.basename(Utils.generateGraphThumbnail(graph.id, userId));

      let i = 1;
      nodes = nodes.map((d) => {
        const oldId = d.id;
        d.id = `${i}.${graph.id}`;
        d.uId = Utils.nodeIdEncode(d.id);
        d.crateUser = userId;
        d.updatedUser = userId;
        delete d.index;

        if (oldId) {
          links = links.map((l) => {
            if (l.source === oldId) {
              l.source = d.id;
            } else if (l.target === oldId) {
              l.target = d.id;
            }
            return l;
          });
          /* eslint-disable */
          for (const nodeType in customFields) {
            for (const tab in customFields[nodeType]) {
              for (const nodeId in customFields[nodeType][tab].values) {
                if (nodeId === oldId) {
                  customFields[nodeType][tab].values[d.id] = customFields[nodeType][tab].values[nodeId];
                  delete customFields[nodeType][tab].values[nodeId];
                }
              }
            }
          }
          /* eslint-enable */
        }
        delete d.lx;
        delete d.ly;
        i += 1;
        return d;
      });

      links = links.map((d) => {
        d.id = `${i}.${graph.id}`;
        delete d.index;
        i += 1;
        return d;
      });

      labels = labels.map((d) => {
        if (d.type === 'folder') {
          d.id = `f_${i}.${graph.id}`;
        } else {
          d.id = `${i}.${graph.id}`;
        }
        delete d.index;
        i += 1;
        return d;
      });

      await Graphs.update({
        nodes,
        links,
        labels,
        thumbPath,
        customFields,
      }, {
        where: {
          id: graph.id,
        },
        individualHooks: true,
      });

      // const thumbnail = Utils.getGraphThumbnail(graph.id, userId);
      // graph.thumbnail = thumbnail;
      // await graph.save();

      // const uploadedFiles = Utils.uploadNodeFiles(files, graph.id);
      //
      // const $icon = Symbol('icon');
      // nodes = (nodes || []).map((n) => {
      //   n.description = _.template(n.description)(uploadedFiles);
      //   n[$icon] = n.icon;
      //   n.icon = Utils.uploadIcon(n.icon, graph.id);
      //   return n;
      // });
      // graph.nodes = await Promise.object(nodes, 'icon', 20);

      const emptyThumbnailPath = Directories.PUBLIC_DIR('empty-thumbnail.png');
      if (!fs.existsSync(path.dirname(thumbPath))) {
        fs.mkdirSync(path.dirname(thumbPath), { recursive: true });
      }
      fs.copyFileSync(emptyThumbnailPath, thumbPath);

      res.json({
        status: 'ok',
        graphId: graph.id,
      });
    } catch (e) {
      next(e);
    }
  };

  static update = async (req, res, next) => {
    try {
      await validate(req.body, {
        status: 'in:active,draft,template',
        tags: 'array',
        'tags.*': 'string',
      });
      const {
        title, description, status, tags, svg, labels, publicState
      } = req.body;
      const { id } = req.params;
      const { userId } = req;

      const graph = await Graphs.findUserAssociatedGraph(id, userId, {
        full: true,
        userRole: ['edit', 'admin', 'edit_inside'],
      });
      if (!graph) {
        throw HttpErrors(403);
      }

      await Graphs.update({
        title: title || 'untitled',
        description,
        publicState,
        status: status || 'active',
        tags,
      }, {
        where: {
          id,
        },
      });

      const embedLabels = _.compact(labels.map((l) => l.sourceId));

      await GraphLabelsEmbed.destroy({
        where: {
          graphId: id,
          userId,
          sourceId: { $notIn: embedLabels },
        },
      });

      await Socket.emitEmbeddedUsers(graph);

      if (svg) {
        const thumbPath = Utils.generateGraphThumbnail(graph.id, userId);
        await Utils.generateThumbnail(svg, thumbPath, 'medium');
      }

      res.json({
        status: 'ok',
        graphId: graph.id,
      });
    } catch (e) {
      next(e);
    }
  };

  static updatePositions = async (req, res, next) => {
    try {
      await validate(req.body, {
        labels: 'array',
        'labels.*.id': 'required|string',

        nodes: 'array',
        'nodes.*.id': 'required|string',
        'nodes.*.fx': 'required|numeric',
        'nodes.*.fy': 'required|numeric',
        'nodes.*.labels': 'array',
      });
      const { userId } = req;
      const { labels, nodes } = req.body;
      const { graphId } = req.params;

      const graph = await Graphs.findUserAssociatedGraph(graphId, userId, {
        full: true,
        userRole: ['edit', 'admin', 'edit_inside'],
      });
      if (!graph) {
        throw HttpErrors(403);
      }
      const updateNodes = [];
      const updateLabels = [];

      if (labels.length) {
        graph.labels = graph.labels.map((label) => {
          const d = labels.find((l) => l.id === label.id);
          if (d) {
            if (d.type === 'folder' && !d.open) {
              graph.nodes = graph.nodes.map((node) => {
                if (node.labels.includes(d.id)) {
                  node.fx = node.fx - label.d[0][0] + d.d[0][0];
                  node.fy = node.fy - label.d[0][1] + d.d[0][1];
                  updateNodes.push(node);
                }
                return node;
              });
            }
            if (d.d) {
              label.d = d.d;
            } else {
              label.size = d.size;
            }
            updateLabels.push(label);
          }
          return label;
        })
      }

      if (nodes.length) {
        graph.nodes = graph.nodes.map((node) => {
          const d = nodes.find((n) => n.id === node.id);
          if (d) {
            node.fx = d.fx;
            node.fy = d.fy;
            node.labels = d.labels || node.labels;
            updateNodes.push(node);
          // history
          // GraphsHistory.insertData({
          //   graphsId: graphId,
          //   eventId: eventType.updatePositions,
          //   eventType: eventType.updatePositions,
          //   userId,
          //   data: node,
          //   dataDetails: { nodeName: [node.name] },
          //   eventDate: moment().utc().unix(),
          // });
          // history


          }
          return node;
        });
      }

      await Graphs.update({
        nodes: graph.nodes,
        labels: graph.labels,
      }, {
        where: {
          id: graphId,
        },
        individualHooks: true,
      });

      await Socket.emitSharedUsers(graphId, graph.userId, userId, 'graph.update-positions', {
        graphId: graph.id, labels: updateLabels, nodes: updateNodes, userId,
      });

      await Socket.emitEmbeddedUsers(graph);

      res.json({
        status: 'ok',
        graphId: graph.id,
      });
    } catch (e) {
      next(e);
    }
  };

  static setThumbnail = async (req, res, next) => {
    try {
      await validate(req.body, {
        svg: 'required|string',
      });
      const { userId } = req;
      const { svg, size = 'medium' } = req.body;
      const { id } = req.params;

      const graph = await Graphs.findOne({
        where: {
          id,
          userId,
        },
        attributes: ['thumbnail', 'id'],
      });

      if (!graph) {
        throw HttpErrors(404);
      }
      const thumbPath = Utils.generateGraphThumbnail(graph.id, userId);
      await Utils.generateThumbnail(svg, thumbPath, size);

      res.json({
        status: 'ok',
        graphId: graph.id,
      });
    } catch (e) {
      next(e);
    }
  };

  static delete = async (req, res, next) => {
    try {
      const { id } = req.params;
      const { userId } = req;

      const documents = await Documents.findDocumentsByGraphId(id, userId);

      for (let i = 0; i < documents.length; i++) {
        await documents[i].destroy();
      }

      const graph = await Graphs.findUserAssociatedGraph(id, userId);
      if (!graph) {
        throw HttpErrors(404);
      }

      await graph.destroy();

      Utils.unlinkDirSync(Directories.THUMBNAILS_DIR(id));
      Utils.unlinkDirSync(Directories.ICONS_DIR(id));
      Utils.unlinkDirSync(Directories.FILES_DIR(id));

      res.json({
        status: 'ok',
      });
    } catch (e) {
      next(e);
    }
  };

  static actionsCount = async (req, res, next) => {
    try {
      const { id } = req.params;
      const { userId } = req;

      const graph = await Graphs.findOne({
        where: {
          id,
        },
      });

      if (!graph) {
        throw HttpErrors(404);
      }
      const sharesCount = await Share.count({
        where: {
          graphId: id,
          status: 'shared',
        },
      });

      const commentsCount = await CommentGraphs.count({
        where: {
          graphId: id,
        },
      });

      res.json({
        status: 'ok',
        result: {
          [id]: {
            shares: sharesCount,
            comments: commentsCount,
            views: 0,
            likes: 0,
          },
        },
      });
    } catch (e) {
      next(e);
    }
  };

  static updateData = async (req, res, next) => {
    try {
      await validate(req.body, {
        title: 'required|string',
      });
      const {
        title, description,
      } = req.body;
      const { id } = req.params;
      const { userId } = req;
      const graph = await Graphs.findUserAssociatedGraph(id, userId);
      if (!graph) {
        throw HttpErrors(404);
      }

      await Graphs.update({
        title: title || 'untitled',
        description,
      }, {
        where: {
          id,
        },
      });

      res.json({
        status: 'ok',
        graphId: graph.id,
      });
    } catch (e) {
      next(e);
    }
  };

  static compare = async (req, res, next) => {
    try {
      const { id1, id2 } = req.params;

      res.json({
        status: 'ok',
      });
    } catch (e) {
      next(e);
    }
  };

  static copy = async (req, res, next) => {
    try {
      await validate(req.body, {
        square: 'required|object',
        'square.width': 'required|numeric',
        'square.height': 'required|numeric',
        'square.x': 'required|numeric',
        'square.y': 'required|numeric',
        type: 'string',
      });
      const { userId } = req;
      const { id } = req.params;
      const {
        square: {
          width, height, x, y,
        },
      } = req.body;
      const graph = await Graphs.findUserAssociatedGraph(id, userId, {
        userRole: ['edit', 'admin', 'edit_inside'],
      });
      if (!graph) {
        throw HttpErrors(403);
      }
      const minXArr = [];
      const minYArr = [];
      let nodes = graph.nodes.filter((d) => !d.fake && d.fx >= x && d.fx <= x + width && d.fy >= y && d.fy <= y + height);

      const links = Utils.cleanLinks(graph.links, nodes);

      let labels = graph.labels.filter((l) => nodes.some((n) => n.labels.includes(l.id)));

      nodes.forEach((n) => {
        minXArr.push(n.fx);
        minYArr.push(n.fy);
      });
      labels.forEach((l) => {
        if (l.type === 'folder') {
          minXArr.push(l.d[0][0]);
          minYArr.push(l.d[0][1]);
        } else {
          if (l.type === 'ellipse' || l.type === 'square') {
            minXArr.push(l.size.x);
            minYArr.push(l.size.y);
          } else {
            minXArr.push(...l.d.map((p) => p[0]));
            minYArr.push(...l.d.map((p) => p[1]));
          }
        }
      });
      const minX = _.min(minXArr);
      const minY = _.min(minYArr);

      nodes = nodes.map((n) => {
        n.fx -= minX;
        n.fy -= minY;

        return n;
      });
      labels = labels.map((l) => {
        if (l.type === 'folder') {
          l.d[0][0] -= minX;
          l.d[0][1] -= minY;
        } else {
          if (l.type === 'ellipse' || l.type === 'square') {
            l.size.x -= minX;
            l.size.y -= minY;
          } else {
            l.d = l.d.map((p) => {
              p[0] -= minX;
              p[1] -= minY;
              return p;
            });
          }
        }
        return l;
      });

      res.json({
        status: 'ok',
        data: {
          sourceId: +id,
          type: 'square',
          labels,
          nodes,
          links,
          title: graph.title,
        },
      });
    } catch (e) {
      next(e);
    }
  };

  static pastCompare = async (req, res, next) => {
    try {
      const { userId } = req;
      const { id: graphId } = req.params;
      const { nodes } = req.body;
      const graph = await Graphs.findUserAssociatedGraph(graphId, userId, {
        full: true,
        userRole: ['edit', 'admin', 'edit_inside'],
      });
      if (!graph) {
        throw HttpErrors(403);
      }

      const sourceNodes = _.intersectionBy(nodes, graph.nodes, 'name');
      const duplicatedNodes = _.intersectionBy(graph.nodes, nodes, 'name');

      res.json({
        status: 'ok',
        duplicatedNodes,
        sourceNodes,
      });
    } catch (e) {
      next(e);
    }
  };

  static past = async (req, res, next) => {
    try {
      await validate(req.body, {
        // ...schema.labels,
        // ...schema.nodes,
        // ...schema.links,
        position: 'required|array',
        'position.0': 'required|numeric',
        'position.1': 'required|numeric',
        action: 'string',
        merge: 'object',
        'merge.source': 'array',
        'merge.source.*': 'string',
        'merge.duplications': 'array',
        'merge.duplications.*': 'string',
        sourceId: 'numeric',
      });
      const { userId } = req;
      const {
        position, labels = [], nodes = [], action, merge, sourceId,
      } = req.body;
      let links = req.body.links || [];
      const { id: graphId } = req.params;
      const graph = await Graphs.findUserAssociatedGraph(graphId, userId, {
        full: true,
        userRole: ['edit', 'admin', 'edit_inside'],
        raw: true,
        nest: true,
      });
      const isEmbed = !!sourceId;
      const isSkip = action === 'skip';
      const isReplace = action === 'replace';
      const isMerge = action === 'merge' || action === 'merge-compare';

      if (!graph) {
        throw HttpErrors(403);
      }

      let sourceGraph;
      if (isEmbed) {
        if (labels.length !== 1) {
          throw HttpErrors(422);
        }
        if (graph.labels.some((l) => l.id === labels[0].id)) {
          throw HttpErrors(422, 'Label already exists');
        }
        sourceGraph = await Graphs.findByPk(sourceId);
        if (!sourceGraph) {
          throw HttpErrors(404, 'Source Graph not found');
        }
      }

      const [posX, posY] = position;

      // const positionsX = [];
      // const positionsY = [];
      // labels.forEach((l) => {
      //   if (l.type !== 'folder') {
      //     positionsX.push(...l.d.map((p) => p[0]));
      //     positionsY.push(...l.d.map((p) => p[1]));
      //   }
      // });
      // positionsX.push(...nodes.map((n) => n.fx));
      // positionsY.push(...nodes.map((n) => n.fy));
      //
      // const minX = Math.min(...positionsX);
      // const minY = Math.min(...positionsY);
      const minX = 0;
      const minY = 0;

      const labelOriginalIds = {};
      await Promise.map(labels, async (label) => {
        if (isEmbed) {
          label.readOnly = true;
          label.sourceId = sourceId;
          labelOriginalIds[label.id] = label.id;
        } else {
          const uniqId = await Utils.graphUniqueId(graph, false);
          graph.lastUid = uniqId.lastUid;
          let labelId = uniqId.id;
          if (label.type === 'folder') {
            labelId = `f_${labelId}`;
          }
          labelOriginalIds[label.id] = labelId;

          label.id = labelId;
          delete label.color;
        }

        if (label.type === 'folder') {
          label.d[0][0] = label.d[0][0] - minX + posX;
          label.d[0][1] = label.d[0][1] - minY + posY;
          label.open = true;
        } else {
          if (label.type === 'square' || label.type === 'ellipse') {
            label.size.x = label.size.x - minX + posX;
            label.size.y = label.size.y - minY + posY;
          } else {
            label.d = label.d.map((i) => {
              i[0] = i[0] - minX + posX;
              i[1] = i[1] - minY + posY;
              return i;
            });
          }
        }
      });

      graph.labels.push(...labels);

      const createNodes = [];
      const updateNodes = [];
      const deleteNodes = [];
      const updateLinks = [];
      const documents = [];

      await Promise.map(nodes, async (d) => {
        // if (merge) {
        //   isMerge = merge.sources.includes(d.id);
        // }
        if (isSkip && graph.nodes.some((n) => n.name === d.name)) {
          return;
        }
        d.fx = d.fx - minX + posX;
        d.fy = d.fy - minY + posY;
        let id;
        if (isEmbed || isReplace) {
          id = d.id;
        } else if (isMerge) {
          const duplicateNode = graph.nodes.find((n) => n.name === d.name);
          id = duplicateNode ? duplicateNode.id : d.id;
        } else {
          const uniqId = await Utils.graphUniqueId(graph, false);
          graph.lastUid = uniqId.lastUid;
          id = uniqId.id;
        }

        if (isEmbed) {
          d.readOnly = true;
          d.sourceId = sourceId;
        } else {
          d.labels = d.labels.map((l) => {
            if (labelOriginalIds[l]) {
              return labelOriginalIds[l];
            }
            return l;
          });
        }
        if (links) {
          links = links.map((l) => {
            if (l.source === d.id) {
              l.source = id;
              l.sx = l.sx - minX + posX;
              l.sy = l.sy - minY + posY;
            } else if (l.target === d.id) {
              l.target = id;
              l.tx = l.tx - minX + posX;
              l.ty = l.ty - minY + posY;
            }
            return l;
          });
        }
        d.originalId = d.id;
        d.id = id;
        d.name = (isReplace || isMerge || isEmbed) ? d.name : Utils.nodeUniqueName(d, graph.nodes);
        if (isReplace) {
          graph.nodes = graph.nodes.map((n) => {
            if (n.name === d.name) {
              updateNodes.push(d);
              return d;
            }
            return n;
          });
        } else if (isMerge) {
          let create = true;
          graph.nodes = graph.nodes.map((n) => {
            if (n.name === d.name) {
              if (action === 'merge' || (merge.sources.includes(d.originalId) && merge.duplications.includes(n.id))) {
                d = Utils.graphObjectMerge(n, d);
              }
              create = false;
              updateNodes.push(d);
              return d;
            }
            return n;
          });
          if (create) {
            createNodes.push(d);
            graph.nodes.push(d);
          }
        } else {
          createNodes.push(d);
          graph.nodes.push(d);
        }

        d.customFields = await Promise.map(d.customFields || [], async (field) => {
          const dir = Directories.FILES_DIR(graphId.toString());
          const { html, files } = await Utils.downloadFilesFromHtml2(field.value, dir, undefined, 'url');
          files.forEach((file) => {
            documents.push({
              userId,
              nodeId: d.id,
              nodeType: d.type,
              nodeName: d.name,
              graphId,
              tabName: field.name,
              data: file,
              description: '',
              altText: '',
              tags: [],
              type: mime.lookup(path.extname(file)),
            });
          });
          field.value = html;
          return field;
        });
      });

      links = await Promise.map(links, async (l) => {
        l.id = (await Utils.graphUniqueId(graph, false)).id;
        if (isEmbed) {
          l.readOnly = true;
          l.sourceId = sourceId;
        }
        return l;
      });

      if (links) {
        graph.links.push(...links);
        graph.links = Utils.cleanLinks(graph.links, graph.nodes);
        graph.links = Utils.uniqueLinks(graph.links);
      }

      const eventId = uuidv4();

      const p1 = Socket.emitSharedUsers(graphId, graph.userId, null, 'node.create', {
        graphId, nodes: createNodes, userId, eventId,
      });

      const p2 = Socket.emitSharedUsers(graphId, graph.userId, null, 'node.update', {
        graphId, nodes: updateNodes, userId, eventId,
      });

      const p3 = Socket.emitSharedUsers(graphId, graph.userId, null, 'node.delete', {
        graphId, nodes: deleteNodes, userId, eventId,
      });

      const pLink4 = Socket.emitSharedUsers(graphId, graph.userId, null, 'link.create', {
        graphId, links, userId, eventId,
      });
      const pLink5 = Socket.emitSharedUsers(graphId, graph.userId, null, 'link.update', {
        graphId, links: updateLinks, userId, eventId,
      });
      const p6 = Socket.emitSharedUsers(graphId, graph.userId, null, 'label.create', {
        graphId, labels, userId, eventId,
      });

      const pUpdate = Graphs.update({
        nodes: graph.nodes,
        links: graph.links,
        labels: graph.labels,
        lastUid: graph.lastUid,
      }, {
        where: {
          id: graphId,
        },
        individualHooks: true,
      });

      const pDocuments = documents.length ? Documents.bulkCreate(documents) : undefined;

      await Promise.all([p1, p2, p3, p6, pUpdate, pDocuments]);
      await Promise.all([pLink4, pLink5]);

      if (isEmbed) {
        const { userId: ownerId } = sourceGraph;
        const [labelEmbed] = await GraphLabelsEmbed.findOrCreate({
          where: {
            graphId,
            sourceId,
            ownerId,
            userId,
            labelId: Object.keys(labelOriginalIds)[0],
          },
          defaults: {
            graphId,
            sourceId,
            ownerId,
            userId,
            labelId: Object.keys(labelOriginalIds)[0],
          },
        });
        Socket.emit(ownerId, 'labelEmbedCopy', labelEmbed, sourceId);

        const sharedGraphs = await ShareGraphs.findAll({
          where: {
            graphId: sourceId,
            status: 'shared',
          },
        });
        sharedGraphs.forEach((d) => {
          Socket.emit(d.userId, 'labelEmbedCopy', labelEmbed, sourceId);
        });
      }

      res.json({
        status: 'ok',
        update: {
          nodes: updateNodes,
        },
        create: {
          nodes: createNodes,
          links,
          labels,
        },
      });
    } catch (e) {
      next(e);
    }
  };

  static getAllTabsByGraphId = async (req, res, next) => {
    try {
      const { id } = req.params;
      const { userId } = req;
      const graph = await Graphs.findUserAssociatedGraph(id, userId, {
        userRole: ['edit', 'edit_inside', 'admin', 'view'],
      });

      const { nodes } = graph;

      const graphTabs = nodes.map((p) => ({ nodeId: p.id, tab: p.customFields }));

      res.json({
        status: 'ok',
        graphTabs,
      });
    } catch (e) {
      next(e);
    }
  };
}

export default GraphController;
