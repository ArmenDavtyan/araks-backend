import HttpErrors from 'http-errors';
import Promise from 'bluebird';
import _ from 'lodash';
import moment from 'moment';
import Sequelize from 'sequelize';
import validate from '../services/validate';
import {
  Documents, GraphLabelsEmbed, Graphs, ShareGraphs,
} from '../models';
import Socket from '../services/Socket';
import Utils from '../services/Utils';
import schema from '../services/schema';
import eventType from '../helpers/EventType';
// import GraphsHistory from './GraphHistoryController';

class LabelController {
  static create = async (req, res, next) => {
    try {
      await validate(req.body, {
        'labels.*.id': 'required|string',
        ...schema.labels,
      });
      const { userId } = req;
      let { labels } = req.body;
      const { graphId } = req.params;
      const graph = await Graphs.findUserAssociatedGraph(graphId, userId, {
        full: true,
        userRole: ['edit', 'admin', 'edit_inside'],
      });
      if (!graph) {
        throw HttpErrors(403);
      }

      labels = labels.filter((n) => !graph.labels.some((d) => d.id === n.id));

      graph.labels.push(...labels);

      await Socket.emitSharedUsers(graphId, graph.userId, userId, 'label.create', {
        graphId: graph.id, labels, userId,
      });

      await Graphs.update({
        labels: graph.labels,
      }, {
        where: {
          id: graphId,
        },
        individualHooks: true,
      });
      // history
      // GraphsHistory.insertData({
      //   graphsId: graphId,
      //   eventType: eventType.LabelCreate,
      //   userId,
      //   data: labels,
      //   eventDate: moment().utc().unix(),
      // });
      res.json({
        status: 'ok',
        labels,
      });
    } catch (e) {
      next(e);
    }
  };

  static update = async (req, res, next) => {
    try {
      if (!req.body.name) {
        await validate(req.body, {
          'label.*.d': 'required|array',
        });
      } else {
        await validate(req.body, {
          'labels.*.id': 'required|string',
          ...schema.labels,
        });
      }
      const { userId } = req;
      let { labels } = req.body;
      const { graphId } = req.params;

      const graph = await Graphs.findUserAssociatedGraph(graphId, userId, {
        full: true,
        userRole: ['edit', 'admin', 'edit_inside'],
      });
      if (!graph) {
        throw HttpErrors(403);
      }

      const updateNodes = [];

      labels = labels.map((label) => {
        const labelIndex = graph.labels.findIndex((n) => n.id === label.id);
        if (labelIndex === -1) {
          throw HttpErrors(422, 'invalid label');
        }
        const originalLabel = graph.labels[labelIndex];

        if (label.type === 'folder' && !label.open) {
          graph.nodes = graph.nodes.map((d) => {
            if (d.labels.includes(label.id)) {
              d.fx = d.fx - originalLabel.d[0][0] + label.d[0][0];
              d.fy = d.fy - originalLabel.d[0][1] + label.d[0][1];
              updateNodes.push(d);
            }
            return d;
          });
        }

        if (!label.name) {
          label = {
            ...originalLabel,
            d: label.d || [],
          };
        }

        graph.labels[labelIndex] = label;

        return label;
      });

      await Graphs.update({
        labels: graph.labels,
        nodes: graph.nodes,
      }, {
        where: {
          id: graphId,
        },
        individualHooks: true,
      });

      await Socket.emitSharedUsers(graphId, graph.userId, userId, 'label.update', {
        graphId: graph.id, labels, userId,
      });

      if (updateNodes.length) {
        await Socket.emitSharedUsers(graphId, graph.userId, userId, 'node.update-positions', {
          graphId: graph.id, nodes: updateNodes, userId,
        });
      }
      // history
      // GraphsHistory.insertData({
  //       graphsId: graphId,
  //       eventType: eventType.LabelUpdate,
  //       userId,
  //       data: labels,
  //       eventDate: moment().utc().unix(),
  //     });
      res.json({
        status: 'ok',
        labels,
      });
    } catch (e) {
      next(e);
    }
  };

  static labelToggle = async (req, res, next) => {
    try {
      await validate(req.body, {
        'label.id': 'required',
        'label.open': 'required|boolean',
      });
      const { userId } = req;
      let { label } = req.body;
      const { graphId } = req.params;

      const graph = await Graphs.findUserAssociatedGraph(graphId, userId, {
        full: true,
        userRole: ['edit', 'admin', 'edit_inside'],
      });
      if (!graph) {
        throw HttpErrors(403);
      }

      graph.labels = graph.labels.map((l) => {
        if (l.id === label.id) {
          l.open = label.open;
          label = l;
        }
        return l;
      });

      await Graphs.update({
        labels: graph.labels,
      }, {
        where: {
          id: graphId,
        },
        individualHooks: true,
      });

      await Socket.emitSharedUsers(graphId, graph.userId, userId, 'label.toggle', {
        graphId: graph.id, label, userId,
      });

      res.json({
        status: 'ok',
        label,
      });
    } catch (e) {
      next(e);
    }
  };

  // deprecated
  static updatePosition = async (req, res, next) => {
    try {
      await validate(req.body, {
        'label.*.id': 'required|string',
        'label.*.d': 'required|array',
      });
      const { userId } = req;
      let { labels } = req.body;
      const { graphId } = req.params;

      const graph = await Graphs.findUserAssociatedGraph(graphId, userId, {
        full: true,
        userRole: ['edit', 'admin', 'edit_inside'],
      });
      if (!graph) {
        throw HttpErrors(403);
      }

      const updateNodes = [];
      const sqlLabels = [];
      const sqlNodes = [];
      labels = labels.map((label) => {
        const i = graph.labels.findIndex((n) => n.id === label.id);
        if (i === -1) {
          throw HttpErrors(422, 'invalid label');
        }
        const originalLabel = graph.labels[i];

        if (label.type === 'folder' && !label.open) {
          graph.nodes = graph.nodes.map((d, j) => {
            if (d.labels.includes(label.id)) {
              d.fx = d.fx - originalLabel.d[0][0] + label.d[0][0];
              d.fy = d.fy - originalLabel.d[0][1] + label.d[0][1];
              updateNodes.push(d);
              sqlNodes.push(`'$[${j}].fx', ${d.fx}`);
              sqlNodes.push(`'$[${j}].fy', ${d.fy}`);
            }
            return d;
          });
        }
        sqlLabels.push(`'$[${i}].d', JSON_EXTRACT('${JSON.stringify(label.d)}', '$')`);
        return label;
      });

      const values = {
        labels: Sequelize.literal(`JSON_REPLACE(labels, ${sqlLabels.join(', ')})`),
      };

      if (sqlNodes.length) {
        values.nodes = Sequelize.literal(`JSON_REPLACE(nodes, ${sqlNodes.join(', ')})`);
      }

      await Graphs.update(values, {
        where: {
          id: graphId,
        },
        individualHooks: true,
      });

      await Socket.emitSharedUsers(graphId, graph.userId, userId, 'label.update-positions', {
        graphId: graph.id, labels, nodes: updateNodes, userId,
      });

      // history
      // GraphsHistory.insertData({
      //   graphsId: graphId,
      //   eventType: eventType.LabelUpdate,
      //   userId,
      //   data: labels,
      //   eventDate: moment().utc().unix(),
      // });
      res.json({
        status: 'ok',
        labels,
      });
    } catch (e) {
      next(e);
    }
  };

  static delete = async (req, res, next) => {
    try {
      const { userId } = req;
      const { graphId } = req.params;
      let { labels } = req.body;
      const graph = await Graphs.findUserAssociatedGraph(graphId, userId, {
        full: true,
        userRole: ['edit', 'admin', 'edit_inside'],
      });
      if (!graph) {
        throw HttpErrors(403);
      }

      labels = labels.map((label) => {
        const labelId = label.id || label;
        const labelIndex = graph.labels.findIndex((n) => n.id === labelId);
        if (labelIndex === -1) {
          throw HttpErrors(422, 'invalid label');
        }
        const deletedLabel = graph.labels[labelIndex];
        graph.labels.splice(labelIndex, 1);

        graph.nodes = graph.nodes.filter((d) => !(d.labels || []).includes(labelId));

        return deletedLabel;
      });

      graph.links = Utils.cleanLinks(graph.links, graph.nodes);

      await Socket.emitSharedUsers(graphId, graph.userId, userId, 'label.delete', {
        graphId: graph.id,
        labels,
        userId,
      });

      await Graphs.update({
        labels: graph.labels,
        nodes: graph.nodes,
        links: graph.links,
      }, {
        where: {
          id: graphId,
        },
      });
      // history
      // GraphsHistory.insertData({
      //   graphsId: graphId,
      //   eventType: eventType.LabelDelete,
      //   userId,
      //   data: labels,
      //   eventDate: moment().utc().unix(),
      // });
      res.json({
        status: 'ok',
      });
    } catch (e) {
      next(e);
    }
  };

  static pastCompare = async (req, res, next) => {
    try {
      const { userId } = req;
      const { graphId } = req.params;
      const { nodes } = req.body;
      const graph = await Graphs.findUserAssociatedGraph(graphId, userId, {
        full: true,
        userRole: ['edit', 'admin', 'edit_inside'],
      });
      if (!graph) {
        throw HttpErrors(403);
      }

      const sourceNodes = _.intersectionBy(nodes, graph.nodes, 'name');
      const duplicatedNodes = _.intersectionBy(graph.nodes, nodes, 'name');

      res.json({
        status: 'ok',
        duplicatedNodes,
        sourceNodes,
      });
    } catch (e) {
      next(e);
    }
  };

  static copy = async (req, res, next) => {
    try {
      const { graphId, labelId } = req.params;
      const { userId } = req;
      const graph = await Graphs.findUserAssociatedGraph(graphId, userId, {
        userRole: ['edit', 'admin', 'edit_inside'],
      });
      if (!graph) {
        throw HttpErrors(403);
      }
      const label = graph.labels.find((l) => l.id === labelId);
      if (!label) {
        throw HttpErrors(404);
      }
      let nodes = graph.nodes.filter((n) => !n.fake && (n.labels || []).includes(labelId)).map((d) => {
        d.labels = [labelId];
        return d;
      });

      const links = Utils.cleanLinks(graph.links, nodes);

      const minXArr = [];
      const minYArr = [];
      nodes.forEach((n) => {
        minXArr.push(n.fx);
        minYArr.push(n.fy);
      });

      if (label.type === 'folder') {
        minXArr.push(label.d[0][0]);
        minYArr.push(label.d[0][1]);
      } else {
        if (label.type === 'square' || label.type === 'ellipse') {
          minXArr.push(label.size.x);
          minYArr.push(label.size.y);
        } else {
          minXArr.push(...label.d.map((p) => p[0]));
          minYArr.push(...label.d.map((p) => p[1]));
        }
      }
      const minX = _.min(minXArr);
      const minY = _.min(minYArr);

      nodes = nodes.map((n) => {
        n.fx -= minX;
        n.fy -= minY;

        return n;
      });
      if (label.type === 'folder') {
        label.d[0][0] -= minX;
        label.d[0][1] -= minY;
      } else {
        if (label.type === 'square' || label.type === 'ellipse') {
          label.size.x -= minX;
          label.size.y -= minY;
        } else {
          label.d = label.d.map((p) => {
            p[0] -= minX;
            p[1] -= minY;
            return p;
          });
        }
      }

      res.json({
        status: 'ok',
        data: {
          sourceId: +graphId,
          type: 'label',
          labels: [label],
          nodes,
          links,
          title: graph.title,
        },
      });
    } catch (e) {
      next(e);
    }
  };

  // deprecated  todo remove me
  static past = async (req, res, next) => {
    try {
      await validate(req.body, {
        ...schema.label,
        ...schema.nodes,
        ...schema.links,

        merge: 'object',
        'merge.source': 'array',
        'merge.source.*': 'string',
        'merge.duplications': 'array',
        'merge.duplications.*': 'string',
        position: 'required|array',
        'position.0': 'required|numeric',
        'position.1': 'required|numeric',
        action: 'string',
        sourceId: 'numeric',
      });
      const { userId } = req;
      const { graphId } = req.params;
      let {
        label, links, nodes, position, sourceId, action, merge,
      } = req.body;
      const graph = await Graphs.findUserAssociatedGraph(graphId, userId, {
        full: true,
        userRole: ['edit', 'admin', 'edit_inside'],
        raw: true,
        nest: true,
      });
      if (!graph) {
        throw HttpErrors(403);
      }
      let sourceGraph;

      if (sourceId) {
        sourceGraph = await Graphs.findByPk(sourceId);
        if (!sourceGraph) {
          throw HttpErrors(404, 'Source Graph not found');
        }
      }

      const isEmbed = !!sourceId;
      const isSkip = action === 'skip';
      const isReplace = action === 'replace';
      let isMerge = action === 'merge';

      const labelOriginalId = label.id;
      let labelId = label.id;

      if (isEmbed) {
        if (graph.labels.some((l) => l.id === label.id)) {
          throw HttpErrors(422, 'Label already exists');
        }
        label.readOnly = true;
        label.sourceId = sourceId;
      } else {
        const uniqId = await Utils.graphUniqueId(graph, false);
        graph.lastUid = uniqId.lastUid;
        labelId = uniqId.id;
        if (label.type === 'folder') {
          labelId = `f_${labelId}`;
        }
        label.id = labelId;
      }

      graph.labels.push(label);

      const [posX, posY] = position;
      let minX = Math.min(...label.d.map((l) => l[0]));
      let minY = Math.min(...label.d.map((l) => l[1]));

      if (label.type === 'folder') {
        // eslint-disable-next-line prefer-destructuring
        [minX, minY] = label.d[0];
        label.d[0][0] = posX;
        label.d[0][1] = posY;
      } else {
        label.d = label.d.map((i) => {
          i[0] = i[0] - minX + posX;
          i[1] = i[1] - minY + posY;
          return i;
        });
      }
      delete label.color;

      const createNodes = [];
      const updateNodes = [];
      const deleteNodes = [];
      const updateLinks = [];
      await Promise.map(nodes, async (d) => {
        if (merge) {
          isMerge = graph.nodes.some((n) => merge.duplications.includes(n.id) && d.name === n.name);
        }
        if (isSkip && graph.nodes.map((n) => n.name === d.name)) {
          return;
        }
        d.fx = d.fx - minX + posX;
        d.fy = d.fy - minY + posY;
        const originalId = d.id;
        let id;
        if (isReplace) {
          id = d.id;
        } else if (isMerge) {
          id = graph.nodes.find((n) => n.name === d.name).id;
        } else {
          const uniqId = await Utils.graphUniqueId(graph, false);
          graph.lastUid = uniqId.lastUid;
          id = uniqId.id;
        }

        if (isEmbed) {
          d.readOnly = true;
          d.sourceId = sourceId;
          links = links.map((l) => {
            if (l.source === d.id) {
              l.sx = l.sx - minX + posX;
              l.sy = l.sy - minY + posY;
            } else if (l.target === d.id) {
              l.tx = l.tx - minX + posX;
              l.ty = l.ty - minY + posY;
            }
            return l;
          });
        } else {
          d.labels = d.labels.map((l) => {
            if (l === labelOriginalId) {
              return labelId;
            }
            return l;
          });
          links = Promise.map(links, async (l) => {
            if (l.source === originalId) {
              l.source = id;
              l.sx = l.sx - minX + posX;
              l.sy = l.sy - minY + posY;
            } else if (l.target === originalId) {
              l.target = id;
              l.tx = l.tx - minX + posX;
              l.ty = l.ty - minY + posY;
            }
            return l;
          });
          d.id = id;
        }

        d.name = (isReplace || isMerge || isEmbed) ? d.name : Utils.nodeUniqueName(d, graph.nodes);
        d.labels = [labelId];

        if (isReplace) {
          graph.nodes = graph.nodes.map((n) => {
            if (n.name === d.name) {
              deleteNodes.push(_.cloneDeep(n));
              const oldId = n.id;
              graph.links = graph.links.map((l) => {
                let update = false;
                if (l.source === oldId) {
                  update = true;
                  l.source = id;
                }
                if (l.target === oldId) {
                  update = true;
                  l.target = id;
                }
                if (update) {
                  updateLinks.push(l);
                }
                return l;
              });
              return d;
            }
            return n;
          });

          updateNodes.push(d);
        } else if (isMerge) {
          graph.nodes = graph.nodes.map((n) => {
            if (n.name === d.name) {
              if (merge && merge.sources.includes(n.id)) {
                d = Utils.graphObjectMerge(d, n);
              }
              updateNodes.push(d);
              return d;
            }
            return n;
          });
        } else {
          createNodes.push(d);
          graph.nodes.push(d);
        }
      });

      links = links.map((d) => {
        if (isEmbed) {
          d.readOnly = true;
          d.sourceId = sourceId;
        }
        return d;
      });
      graph.links.push(...links);

      graph.links = Utils.cleanLinks(graph.links, graph.nodes);
      graph.links = Utils.uniqueLinks(graph.links);

      const p1 = Socket.emitSharedUsers(graphId, graph.userId, null, 'node.create', {
        graphId, nodes: createNodes, userId,
      });

      const p2 = Socket.emitSharedUsers(graphId, graph.userId, null, 'node.update', {
        graphId, nodes: updateNodes, userId,
      });

      const p3 = Socket.emitSharedUsers(graphId, graph.userId, null, 'node.delete', {
        graphId, nodes: deleteNodes, userId,
      });

      const p4 = Socket.emitSharedUsers(graphId, graph.userId, null, 'link.create', {
        graphId, links, userId,
      });
      const p5 = Socket.emitSharedUsers(graphId, graph.userId, null, 'link.update', {
        graphId, links: updateLinks, userId,
      });

      const p6 = Socket.emitSharedUsers(graphId, graph.userId, null, 'label.create', {
        graphId, labels: [label], userId,
      });

      const pUpdate = Graphs.update({
        nodes: graph.nodes,
        links: graph.links,
        labels: graph.labels,
        lastUid: graph.lastUid,
      }, {
        where: {
          id: graphId,
        },
        individualHooks: true,
      });
      await Promise.all([p1, p2, p3, p4, p5, p6, pUpdate]);

      if (isEmbed) {
        const { userId: ownerId } = sourceGraph;
        const [labelEmbed] = await GraphLabelsEmbed.findOrCreate({
          where: {
            graphId,
            sourceId,
            ownerId,
            userId,
            labelId: labelOriginalId,
          },
          defaults: {
            graphId,
            sourceId,
            ownerId,
            userId,
            labelId: labelOriginalId,
          },
        });
        Socket.emit(ownerId, 'labelEmbedCopy', labelEmbed, sourceId);

        const sharedGraphs = await ShareGraphs.findAll({
          where: {
            graphId: sourceId,
            status: 'shared',
          },
        });
        sharedGraphs.forEach((d) => {
          Socket.emit(d.userId, 'labelEmbedCopy', labelEmbed, sourceId);
        });
        console.log(222);
      }

      res.json({
        status: 'ok',
        update: {
          nodes: updateNodes,
        },
        create: {
          nodes: createNodes,
          links,
          labels: [label],
        },
      });
    } catch (e) {
      next(e);
    }
  };

  static getLabelNodes = async (req, res, next) => {
    try {
      const { userId } = req;
      const { graphId, labelId } = req.params;
      const graph = await Graphs.findUserAssociatedGraph(graphId, userId, {
        userRole: ['edit', 'admin', 'edit_inside'],
      });
      if (!graph) {
        throw HttpErrors(403);
      }

      const nodes = graph.nodes.filter((d) => (d.labels || []).includes(labelId));

      const links = graph.links.filter((l) => nodes.some((n) => l.source === n.id) || nodes.some((n) => l.target === n.id));

      const label = graph.labels.find((d) => d.id === labelId);
      if (!label) {
        throw HttpErrors(404);
      }
      label.nodes = nodes.map((d) => d.id);

      const fakeData = {
        node: {},
        links: [],
      };
      if (label.id.startsWith('f_')) {
        const fakeId = `fake_${label.id}`;
        fakeData.node = {
          id: fakeId,
          fx: label.d[0][0] + 30,
          fy: label.d[0][1] + 30,
          fake: true,
          labels: [label.id],
        };
        nodes.forEach((node) => {
          graph.links.forEach((link) => {
            if (link.source === node.id) {
              fakeData.links.push({
                ...link,
                _source: link.source,
                source: fakeId,
                fake: true,
              });
            } else if (link.target === node.id) {
              fakeData.links.push({
                ...link,
                _target: link.target,
                target: fakeId,
                fake: true,
              });
            }
          });
        });

        graph.nodes.push({
          id: fakeId,
          fx: label.d[0][0] + 30,
          fy: label.d[0][1] + 30,
          fake: true,
          labels: [label.id],
        });
      }

      res.json({
        status: 'ok',
        label: {
          nodes,
          links,
          label,
          fakeData,
        },
      });
    } catch (e) {
      next(e);
    }
  };
}

export default LabelController;
