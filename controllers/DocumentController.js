import Promise from 'bluebird';
import { fn, col } from 'sequelize';
import Utils from '../services/Utils';

import {
  Documents, Users, Graphs, ShareGraphs,
} from '../models';

class DocumentController {
  static copyDocumentForGraph = async (req, res, next) => {
    try {
      const {
        graphId,
        toGraphId,
        nodes,
      } = req.body;

      const ids = [];

      nodes.forEach((node) => {
        ids.push(node.originalId);
      });

      const documents = await Documents.findDocumentsByNodeIds(parseInt(graphId), ids);

      await Promise.map(documents, (doc) => {
        Utils.copyNodeFiles(doc.data, toGraphId);

        doc.nodeId = nodes.filter((p) => p.originalId === doc.nodeId)[0].id;

        doc.graphId = toGraphId;

        return Documents.create({
          userId: doc.userId,
          nodeId: doc.nodeId,
          nodeType: doc.nodeType,
          nodeName: doc.nodeName,
          graphId: doc.graphId,
          tabName: doc.tabName,
          data: doc.data,
          description: doc.description,
          altText: doc.altText,
          tags: doc.tags,
          type: doc.type,
        });
      });

      res.json({
        status: 'ok',
        documents,
      });
    } catch (e) {
      next(e);
    }
  };

  static getDocumentsByGraphId = async (req, res, next) => {
    try {
      const {
        graphId,
      } = req.query;
      const { userId } = req;

      const documents = await Documents.findDocumentsByGraphId(parseInt(graphId), userId);

      res.json({
        status: 'ok',
        documents,
      });
    } catch (e) {
      next(e);
    }
  };

  static findPictures = async (req, res, next) => {
    try {
      const {
        tag,
      } = req.query;

      const { userId } = req;

      let pictures = await Documents.findAll({

        where: {
          $or: [
            { nodeName: { $like: `%${tag}%` } },
            { nodeType: { $like: `%${tag}%` } },
            { $or: fn('JSON_CONTAINS', col('documents.tags'), `"${tag}"`) },
          ],
        },
        include: [{
          model: Users,
          as: 'user',
        },
          {
            model: Graphs,
            as: 'graphs',
          },
        ],
        limit: 20,
      });

      const graphAllShares = await ShareGraphs.findAll({
        where: {
          userId,
        },
        include: [
          {
            model: Users,
            as: 'user',
            attributes: ['email'],
          },
        ],
      });

      pictures = pictures.filter((p) => p.type.includes('image')
        && (p.userId === userId
          || graphAllShares.find((s) => s.graphId === p.graphId)));

      res.json({
        status: 'ok',
        pictures,
      });
    } catch (e) {
      next(e);
    }
  };

  static findDocuments = async (req, res, next) => {
    try {
      const {
        tag,
      } = req.query;

      let documents = await Documents.findAll({
        where: {
          $or: [
            { nodeName: { $like: `%${tag}%` } },
            { nodeType: { $like: `%${tag}%` } },
            { $or: fn('JSON_CONTAINS', col('documents.tags'), `"${tag}"`) },
          ],
        },
        include: [{
          model: Users,
          as: 'user',
        },
          {
            model: Graphs,
            as: 'graphs',
          }],
        limit: 10,
      });

      const graphAllShares = await ShareGraphs.findAll({
        where: {
          userId,
        },
        include: [
          {
            model: Users,
            as: 'user',
            attributes: ['email'],
          },
        ],
      });

      documents = documents.filter((p) => !p.type.includes('image')
        && (p.userId === userId
          || graphAllShares.find((s) => s.graphId === p.graphId)));

      res.json({
        status: 'ok',
        documents,
      });
    } catch (e) {
      next(e);
    }
  };
}

export default DocumentController;
