// const esconfig = require('../services/elastic/esConfig');
// const esb = require('elastic-builder'); //the builder
// const client = esconfig.esClient;
// const config = require('../services/elastic/config');
// const { default: EventType } = require('../helpers/EventType');
// const index = config.es_index;
// const type = config.es_type;
// const limit  =  config.es_limit

// module.exports = {
//   /**
//    * Return graphs history data by graph id
//    * @param {*} graphsId 
//    */
//   async search( graphsId){
//     const requestBody = esb.requestBodySearch()
//     .query(
//       esb.boolQuery()
//        .must(esb.matchPhraseQuery('graphsId', graphsId))
//      ).sort(esb.sort('eventDate', 'desc'));
//     return client.search({index: index, body: requestBody.toJSON(), size: limit});
//   },
//   async getEventId(graphsId){
         
//     const requestBody = esb.requestBodySearch()

//     .agg(
//       esb.termsAggregation('graphsId', graphsId)
       
//         .agg(esb.termsAggregation('max', 'eventId'))
     


 
//           // .filter(esb.termsQuery('eventType', [EventType.NodeCreate, EventType.NodeUpdate])) 
//       ); 
//     return client.search({index: index, body: requestBody.toJSON()});
//   },
//   /**
//     * Insert data in index elasticsearch
//     * @param {*} data 
//     */
//     async  insertData(data){  
//       try {
//         const body  =  [{ index: { _index: index, _type: type } }, data]
//         return  await client.bulk({
//             refresh: true, body
//           });
//         }catch (e) {
//         console.log(e);
//       }
//     },
    
//     /**
//      * Filter and return data for Node history
//      * @param {*} graphsId 
//      * @param {*} nodeId 
//      */
//   async filterGraphsByNodeMade(graphsId, nodeId) { 
//     const requestBody = esb.requestBodySearch()
//       .query(
//         esb.boolQuery()
//           .must(esb.matchPhraseQuery('graphsId', graphsId))
//           .must(esb.matchPhraseQuery('data.id', nodeId))
//           .filter(esb.termsQuery('eventType', [EventType.NodeCreate, EventType.NodeUpdate])) 
//       ).sort(esb.sort('eventDate', 'desc')); 
//     return client.search({index: index, body: requestBody.toJSON(), size: limit});
//   }, 
//    /**
//      * Filter and return data for  count node chnage positions
//      * @param {*} graphsId 
//      * @param {*} nodeId 
//      */
//     async filterGraphsByNodePositions(graphsId, nodeId) { 
//       const requestBody = esb.requestBodySearch()
//         .query(
//           esb.boolQuery()
//             .must(esb.matchPhraseQuery('graphsId', graphsId))
//             .must(esb.matchPhraseQuery('data.id', nodeId))
//             .must(esb.matchPhraseQuery('eventType', EventType.updatePositions)) 
//         ); 
//       return client.count({index: index, body: requestBody.toJSON()});
//     },
//    /**
//      * Filter and return data for count tabs view
//      * @param {*} graphsId 
//      * @param {*} nodeId 
//      */
//     async filterGraphsByNodeTabsView(graphsId, nodeId) { 
//       const requestBody = esb.requestBodySearch()
//         .query(
//           esb.boolQuery()
//             .must(esb.matchPhraseQuery('graphsId', graphsId))
//             .must(esb.matchPhraseQuery('data.id', nodeId))
//             .must(esb.matchPhraseQuery('eventType', EventType.NodeUpdateCustomFieldsView)) 
//         ); 
//       return client.count({index: index, body: requestBody.toJSON()});
//     }, 
     
//   /**
//      * Filter and return data for count tabs view
//      * @param {*} graphsId 
//      * @param {*} nodeId 
//      */
//     async filterGraphsByLink(graphsId) { 
//       const requestBody = esb.requestBodySearch()
//         .query(
//           esb.boolQuery()
//             .must(esb.matchPhraseQuery('graphsId', graphsId))
//            // .must(esb.matchPhraseQuery('data.id', nodeId))
//             .must(esb.matchPhraseQuery('eventType', EventType.LabelCreate)) 
//         ); 
//       return client.search({index: index, body: requestBody.toJSON(), size: limit});
//     }, 
     
//     groupByEventDate(data){
//       const  groupBy = keys => array =>
//         array.reduce((objectsByKeyValue, obj) => {
//           const value = keys.map(key => obj[key]).join('-');
//           objectsByKeyValue[value] = (objectsByKeyValue[value] || []).concat(obj);
//           return objectsByKeyValue;
//         }, {});

         
//         const eventDate = groupBy(['eventDate']);

//      return eventDate(data);
//     } 
// };