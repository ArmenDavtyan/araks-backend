import { DataTypes, Model } from 'sequelize';
import db from '../services/db';
import Graphs from './Graphs';
import Users from './Users';

class AccessTokens extends Model {

}

AccessTokens.init({
  id: {
    type: DataTypes.BIGINT.UNSIGNED,
    primaryKey: true,
    allowNull: false,
    autoIncrement: true,
  },
  token: {
    type: DataTypes.STRING,
    allowNull: false,
  },
}, {
  sequelize: db,
  tableName: 'access_tokens',
  modelName: 'accessTokens',
});

AccessTokens.belongsTo(Graphs, {
  onUpdate: 'cascade',
  onDelete: 'cascade',
  foreignKey: 'graphId',
  as: 'graph',
});

Graphs.hasMany(AccessTokens, {
  onUpdate: 'cascade',
  onDelete: 'cascade',
  foreignKey: 'graphId',
  as: 'accessTokens',
});

AccessTokens.belongsTo(Users, {
  onUpdate: 'cascade',
  onDelete: 'cascade',
  foreignKey: 'userId',
  as: 'user',
});

Users.hasMany(AccessTokens, {
  onUpdate: 'cascade',
  onDelete: 'cascade',
  foreignKey: 'userId',
  as: 'accessTokens',
});

export default AccessTokens;
