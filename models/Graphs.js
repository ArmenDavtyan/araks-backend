import { DataTypes, literal, Model } from 'sequelize';
import { v4 as uuidv4 } from 'uuid';
import _ from 'lodash';
import db from '../services/db';
import Users from './Users';
import { ShareGraphs, Share } from './index';
import Utils from '../services/Utils';

class Graphs extends Model {
  static async findUserAssociatedGraph(id, userId, props = {}) {
    const params = {
      userRole: ['edit', 'admin'],
      ...props,
    };
    let graph = await Graphs.findOne({
      where: {
        id,
        userId,
      },
      raw: params.raw,
      nest: params.nest,
      include: [{
        model: Users,
        as: 'user',
      }],
    });
    if (!graph) {
      const where = {
        role: { $in: params.userRole },
        userId,
      };
      graph = await Graphs.findOne({ // todo remove me
        where: { id },
        include: [{
          model: Users,
          as: 'user',
        }, {
          model: ShareGraphs,
          as: 'shareGraph',
          where,
        }],
      });
      if (!graph) {
        if (params.type) where.type = params.type;
        if (params.objectId) where.objectId = params.objectId;

        graph = await Graphs.findOne({
          where: { id },
          include: [{
            model: Users,
            as: 'user',
          }, {
            model: Share,
            as: 'share',
            where,
          }],
        });
      }
      if (graph) {
        if (graph.shareGraph) { // todo remove me
          graph.share = graph.shareGraph;
          if (graph.setDataValue) {
            graph.setDataValue('share', graph.shareGraph);
          }
        }
        graph.currentUserRole = graph.share.role;
        if (graph.setDataValue) {
          graph.setDataValue('currentUserRole', graph.currentUserRole);
        }
      }
    } else {
      graph.currentUserRole = 'admin';
      if (graph.setDataValue) {
        graph.setDataValue('currentUserRole', 'admin');
      }
    }

    if (graph) {
      const { nodes, labels, links } = graph;
      let labelsPartial = _.cloneDeep(labels);
      let nodesPartial = _.cloneDeep(nodes);
      let linksPartial = _.cloneDeep(links);
      if (graph.share && graph.share.type === 'label') {
        labelsPartial = labelsPartial.filter((l) => l.id === graph.share.objectId);
        const labelsId = labelsPartial.map((l) => l.id);
        nodesPartial = nodesPartial.filter((d) => _.intersection(d.labels, labelsId).length);
      }
      if (graph.currentUserRole !== 'admin') {
        const lockedLabels = labelsPartial.filter((l) => l.status === 'lock').map((l) => l.id);
        nodesPartial = nodesPartial.filter((d) => !_.intersection(d.labels, lockedLabels).length);
        labelsPartial = labelsPartial.filter((l) => l.status !== 'lock');
      }
      if (graph.currentUserRole === 'view') {
        nodesPartial = nodesPartial.filter((d) => d.status !== 'draft' || +d.userId === +userId);
      }

      linksPartial = linksPartial.filter((l) => nodesPartial.some((n) => n.id === l.target) && nodesPartial.some((n) => n.id === l.source));

      if (!params.full) {
        graph.nodes = nodesPartial;
        graph.links = linksPartial;
        graph.labels = labelsPartial;
        if (graph.setDataValue) {
          graph.setDataValue('nodes', nodesPartial);
          graph.setDataValue('links', linksPartial);
          graph.setDataValue('labels', labelsPartial);
        }
      }
      graph.nodesPartial = nodesPartial;
      graph.linksPartial = linksPartial;
      graph.labelsPartial = labelsPartial;

      if (graph.setDataValue) {
        graph.setDataValue('nodesPartial', nodesPartial);
        graph.setDataValue('linksPartial', linksPartial);
        graph.setDataValue('labelsPartial', labelsPartial);
      }
    }

    return graph;
  }
}

Graphs.init({
  id: {
    type: DataTypes.BIGINT.UNSIGNED,
    primaryKey: true,
    allowNull: false,
    autoIncrement: true,
  },
  title: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  nodes: {
    type: DataTypes.JSON,
    allowNull: false,
    defaultValue: [],
  },
  infoGraphics: {
    type: DataTypes.JSON,
    allowNull: true,
    defaultValue: [],
  },
  links: {
    type: DataTypes.JSON,
    allowNull: false,
    defaultValue: [],
  },
  labels: {
    type: DataTypes.JSON,
    allowNull: false,
    defaultValue: [],
  },
  customFields: {
    type: DataTypes.JSON,
    allowNull: true, // todo change to false
    defaultValue: {},
  },
  description: {
    type: DataTypes.TEXT,
    allowNull: false,
    defaultValue: '',
  },
  status: {
    type: DataTypes.ENUM('active', 'draft', 'template'),
    allowNull: false,
    defaultValue: 'active',
  },
  thumbnail: {
    type: DataTypes.STRING, // todo change to VIRTUAL
    get() {
      const graphId = this.getDataValue('id');
      return Utils.generateGraphThumbnail(graphId, global.userId, 'url');
    },
  },
  tags: {
    type: DataTypes.JSON,
    allowNull: false,
    defaultValue: [],
  },
  token: {
    type: DataTypes.STRING(36),
    allowNull: false,
    defaultValue: () => uuidv4(),
  },
  lastUid: {
    type: DataTypes.BIGINT,
    allowNull: false,
    defaultValue: 0,
  },
  publicState: {
    type: DataTypes.BOOLEAN,
    allowNull: false,
    defaultValue: false,
  },
}, {
  sequelize: db,
  tableName: 'graphs',
  modelName: 'graphs',
});

Graphs.belongsTo(Users, {
  onUpdate: 'cascade',
  onDelete: 'cascade',
  foreignKey: 'userId',
  as: 'user',
});

Users.hasMany(Graphs, {
  onUpdate: 'cascade',
  onDelete: 'cascade',
  foreignKey: 'userId',
  as: 'graphs',
});

Graphs.addHook('afterSave', async (user, options) => {
  const { id } = options.where || {};
  if (id) {
    const data = await Graphs.findOne({
      attributes: [
        literal('nodes->"$**.id" as nodes'),
        literal('links->"$**.id" as links'),
        literal('labels->"$**.id" as labels'),
      ],
      where: { id },
      raw: true,
    });
    if (!data) {
      return;
    }
    data.nodes = (data.nodes || []).map((d) => parseInt(d));
    data.links = (data.links || []).map((d) => parseInt(d));
    data.labels = (data.labels || []).map((d) => parseInt(d.replace('f_', '')));

    Graphs.update({
      lastUid: Math.max.apply(null, [...data.nodes, ...data.links, ...data.labels]) || 0,
    }, {
      where: { id },
    });
  }
});

export default Graphs;
