import { DataTypes, Model } from 'sequelize';
import _ from 'lodash';
import db from '../services/db';
import Users from './Users';
import Graphs from './Graphs';

class Documents extends Model {
  static async findDocuments(graphId, userId, nodeId, tabName) {
    const docs = await Documents.findAll({
      where: {
        graphId,
        userId,
        nodeId,
        tabName,
      },
    });

    return docs;
  }

  static async findDocumentsByNodeIds(graphId, nodeIds) {
    const docs = await Documents.findAll({
      where: {
        graphId,
        nodeId: nodeIds,
      },
    });

    return docs;
  }

  static async findDocumentsByNodeType(graphId, userId, nodeType, tabName) {
    const docs = await Documents.findAll({
      where: {
        graphId,
        userId,
        nodeType,
        tabName,
      },
    });

    return docs;
  }

  static async findDocumentsByGraphId(graphId, userId) {
    const docs = await Documents.findAll({
      where: {
        graphId,
        // userId,
      },
      include: [
        {
          model: Graphs,
          as: 'graphs',
        },
        {
          model: Users,
          as: 'user',
        },
      ],
    });

    return docs;
  }
}

Documents.init({
  id: {
    type: DataTypes.BIGINT.UNSIGNED,
    primaryKey: true,
    allowNull: false,
    autoIncrement: true,
  },
  nodeId: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  nodeType: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  nodeName: {
    type: DataTypes.STRING,
    allowNull: true,
  },
  tabName: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  description: {
    type: DataTypes.TEXT({ length: 'long' }),
    allowNull: true,
  },
  altText: {
    type: DataTypes.STRING,
    allowNull: true,
  },
  data: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  tags: {
    type: DataTypes.JSON,
    allowNull: false,
    defaultValue: [],
  },
  type: {
    type: DataTypes.STRING,
    allowNull: false,
  },
}, {
  sequelize: db,
  tableName: 'documents',
  modelName: 'documents',
});

Documents.belongsTo(Users, {
  onUpdate: 'cascade',
  onDelete: 'cascade',
  foreignKey: 'userId',
  as: 'user',
});

Users.hasMany(Documents, {
  onUpdate: 'cascade',
  onDelete: 'cascade',
  foreignKey: 'userId',
  as: 'documents',
});

Documents.belongsTo(Graphs, {
  onUpdate: 'cascade',
  onDelete: 'cascade',
  foreignKey: 'graphId',
  as: 'graphs',
});

Graphs.hasMany(Documents, {
  onUpdate: 'cascade',
  onDelete: 'cascade',
  foreignKey: 'graphId',
  as: 'documents',
});

export default Documents;
