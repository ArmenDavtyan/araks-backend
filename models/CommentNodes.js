import { DataTypes, Model } from 'sequelize';
import db from '../services/db';
import Users from './Users';

class CommentNodes extends Model {
  static async getListData(graphId, nodeId,  page = 1) {
    const limit = 15;
    const offset = (page - 1) * limit;

    const commentNodes = await CommentNodes.findAll({
      where: {
        graphId,
        nodeId,
      },
      include: [
        {
          model: Users,
          as: 'user',
        },
      ],
      order: [
        ['updatedAt', 'DESC'],
      ],
      limit,
      offset,
    });

    const total = await CommentNodes.count({
      where: {
        graphId,
        nodeId,
      },
    });

    const totalPages = Math.ceil(total / limit);

    return {
      status: 'ok',
      commentNodes,
      page,
      total,
      totalPages,
    };
  }
}

CommentNodes.init({
  id: {
    type: DataTypes.BIGINT.UNSIGNED,
    primaryKey: true,
    allowNull: false,
    autoIncrement: true,
  },
  graphId: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  nodeId: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  text: {
    type: DataTypes.TEXT,
    allowNull: false,
  },
}, {
  sequelize: db,
  tableName: 'comment_nodes',
  modelName: 'commentNodes',
});

CommentNodes.belongsTo(Users, {
  onUpdate: 'cascade',
  onDelete: 'cascade',
  foreignKey: 'userId',
  as: 'user',
});

Users.hasMany(CommentNodes, {
  onUpdate: 'cascade',
  onDelete: 'cascade',
  foreignKey: 'userId',
  as: 'commentNodes',
});

CommentNodes.belongsTo(CommentNodes, {
  onUpdate: 'cascade',
  onDelete: 'cascade',
  foreignKey: 'parentId',
  as: 'parent',
});

CommentNodes.hasMany(CommentNodes, {
  onUpdate: 'cascade',
  onDelete: 'cascade',
  foreignKey: 'parentId',
  as: 'commentNodes',
});

export default CommentNodes;
