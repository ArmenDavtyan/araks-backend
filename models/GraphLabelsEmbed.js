import { DataTypes, Model } from 'sequelize';

import db from '../services/db';
import Graphs from './Graphs';
import Users from './Users';

class GraphLabelsEmbed extends Model {
  static async findWithGraph(options) {
    const labelEmbed = await GraphLabelsEmbed.findOne(options);
    if (!labelEmbed) {
      return labelEmbed;
    }
    const { labelId } = labelEmbed;
    const graph = await Graphs.findByPk(labelEmbed.sourceId);
    const nodes = graph.nodes
      .filter((n) => n.labels.includes(labelId))
      .filter((d) => d.status !== 'draft' || +d.userId === +labelEmbed.userId);
    const label = graph.labels
      .find((l) => l.id === labelId);

    const links = graph.links
      .filter((l) => nodes.some((n) => n.id === l.target) && nodes.some((n) => n.id === l.source));

    labelEmbed.setDataValue('nodes', nodes);
    labelEmbed.setDataValue('label', label);
    labelEmbed.setDataValue('links', links);
    return labelEmbed;
  }
}

GraphLabelsEmbed.init({
  id: {
    type: DataTypes.BIGINT.UNSIGNED,
    primaryKey: true,
    allowNull: false,
    autoIncrement: true,
  },
  labelId: {
    type: DataTypes.STRING,
    allowNull: false,
  },
}, {
  sequelize: db,
  tableName: 'graph-labels-embed',
  modelName: 'graphLabelsEmbed',
});

GraphLabelsEmbed.belongsTo(Graphs, {
  as: 'graph',
  onUpdate: 'cascade',
  onDelete: 'cascade',
  foreignKey: 'graphId',
});
GraphLabelsEmbed.belongsTo(Graphs, {
  as: 'source',
  onUpdate: 'cascade',
  onDelete: 'cascade',
  foreignKey: 'sourceId',
});

GraphLabelsEmbed.belongsTo(Users, {
  as: 'user',
  onUpdate: 'cascade',
  onDelete: 'cascade',
  foreignKey: 'userId',
});

GraphLabelsEmbed.belongsTo(Users, {
  as: 'owner',
  onUpdate: 'cascade',
  onDelete: 'cascade',
  foreignKey: 'ownerId',
});

export default GraphLabelsEmbed;
