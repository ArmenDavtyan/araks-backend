import { DataTypes, Model, Op } from 'sequelize';
import db from '../services/db';
import Users from './Users';
import Graphs from './Graphs';

class Share extends Model {
  static async getListData(graphId, page = 1, type = 'graph') {
    const limit = 15;
    const offset = (page - 1) * limit;    
    const shareGraphs = await Share.findAll({
      where: {
        graphId,
        [Op.not]: {status: "deleted"},
        type,
      },
      include: [
        {
          model: Users,
          as: 'user',
        },
        {
          model: Graphs,
          as: 'graph',
        },
      ],
      order: [
        ['updatedAt', 'DESC'],
      ],
      limit,
      offset,
    });

    const total = await Share.count({
      where: {
        graphId,
        status: 'shared',
        type,
      },
    });

    const totalPages = Math.ceil(total / limit);

    return {
      status: 'ok',
      shareGraphs,
      page,
      total,
      totalPages,
    };
  }
}

Share.init({
  id: {
    type: DataTypes.BIGINT.UNSIGNED,
    primaryKey: true,
    allowNull: false,
    autoIncrement: true,
  },
  role: {
    type: DataTypes.ENUM('view', 'edit', 'owner', 'admin', 'edit_inside'),
    allowNull: false,
    defaultValue: 'view',
  },
  type: {
    type: DataTypes.ENUM('graph', 'label'),
    allowNull: false,
    defaultValue: 'graph',
  },
  objectId: {
    type: DataTypes.STRING,
    allowNull: true,
    defaultValue: null,
  },
  status: {
    type: DataTypes.ENUM('new', 'shared', 'deleted'),
    allowNull: false,
    defaultValue: 'new',
  },
}, {
  sequelize: db,
  tableName: 'share',
  modelName: 'share',
  indexes: [{
    unique: true,
    fields: ['graphId', 'userId', 'type', 'objectId'],
  }],
});

Share.belongsTo(Users, {
  onUpdate: 'cascade',
  onDelete: 'cascade',
  foreignKey: 'userId',
  as: 'user',
});

Users.hasOne(Share, {
  onUpdate: 'cascade',
  onDelete: 'cascade',
  foreignKey: 'userId',
  as: 'share',
});

Share.belongsTo(Graphs, {
  onUpdate: 'cascade',
  onDelete: 'cascade',
  foreignKey: 'graphId',
  as: 'graph',
});

Graphs.hasOne(Share, {
  onUpdate: 'cascade',
  onDelete: 'cascade',
  foreignKey: 'graphId',
  as: 'share',
});

export default Share;
