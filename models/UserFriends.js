import { DataTypes, Model, literal } from 'sequelize';
import db from '../services/db';
import Users from './Users';

class UserFriends extends Model {
  static async getListData(userId) {
    const friends = await UserFriends.findAll({
      attributes: [
        'id',
        'status',
        [literal(`CASE senderUserId WHEN ${userId} THEN receiverUserId ELSE senderUserId END`), 'friendUserId'],
        [literal(`CASE senderUserId WHEN ${userId} THEN true ELSE false END`), 'isSender'],
      ],
      where: [{
        $or: [
          { senderUserId: userId },
          { receiverUserId: userId },
        ],
      }],
    });

    return friends;
  }
}

UserFriends.init({
  id: {
    type: DataTypes.BIGINT.UNSIGNED,
    primaryKey: true,
    allowNull: false,
    autoIncrement: true,
  },
  status: {
    type: DataTypes.ENUM('pending', 'accepted', 'rejected'),
    allowNull: false,
    defaultValue: 'pending',
  },
}, {
  sequelize: db,
  tableName: 'user_friends',
  modelName: 'userFriends',
});

UserFriends.belongsTo(Users, {
  onUpdate: 'cascade',
  onDelete: 'cascade',
  foreignKey: 'senderUserId',
  as: 'senderUser',
});

UserFriends.belongsTo(Users, {
  onUpdate: 'cascade',
  onDelete: 'cascade',
  foreignKey: 'receiverUserId',
  as: 'receiverUser',
});

Users.hasMany(UserFriends, {
  onUpdate: 'cascade',
  onDelete: 'cascade',
  foreignKey: 'senderUserId',
  as: 'userSenders',
});

Users.hasMany(UserFriends, {
  onUpdate: 'cascade',
  onDelete: 'cascade',
  foreignKey: 'receiverUserId',
  as: 'userReceivers',
});

export default UserFriends;
