import mime from 'mime-types';
import path from 'path';
import fs from 'fs';
import { JSDOM } from 'jsdom';
import _ from 'lodash';
import { v4 as uuidv4 } from 'uuid';
import CryptoJS from 'crypto-js';
import axios from 'axios';
import sharp from 'sharp';
import replaceAsync from 'string-replace-async';
import Converter from './Converter';
import { IMAGE_MIME_TYPES } from '../data/mimeTypes';
import Directories from './Directories';
import { Graphs } from '../models';

const { NODE_ID_SECRET, THUMBNAIL_SECRET } = process.env;

class Utils {
  static sleep = (ms) => new Promise((resolve) => {
    setTimeout(() => {
      resolve(true);
    }, ms);
  })

  static copyNodeFiles(filePath, graphId) {
    const dir = Directories.FILES_DIR(graphId.toString());

    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir, { recursive: true });
    }

    const file = path.join(dir, filePath.replace(/^.*[\\\/]/, ''));

    try {
      fs.copyFileSync(path.join(__dirname, '../', Utils.urlToPath(filePath)), file);
    } catch (e) {
      console.log(e);
    }
  }

  static uploadNodeFiles(files, nodeId) {
    const uploadedFiles = {};
    if (!_.isEmpty(files)) {
      const dir = Directories.FILES_DIR(nodeId.toString());
      if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir, { recursive: true });
      }
      _.forEach(files, (base64, i) => {
        const fileBase = base64.replace(/^data:.+;base64,/, '');
        const ext = mime.extension(base64.match(/^data:(.+);/)[1]);
        const file = path.join(dir, `file_${i}.${ext}`);
        uploadedFiles[`file_${i}`] = Directories.dirToUri(file);
        return fs.writeFileSync(file, fileBase, 'base64');
      });
    }
    return uploadedFiles;
  }

  static uploadFiles(files, graphId, nodeId) {
    files = _.isArray(files) ? files : [files];
    const dir = Directories.FILES_DIR(graphId.toString());
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir, { recursive: true });
    }
    return files.map((file) => {
      const ext = path.extname(file.originalname);
      const fileName = `${nodeId}_${uuidv4()}${ext}`;
      const filePath = path.join(dir, fileName);
      fs.writeFileSync(filePath, file.buffer);
      return Directories.dirToUri(filePath);
    });
  }

  static deleteIcon(graphId, nodeId) {
    try {
      const dir = Directories.ICONS_DIR(graphId.toString());
      const icons = fs.readdirSync(dir);
      icons.forEach((icon) => {
        if (icon.startsWith(`${nodeId}_`)) {
          fs.unlinkSync(path.join(dir, icon));
        }
      });
    } catch (e) {
      //
    }
  }

  static async uploadIcon(icon, graphId, nodeId) {
    if (!icon) {
      return '';
    }
    const dir = Directories.ICONS_DIR(graphId.toString());
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir, { recursive: true });
    }
    let buffer;
    const fileName = nodeId ? `${nodeId}_${uuidv4()}` : uuidv4();

    let ext;
    if (_.isBuffer(icon.buffer)) {
      ext = IMAGE_MIME_TYPES[icon.mimetype];
      if (!ext) {
        return null;
      }
      buffer = icon.buffer;
    } else if (icon.startsWith('data:image/')) {
      const mimeType = icon.match(/^data:(.+);/)[1];
      ext = IMAGE_MIME_TYPES[mimeType];
      if (!ext) {
        return null;
      }
      buffer = Buffer.from(icon.replace(/^data:.+;base64,/, ''), 'base64');
    } else if (!icon.startsWith(`${global.uri}/public/uploads/icons/${graphId}`)) {
      const { data, headers } = await axios.get(icon, {
        responseType: 'arraybuffer',
      }).catch((e) => e);
      if (!data) {
        return null;
      }
      const mimeType = headers['content-type'];
      ext = IMAGE_MIME_TYPES[mimeType];
      if (!ext) {
        return null;
      }
      buffer = Buffer.from(data);
    }

    if (buffer && ext) {
      if (nodeId) {
        this.deleteIcon(graphId, nodeId);
      }

      const fileLarge = path.join(dir, `${fileName}.${ext}.large`);
      const file = path.join(dir, `${fileName}.${ext === 'svg' ? 'png' : ext}`);
      const metadata = await sharp(buffer).metadata();
      const p1 = sharp(buffer)
        .rotate()
        .resize({
          width: 250,
          height: 250,
        })
        .toFile(file);
      const p2 = sharp(buffer)
        .rotate()
        .resize({
          width: metadata.width < 1024 ? metadata.width : 1024,
        })
        .toFile(fileLarge);
      await Promise.all([p1, p2]);
      return Directories.dirToUri(file);
    }
    return icon;
  }

  static escRegExp(str) {
    return str
      .replace(/[|\\{}()[\]^$+*?.]/g, '\\$&')
      .replace(/-/g, '\\x2d');
  }

  static extractFilesFromHtml(html, localFiles = true) {
    const reg = new RegExp(`\\s(href|src)\\s*=\\s*"(${Utils.escRegExp(global.uri)}[^"]+|https://www.youtube.com/[^"]+)"`, 'gi');
    let m = [];
    const files = [];
    while (m = reg.exec(html)) {
      files.push(m[2]);
    }

    return files;
  }

  static async downloadFilesFromHtml(html = '', destDir, allow, resType) {
    html = await replaceAsync(html, new RegExp(`\\shref\\s*=\\s*"(${Utils.escRegExp(global.uri)}[^"]+)"`, 'gi'), async (m, url) => {
      const file = await Utils.downloadFile(url, destDir, allow, resType);
      return ` href="${file}"`;
    });
    html = await replaceAsync(html, /\shref\s*=\s*"(data:.+;base64,[^"]+)"/ig, async (m, url) => {
      const file = await Utils.downloadFile(url, destDir, allow, resType);
      return ` href="${file}"`;
    });
    html = await replaceAsync(html, /\ssrc\s*=\s*"(data:.+;base64,[^"]+)"/ig, async (m, url) => {
      const file = await Utils.downloadFile(url, destDir, allow, resType);
      return ` src="${file}"`;
    });
    html = await replaceAsync(html, new RegExp(`\\ssrc\\s*=\\s*"(${Utils.escRegExp(global.uri)}[^"]+)"`, 'gi'), async (m, url) => {
      const file = await Utils.downloadFile(url, destDir, allow, resType);
      return ` src="${file}"`;
    });

    return html;
  }

  static async downloadFilesFromHtml2(html, destDir, allow, resType) {
    const files = [];
    if (!html) {
      return {
        html,
        files,
      };
    }
    html = await replaceAsync(html, new RegExp(`\\shref\\s*=\\s*"(${Utils.escRegExp(global.uri)}[^"]+)"`, 'gi'), async (m, url) => {
      const file = await Utils.downloadFile(url, destDir, allow, resType);
      files.push(file);
      return ` href="${file}"`;
    });
    html = await replaceAsync(html, /\shref\s*=\s*"(data:.+;base64,[^"]+)"/ig, async (m, url) => {
      const file = await Utils.downloadFile(url, destDir, allow, resType);
      files.push(file);
      return ` href="${file}"`;
    });
    html = await replaceAsync(html, /\ssrc\s*=\s*"(data:.+;base64,[^"]+)"/ig, async (m, url) => {
      const file = await Utils.downloadFile(url, destDir, allow, resType);
      files.push(file);
      return ` src="${file}"`;
    });
    html = await replaceAsync(html, new RegExp(`\\ssrc\\s*=\\s*"(${Utils.escRegExp(global.uri)}[^"]+)"`, 'gi'), async (m, url) => {
      const file = await Utils.downloadFile(url, destDir, allow, resType);
      files.push(file);
      return ` src="${file}"`;
    });

    return {
      html,
      files,
    };
  }

  static async downloadFile(file, destDir, allow = null, resType) {
    if (!file || !destDir) {
      return file;
    }
    let fileName = '';
    if (!fs.existsSync(destDir)) {
      fs.mkdirSync(destDir, { recursive: true });
    }
    if (file.startsWith(global.uri) || fs.existsSync(file)) {
      const iconDir = Directories.uriToDir(file);
      const ext = path.extname(file).replace('.', '');
      if (allow && !allow.includes(ext)) {
        return '';
      }
      fileName = `${uuidv4()}.${ext}`;
      try {
        fs.copyFileSync(iconDir, path.join(destDir, fileName));
      } catch (e) {
        fileName = file;
      }
      try {
        fs.copyFileSync(`${iconDir}.large`, path.join(destDir, `${fileName}.large`));
      } catch (e) {

      }
    } else if (file.includes('base64,')) {
      const fileBase = file.replace(/^data:.+;base64,/, '');
      const ext = mime.extension(file.match(/^data:(.+);/)[1]);
      if (allow && !allow.includes(ext)) {
        return '';
      }
      fileName = `${uuidv4()}.${ext}`;
      fs.writeFileSync(path.join(destDir, fileName), fileBase, 'base64');
    } else {
      const { data, headers } = await axios.get(file, {
        responseType: 'arraybuffer',
      }).catch((e) => e);
      if (!data) {
        return '';
      }
      const mimeType = headers['content-type'];
      const ext = IMAGE_MIME_TYPES[mimeType];
      if (allow && !allow.includes(ext)) {
        return '';
      }
      const buffer = Buffer.from(data);
      fileName = `${uuidv4()}.${ext}`;
      fs.writeFileSync(path.join(destDir, fileName), buffer, 'buffer');
    }
    const filePath = path.join(destDir, fileName);
    if (resType === 'path') {
      return filePath;
    }
    if (resType === 'url') {
      return Directories.dirToUri(filePath);
    }
    return fileName;
  }

  static async cleanNodeIcons(nodes, graphId) {
    const dir = Directories.ICONS_DIR(graphId.toString());
    if (!fs.existsSync(dir)) {
      return dir;
    }
    const icons = nodes.map((d) => (d.icon ? path.basename(d.icon) : null)).flat(1);
    const fsIcons = fs.readdirSync(dir);
    fsIcons.forEach((fsIcon) => {
      if (!icons.some((i) => i === fsIcon || `${i}.large` === fsIcon)) {
        fs.unlinkSync(path.join(dir, fsIcon));
      }
    });
    return dir;
  }

  static setGraphSvgStyles(svg, size = null) {
    const svgDom = new JSDOM(svg);
    const graphSvg = svgDom.window.document.querySelector('svg');
    const wrapper = graphSvg.querySelector('.wrapper');

    wrapper.querySelectorAll('.unChecked').forEach((d) => {
      d.remove();
    });


    if (size) {
      const svgSize = {
        small: {
          width: 800,
          height: 446,
          scale: 0.5,
        },
        medium: {
          width: 1200,
          height: 669,
          scale: 0.8,
        },
        large: {
          width: 1900,
          height: 1060,
          scale: 1,
        },
      }[size];
      graphSvg.setAttribute('width', svgSize.width);
      graphSvg.setAttribute('height', svgSize.height);
    }

    const arrX = [];
    const arrY = [];
    graphSvg.querySelectorAll('.nodes .node').forEach((node) => {
      const [, x, y] = node.getAttribute('transform').match(/translate\((-?[\d.]+),\s*(-?[\d.]+)/) || [0, 0, 0];
      arrX.push(+x);
      arrY.push(+y);
      const rect = node.querySelector('rect');
      if (rect) {
        arrX.push(+x - (rect.getAttribute('width') / 2));
        arrX.push(+x + (rect.getAttribute('width') / 2));
        arrY.push(+y - (rect.getAttribute('height') / 2));
        arrY.push(+y + (rect.getAttribute('height') / 2));
      }
    });
    // graphSvg.querySelectorAll('.labels .path').forEach((l) => {
    //   if (l.type === 'folder') {
    //     arrX.push(l.d[0][0]);
    //     arrY.push(l.d[0][1]);
    //   } else {
    //     if (l.type === 'square' || l.type === 'ellipse') {
    //       arrX.push(l.size.x);
    //       arrY.push(l.size.y);
    //     } else {
    //       arrX.push(...l.d.map((p) => p[0]));
    //       arrY.push(...l.d.map((p) => p[1]));
    //     }
    //   }
    // });
    const min = [_.min(arrX), _.min(arrY)];
    const max = [_.max(arrX), _.max(arrY)];

    let originalWidth = max[0] - min[0] + 40;
    // originalWidth = originalWidth > 300 ? originalWidth : 300;
    let originalHeight = max[1] - min[1] + 40;
    // originalHeight = originalHeight > 200 ? originalWidth : 200;

    graphSvg.setAttribute('viewBox', `${min[0] - 20} ${min[1] - 20} ${originalWidth} ${originalHeight}`);
    wrapper.setAttribute('transform', 'none');

    graphSvg.style.backgroundColor = '#f2f3ff';

    wrapper.querySelectorAll('.links path').forEach((d) => {
      d.setAttribute('fill', 'transparent');
    });

    wrapper.querySelectorAll('.nodes .node text').forEach((d) => {
      d.setAttribute('font-family', 'Open Sans');
      d.setAttribute('dominant-baseline', 'middle');
      d.setAttribute('fill', '#0D0905');
      d.setAttribute('text-anchor', 'middle');
    });

    wrapper.querySelectorAll('.nodes .node :not(text)').forEach((d) => {
      d.setAttribute('stroke', 'white');
      d.setAttribute('stroke-width', '10');
    });

    wrapper.querySelectorAll('.nodes .node.withIcon :not(text)').forEach((d) => {
      d.setAttribute('stroke-width', '1.5');
    });


    wrapper.querySelectorAll('.fakeNode').forEach((d) => {
      d.setAttribute('opacity', '0');
    });

    wrapper.querySelectorAll('.folders .folderOpen .folderIconUse').forEach((d) => {
      d.setAttribute('display', 'none');
    });

    wrapper.querySelectorAll('.folders .folderOpen text').forEach((d) => {
      d.setAttribute('display', 'none');
    });

    wrapper.querySelectorAll('.nodes .hideInFolder').forEach((d) => {
      d.setAttribute('display', 'none');
    });

    wrapper.querySelectorAll('.node.infography rect').forEach((d) => {
      d.setAttribute('stroke', 'none');
      d.setAttribute('stroke-width', 0);
    });

    // wrapper.querySelector('#label-lock path').setAttribute('opacity', '0.4');

    return graphSvg.outerHTML;
  }

  static async generateThumbnail(svg, filePath, size = 'small') {
    if (!svg || !filePath) {
      return null;
    }

    // const filePath = Directories.THUMBNAILS_DIR(thumbnailName);
    const fileDir = path.dirname(filePath);

    svg = this.setGraphSvgStyles(svg, size);

    const thumbnail = await Converter.svgToPng(svg, {
      '-quality': '50',
      '-density': '100',
    });
    if (thumbnail) {
      if (!fs.existsSync(fileDir)) {
        fs.mkdirSync(fileDir, { recursive: true });
      }
      fs.writeFileSync(filePath, thumbnail);
    }
    return filePath;
  }

  static formatMeta(document, metaName) {
    if (document) {
      if (_.isArray(document)) return document.map((doc) => formatMeta(doc));
      return formatMeta(document);
    }
    return document;

    function formatMeta(doc) {
      if (doc[metaName]) {
        const meta = {};
        doc[metaName].forEach((m) => {
          meta[m.key] = m.value;
        });
        doc.dataValues.meta = meta;

        doc.meta = meta;

        delete doc.dataValues[metaName];
      }

      return doc;
    }
  }

  static hexToRgb(hex, array = false) {
    const arrBuff = new ArrayBuffer(4);
    const vw = new DataView(arrBuff);
    vw.setUint32(0, parseInt(hex, 16), false);
    const arrByte = [...new Uint8Array(arrBuff)];
    arrByte.shift();
    if (array) {
      return arrByte;
    }
    return `rgb(${arrByte.join(',')})`;
  }

  static urlToPath(url) {
    return url.replace(global.uri, '');
  }

  static unlinkDirSync(dir) {
    if (fs.existsSync(dir)) {
      fs.readdirSync(dir).forEach((file) => {
        const curPath = path.join(dir, file);
        if (fs.lstatSync(curPath).isDirectory()) {
          this.unlinkDirSync(curPath);
        } else {
          fs.unlinkSync(curPath);
        }
      });
      fs.rmdirSync(dir);
    }
  }

  static graphIdEncode(graphId) {
    const keyutf = CryptoJS.enc.Utf8.parse(THUMBNAIL_SECRET);
    const iv = CryptoJS.enc.Base64.parse(THUMBNAIL_SECRET);
    return CryptoJS.AES.encrypt(String(graphId), keyutf, { iv }).toString();
  }

  static graphIdDecode(cipherGraphId) {
    const keyutf = CryptoJS.enc.Utf8.parse(THUMBNAIL_SECRET);
    const iv = CryptoJS.enc.Base64.parse(THUMBNAIL_SECRET);

    const desc = CryptoJS.AES.decrypt({ ciphertext: CryptoJS.enc.Base64.parse(cipherGraphId) }, keyutf, { iv });
    return CryptoJS.enc.Utf8.stringify(desc);
  }

  static generateGraphThumbnail(graphId, userId, mode = 'path') {
    const thumbnail = this.graphIdEncode(graphId);
    if (mode === 'url') {
      return `${global.uri}/public/uploads/thumbnails/${userId}/${graphId}_${thumbnail}.png`;
    }
    return Directories.THUMBNAILS_DIR(String(userId), `${graphId}_${thumbnail}.png`);
  }

  static getGraphThumbnail(graphId, userId, mode = 'path') {
    const graphIdEncode = this.graphIdEncode(graphId);
    const thumbnail = graphIdEncode.split('').map((d) => d.charCodeAt()).join('.');
    if (mode === 'url') {
      return `${global.uri}/public/uploads/thumbnails/${userId}/${thumbnail}.png`;
    }
    return Directories.THUMBNAILS_DIR(String(userId), `${thumbnail}.png`);
  }

  static getGraphIdFromThumbnail(thumbnail) {
    if (!thumbnail) return thumbnail;
    const graphIdEncode = this.getEncodedGraphIdFromThumbnail(thumbnail);
    const graphId = this.graphIdDecode(graphIdEncode);
    return +graphId;
  }

  static getEncodedGraphIdFromThumbnail(thumbnail) {
    const graphIdEncodeArr = path.basename(thumbnail).replace(/\.png$/, '').split('.');
    return graphIdEncodeArr.map((c) => String.fromCharCode(+c)).join('');
  }

  static nodeIdEncode(nodeId) {
    return CryptoJS.AES.encrypt(nodeId, NODE_ID_SECRET).toString();
  }

  static nodeIdDecode(cipherNodeId) {
    return CryptoJS.AES.decrypt(cipherNodeId, NODE_ID_SECRET).toString(CryptoJS.enc.Utf8);
  }

  static template(html, params) {
    try {
      return _.template(html)(params);
    } catch (e) {
      const [, fileName] = e.message.match(/^(file_\d+)/) || [];
      if (fileName) {
        params[fileName] = '';
        return this.template(html, params);
      }
      return html;
    }
  }

  static uniqueLinks(links, excludeTypes = false) {
    return _.uniqBy(links, (l) => {
      if (l.direction) {
        return JSON.stringify({
          1: l.name, 2: (excludeTypes ? '' : l.type), 3: l.source, 4: l.target,
        });
      }
      return JSON.stringify({
        1: l.name, 2: (excludeTypes ? '' : l.type), 3: [l.source, l.target].sort(),
      });
    });
  }

  static isLinkUnique(link, links) {
    return !links.some((l) => {
      if (links.id === l.id) {
        return true;
      }
      if (l.direction) {
        return JSON.stringify({
          2: link.type, 3: link.source, 4: link.target,
        }) === JSON.stringify({
          2: l.type, 3: l.source, 4: l.target,
        });
      }
      return JSON.stringify({
        2: link.type, 3: [link.source, link.target].sort(),
      }) === JSON.stringify({
        2: l.type, 3: [l.source, l.target].sort(),
      });
    });
  }

  static cleanLinks(links, nodes) {
    return links.filter((l) => nodes.some((n) => l.source === n.id) && nodes.some((n) => l.target === n.id));
  }

  static differenceNested(object, base) {
    function changes(_object, _base) {
      return _.transform(_object, (result, value, key) => {
        if (!_.isEqual(value, _base[key])) {
          result[key] = (_.isObject(value) && _.isObject(_base[key])) ? changes(value, _base[key]) : value;
        }
      });
    }

    return changes(object, base);
  }

  static async graphUniqueId(grap, updateDB = true) {
    const ids = [0];
    console.time(JSON.stringify({
      nodes: grap.nodes.length,
      links: grap.links.length,
      labels: grap.labels.length,
    }));
    const pushIds = (data) => {
      data.forEach((d) => {
        if (d.sourceId) {
          return;
        }
        const [_id = 0] = /[\d.]+/.exec(d.id) || [];
        const id = +String(_id).split('.')[0];
        if (id) {
          ids.push(id);
        }
      });
    };
    pushIds(grap.nodes);
    pushIds(grap.links);
    pushIds(grap.labels);

    const max = _.max(ids);

    if (grap.lastUid < max) {
      grap.lastUid = max;
    }
    grap.lastUid += 1;

    if (updateDB) {
      await Graphs.update({ lastUid: grap.lastUid }, { where: { id: grap.id } });
    }
    console.timeEnd(JSON.stringify({
      nodes: grap.nodes.length,
      links: grap.links.length,
      labels: grap.labels.length,
    }));

    return {
      id: `${grap.lastUid}.${grap.id}`,
      lastUid: grap.lastUid,
    };
  }

  static nodeUniqueName(node, data) {
    const nodes = data.filter((n) => n.id !== node.id
      && (node.name === n.name || new RegExp(`^${Utils.escRegExp(node.name)}_\\d+$`).test(n.name)));
    if (!nodes.length) {
      return node.name;
    }
    const max = _.max(nodes.map((n) => +(n.name.match(/_(\d+)$/) || [0, 0])[1])) || 0;
    return `${node.name}_${max + 1}`;
  }

  static graphObjectMerge(d1, d2) {
    const customFields = d1.customFields || [];
    (d2.customFields || []).forEach((f) => {
      const i = d1.customFields.findIndex((d) => d.name === f.name);
      const value1 = i > -1 ? d1.customFields[i].value : undefined;
      const value2 = f.value;
      if (value1 && !value2) {
        f.value = value1;
      } else if (!value1 && value2) {
        f.value = value2;
      } else if (value1 && value2) {
        if (value1 !== value2) {
          f.value = `${value1}\n<hr />\n${value2}`;
        } else {
          f.value = value2;
        }
      }
      if (i > -1) {
        customFields[i].value = f.value;
        customFields[i].subtitle = customFields[i].subtitle || f.subtitle;
      } else {
        customFields.push(f);
      }
    });
    const data = { ...d1, ...d2, customFields };
    for (const i in data) {
      if (!data[i]) {
        data[i] = d1[i];
      }
    }
    return data;
  }
}

export default Utils;
