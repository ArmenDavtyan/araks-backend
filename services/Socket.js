import socketIo from 'socket.io';
import socketIoJwt from 'socketio-jwt';
import _ from 'lodash';
import Promise from 'bluebird';
import { literal, Op, col } from 'sequelize';
import {
  Graphs, GraphLabelsEmbed, ShareGraphs, Share, Users,
} from '../models';

const { JWT_SECRET } = process.env;

class Socket {
  static init(server) {
    this.io = socketIo(server);
    this.io.use(socketIoJwt.authorize({
      secret: JWT_SECRET,
      handshake: true,
    }));
    this.io.on('connection', this.#handleConnection);
    this.connections = [];
    this.online = [];
    this.mousemove = [];
    this.mouseMoveTracker = [];
  }

  static #handleConnection = (socket) => {
    const {
      id,
      decoded_token: { userId, userIP },
      request: { connection: { remoteAddress } },
    } = socket;
    if (!userId || userIP !== remoteAddress) {
      return;
    }
    this.connections.push({
      id,
      userId,
      socket,
    });

    if (!this.online.includes(userId)) {
      this.online.push({ id, userId });
    }
    this.io.emit('online', JSON.stringify(this.online));
    socket.on('disconnect', this.#handleDisconnect(socket, userId));
    socket.on('mousemove', this.#mousemove(socket, userId));
    socket.on('mouseMoveTracker', this.#mouseMoveTracker(socket, userId));
    socket.on('labelDataChange', this.#handleSocketLabelDataChange(socket, userId));
    socket.on('setActiveGraph', this.#handleSetActiveGraph(socket, userId));
  };

  static #handleDisconnect = (socket) => () => {
    const { id } = socket;
    this.connections = this.connections.filter((con) => con.id !== id);
    this.online = this.online.filter((con) => con.id !== id);
    this.io.emit('online', JSON.stringify(this.online));
  };

  static #mousemove = (socket) => async (props) => {
    const { graphId, userId, mousePosition } = props;
    const { firstName, lastName } = await Users.getCustomDataById(userId);
    this.mousemove = this.mousemove.filter((mov) => mov.userId !== userId);

    if (!this.mousemove.includes(userId)) {
      this.mousemove.push({
        graphId, userId, mousePosition, firstName, lastName,
      });
    }

    this.io.emit('mousemoving', JSON.stringify(this.mousemove));
  };

  static #mouseMoveTracker = (socket) => async (props) => {
    const { graphId, tracker, userId } = props;
    this.mouseMoveTracker = this.mouseMoveTracker.filter((mov) => mov.userId !== userId);

    if (!this.mouseMoveTracker.includes(userId)) {
      this.mouseMoveTracker.push({ graphId, tracker, userId });
    }
    this.io.emit('mouseMoveTracker', JSON.stringify(this.mouseMoveTracker));
  };

  static #handleSetActiveGraph = (socket) => (props) => {
    const { graphId } = props;
    this.connections.forEach((c, i) => {
      if (c.id === socket.id) {
        this.connections[i].activeGraphId = +graphId;
        this.online[i].activeGraphId = +graphId;
      }
    });
    this.io.emit('online', JSON.stringify(this.online));
  };

  static #handleSocketLabelDataChange = (socket, userId) => async (props) => {
    const { sourceId, label } = props;
    const graph = await Graphs.findUserAssociatedGraph(sourceId, userId);
    if (!graph) {
      return;
    }
    const labelsEmbed = await GraphLabelsEmbed.findAll({
      where: {
        labelId: label.id,
        sourceId,
      },
    });
    labelsEmbed.forEach((embed) => {
      this.emit(embed.userId, 'embedLabelDataChange', props);
    });
  };

  static emit = (userId, key, params = {}, graphId = null) => {
    this.connections.forEach((con) => {
      if (+con.userId === +userId) {
        if (!graphId || con.activeGraphId === +graphId) {
          con.socket.emit(key, params);
        }
      }
    });
  };

  static emitAll = (key, params) => { // don't recommended
    this.io.sockets.emit(key, params);
  };

  static emitSharedUsers = async (graphId, graphUserId, currentUserId, key, data = {}) => {
    let shared = await Share.findAll({
      where: {
        graphId,
        [Op.not]: { status: 'deleted' },
      },
    });
    shared.push({ userId: graphUserId });
    shared = _.compact(shared);

    let graph = {};
    if (shared.some((s) => s.role === 'edit_inside')) {
      graph = await Graphs.findOne({
        where: {
          id: graphId,
        },
        attributes: ['links'],
      });
    }

    shared.forEach((share) => {
      if (share.role === 'edit_inside') {
        if (['label.create'].includes(key)) {
          return;
        }
        const { objectId } = share;
        const d = _.cloneDeep(data);
        let updateLinks = [];
        if (d.nodes) {
          d.nodes = d.nodes.filter((node) => {
            if (!node.labels.includes(share.objectId)) {
              this.emit(share.userId, 'node.delete', {
                graphId, nodes: [node], userId: d.userId, eventId: d.eventId,
              });
              return false;
            }
            return true;
          });
          updateLinks = graph.links.filter((l) => d.nodes.some((n) => n.id === l.source || n.id === l.target));
        }

        if (+share.userId !== +currentUserId) {
          this.emit(share.userId, key, d);
          if (updateLinks.length) {
            this.emit(share.userId, 'link.create', {
              graphId, links: updateLinks, userId: d.userId, eventId: d.eventId,
            });
          }
        }

        return;
      }
      if (+share.userId !== +currentUserId) {
        this.emit(share.userId, key, data);
      }
    });

    return shared;
  };

  static async emitEmbeddedUsers(graph) {
    const labelIds = graph.labels.filter((l) => !l.sourceId).map((l) => l.id);
    const embeddedLabels = await Promise.map(labelIds, (labelId) => (
      GraphLabelsEmbed.findWithGraph({
        where: {
          labelId,
          sourceId: graph.id,
        },
      })
    ));
    _.compact(embeddedLabels).forEach((embed) => {
      Socket.emit(embed.userId, 'embedLabelDataChange', embed);
    });
  }
}

export default Socket;
