import { Client } from '@elastic/elasticsearch';

const elastic = new Client({
  node: 'https://elastic.analysed.ai/elastic',
  sniffOnStart: true,
});

export default elastic;
run();

async function run() {
  try {
    await elastic.indices.create({
      index: 'tweets',
      method: 'POST',
      body: {
        mappings: {
          properties: {
            id: { type: 'integer' },
            text: { type: 'text' },
            user: { type: 'text' },
            time: { type: 'date' },
          },
        },
      },
    }, { ignore: [400] });
    const dataset = [{
      id: 1,
      text: 'If I fall, don\'t bring me back.',
      user: 'jon',
      date: new Date(),
    }, {
      id: 2,
      text: 'Winter is coming',
      user: 'ned',
      date: new Date(),
    }, {
      id: 3,
      text: 'A Lannister always pays his debts.',
      user: 'tyrion',
      date: new Date(),
    }, {
      id: 4,
      text: 'I am the blood of the dragon.',
      user: 'daenerys',
      date: new Date(),
    }, {
      id: 5, // change this value to a string to see the bulk response with errors
      text: 'A girl is Arya Stark of Winterfell. And I\'m going home.',
      user: 'arya',
      date: new Date(),
    }];

    const body = dataset.flatMap((doc) => [{ index: { _index: 'tweets' } }, doc]);
    const { body: bulkResponse } = await elastic.bulk({ refresh: true, body });
    // console.log(bulkResponse);
    const { body: data } = await elastic.search({ index: 'tweets' });
    console.log(data.hits);
  } catch (e) {
    console.log(e);
  }
  process.exit(0);
}
