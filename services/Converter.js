import _ from 'lodash';
import PDFDocument from 'pdfkit';
import Promise from 'bluebird';
import SVGtoPDF from 'svg-to-pdfkit';
import path from 'path';
import moment from 'moment';
import fse from 'fs-extra';
import fs from 'fs';
import { v4 as uuidv4 } from 'uuid';
import { PDFImage } from 'pdf-image';
import xlsx from 'xlsx';
import xlsxStyle from 'xlsx-style';
import replaceAsync from 'string-replace-async';
import HTMLToPDF from 'convert-html-to-pdf';
import captureWebsite from 'capture-website';
import puppeteer from 'puppeteer';
import Csv from './Csv';
import Utils from './Utils';
import Directories from './Directories';
import LinkedinPdfParser from './LinkedinPdfParser';
import { Graphs } from '../models';

String.prototype.splice = function (idx, rem, str) {
  return this.slice(0, idx) + str + this.slice(idx + Math.abs(rem));
};

class Converter {
  static htmlToPdf(html) {
    html = html.replace(/\\/g, '');

    const htmlTemplate = fs.readFileSync(path.join(__dirname, '../helpers/exportTemplate.html'), 'utf8')
      .toString();

    const startContent = htmlTemplate.indexOf('contentPlace');

    let result = htmlTemplate.splice(startContent, 12, html);

    result = result.splice(result.indexOf('timePlace'), 9, moment().format('YYYY-MM-DD hh:mm:ss'));

    const htmlToPDF = new HTMLToPDF(result, {
      waitForNetworkIdle: false,
      browserOptions: {
        args: [
          '--no-sandbox',
          '--disable-setuid-sandbox',
        ],
      },
    });

    return htmlToPDF.convert();
  }

  static async svgToPdf(svg) {
    return this.svgToFile(svg, 'pdf');
  }

  static async svgToPng(svg) {
    return this.svgToFile(svg, 'png');
  }

  static async svgToFile(svg, type) {
    const widthM = svg.match(/\swidth="(\d+)"/) || [];
    const width = +widthM[1] || 1500;

    const height = width / 1.875;

    const browser = await puppeteer.launch({
      args: ['--no-sandbox', '--disable-setuid-sandbox'],
    });
    const page = await browser.newPage();

    await page.setContent(svg, {
      waitUntil: 'networkidle2',
    });

    await page.addStyleTag({
      content: `
      @import url('https://fonts.googleapis.com/css2?family=Open+Sans&display=swap');
       *{margin: 0}
       text {font-family: "Open Sans", sans-serif}
       .folderBadge {
            transform: translate(5px, 45px);
            fill: #ffffff !important;
            font-size: 14px;
            pointer-events: none;
        }
    `,
    });

    let buffer;
    if (type === 'pdf') {
      buffer = await page.pdf({
        printBackground: true,
        width,
        height,
        pageRanges: '1',
        margin: 'none',
      });
    } else {
      buffer = await page.screenshot({
        printBackground: true,
        fullPage: true,
      });
    }

    await browser.close();

    return buffer;
  }

  static async svgToPdf2(svg, params) {
    const configs = {
      margin: 0,
      ...params,
    };
    const widthM = svg.match(/\swidth="(\d+)"/) || [];
    const width = +widthM[1] || 1500;

    const heightM = svg.match(/\sheight="(\d+)"/) || [];
    const height = +heightM[1] || 800;

    const w = width + (configs.margin * 2);
    const h = height + (configs.margin * 2);

    const doc = new PDFDocument({
      bufferPages: true,
      size: [w, h],
      align: 'center',
      valign: 'center',
    });

    const buffers = [];
    doc.on('data', buffers.push.bind(buffers));
    const promise = new Promise((resolve) => {
      doc.on('end', () => {
        const pdfData = Buffer.concat(buffers);
        resolve(pdfData);
      });
    });

    doc.rect(0, 0, w, h).fill('#f0f2fb');

    const tempId = uuidv4();
    svg = await replaceAsync(svg, /(<image[^>]+(xlink:|)href=)(["'])(.*?)\3/g, async (...matches) => {
      const icon = await Utils.uploadIcon(matches[4], tempId);
      return matches[1] + matches[3] + icon + matches[3];
    });

    SVGtoPDF(doc, svg, configs.margin, configs.margin, {
      warningCallback: () => {
      },
      assumePt: true,
      useCSS: true,
      fontCallback: () => path.join(__dirname, '../public/fonts/OpenSans-Regular1.ttf'),
      imageCallback: (url) => {
        const imgPath = url.replace(global.uri, '');
        return path.join(__dirname, '..', imgPath);
      },
    });
    doc.end();

    const pdf = await promise;

    fs.rmdirSync(Directories.ICONS_DIR(tempId), { recursive: true });

    return pdf;
  }

  static async svgToPng2(svg, options = {}) {
    const pdfPath = path.join(__dirname, `/../temp/${uuidv4()}.pdf`);
    try {
      const pdf = await this.svgToPdf(svg, { margin: 0 });

      fs.writeFileSync(pdfPath, pdf);
      const pdfImage = new PDFImage(pdfPath, {
        convertOptions: {
          '-flatten': '',
          '-quality': '75',
          '-density': '200',
          ...options,
        },
      });
      const imagePath = await pdfImage.convertPage(0);

      const image = fs.readFileSync(imagePath);

      fs.unlinkSync(imagePath);
      fs.unlinkSync(pdfPath);

      return image;
    } catch (e) {
      if (fs.existsSync(pdfPath)) {
        fs.unlinkSync(pdfPath);
      }
      e.message = `${e.message} ( ${e.stderr} ) `;
      throw e;
    }
  }

  static async pdfToNode(file) {
    const { filename } = file;

    const pdfFile = path.join(__dirname, `/../temp/${filename}`);

    const node = await LinkedinPdfParser.run(pdfFile);

    return {
      type: 'person',
      name: _.get(node, 'basics.name'),
      location: _.get(node, 'basics.location'),
      address: _.get(node, 'basics.location.address'),
      website: null,
      photo: null,
      phone: null,
      summary: (_.get(node, 'summary')),
      work: (_.get(node, 'work')),
      skills: (_.get(node, 'skills')),
      education: (_.get(node, 'education')),
    };
  }

  static async xlsxToGraph(file, graphId) {
    const iconsDir = Directories.THUMBNAILS_DIR(graphId);
    const filesDir = Directories.FILES_DIR(graphId);
    if (_.isString(file) && fs.lstatSync(file).isDirectory()) {
      const dir = file;
      file = path.join(dir, 'graph.xlsx');
      const tempIconDir = path.join(dir, 'icon');
      if (fs.existsSync(tempIconDir)) {
        fse.moveSync(tempIconDir, Directories.THUMBNAILS_DIR(graphId), { overwrite: true });
      }
      const tempFileDir = path.join(dir, 'files');
      if (fs.existsSync(tempFileDir)) {
        fse.moveSync(tempFileDir, Directories.FILES_DIR(graphId), { overwrite: true });
      }
    }
    const workBook = _.isBuffer(file) ? xlsx.read(file) : xlsx.readFile(file);
    const {
      Nodes: nodesSheet,
      Links: linksSheet,
      Edges: linksSheetAlt,
      Labels: labelsSheet,
    } = workBook.Sheets;

    const nodesIdRelation = {};

    let i = (await Graphs.findByPk(graphId, {
      attributes: ['lastUid'],
    })).lastUid || 1;
    i += 1;
    // nodes
    let nodes = xlsx.utils.sheet_to_json(nodesSheet, { defval: '' });
    nodes = nodes.map((d) => {
      let icon = d.Icon || d.Image;
      if (icon && !icon.includes('/')) {
        icon = Directories.dirToUri(path.join(iconsDir, icon));
      }
      const description = (d.Description || '').replace(/\s(src|href)\s*=\s*"([^"]+)"/g, (m, type, url) => {
        if (!url.includes('/')) {
          url = Directories.dirToUri(path.join(filesDir, url));
        }
        return ` ${type}="${url}"`;
      });
      const id = `${i++}.${graphId}`;
      nodesIdRelation[d.Id] = id;
      nodesIdRelation[d.Name] = id;

      let customFields;
      try {
        customFields = JSON.parse(d.Tabs) || [];
      } catch (e) {
        customFields = [];
      }

      customFields = customFields.map((f) => {
        f.value = (f.value || '').replace(/\s(src|href)\s*=\s*"([^"]+)"/g, (m, type, url) => {
          if (!url.includes('/')) {
            url = Directories.dirToUri(path.join(filesDir, url));
          }
          return ` ${type}="${url}"`;
        });

        return f;
      });
      return {
        id,
        name: d.Name,
        type: d.Type,
        description,
        nodeType: d['Node Type'] || d.NodeType,
        icon,
        link: d.Link || d.Reference,
        keywords: (d.Keywords || '').split(', ').map((k) => k.trim()).filter((k) => k),
        location: d.Location ? JSON.stringify(d.Location) : [],
        fx: +d.Fx || undefined,
        fy: +d.Fy || undefined,
        x: +d.Fx || 0,
        y: +d.Fy || 0,
        d: d.D ? d.D.split('|').map((p) => p.split(',').map((n) => +n)) : undefined,
        scale: d.scale ? d.scale.split(',') : undefined,
        customFields,
      };
    }).filter((d) => d.name);

    // nodes = await Promise.object(nodes, 'icon', 20);
    // nodes = await Promise.object(nodes, 'description', 20);

    // links
    let links = xlsx.utils.sheet_to_json(linksSheet);
    if (_.isEmpty(links)) {
      links = xlsx.utils.sheet_to_json(linksSheetAlt);
    }

    links = links.map((d) => {
      const source = d.Source || d['From Name'];
      const target = d.Target || d['To Name'];
      return {
        id: `${i++}.${graphId}`,
        type: d.Type || d.Edge,
        source: nodesIdRelation[source],
        target: nodesIdRelation[target],
        value: d.Value || d.Weight,
        linkType: d['Link Type'] || d.LinkType,
        direction: d.direction ? 1 : 0,
      };
    });

    // labels
    let labels = xlsx.utils.sheet_to_json(labelsSheet, { defval: '' });
    labels = labels.map((d) => {
      if (d.Type === 'ellipse' || d.Type === 'square') {
        return {
          id: `${i++}.${graphId}`,
          name: d.Name,
          type: d.Type,
          color: d.Color,
          size: d.Size,
          import: true,
        };
      }
      return {
        id: `${i++}.${graphId}`,
        name: d.Name,
        type: d.Type,
        color: d.Color,
        d: d.Size.split('|').map((i) => i.split(',').map((n) => +n)),
        import: true,
      };
    });

    const warnings = [];
    links = links.filter((l, i) => {
      if (!nodes.some((n) => n.id === l.source)) {
        warnings.push({
          name: l.source,
          message: 'Source is missing',
          line: i,
          fileLine: i + 2,
        });
        return false;
      }
      if (!nodes.some((n) => n.id === l.target)) {
        warnings.push({
          name: l.target,
          message: 'Target is missing',
          line: i,
          fileLine: i + 2,
        });
        return false;
      }
      return true;
    });
    return {
      nodes,
      links,
      warnings,
      labels,
    };
  }

  static async GraphFromXlsx(file, graphId) {
    const workBook = xlsx.read(file);
    const nodesIdRelation = {};
    // const workBook = _.isBuffer(file) ? xlsx.read(file) : xlsx.readFile(file);
    const {
      Nodes: nodesSheet,
      Links: linksSheet,
      Edges: linksSheetAlt,
      Labels: labelsSheet,
    } = workBook.Sheets;

    let nodes = xlsx.utils.sheet_to_json(nodesSheet, { defval: '' });
    nodes = nodes.map((d) => {
      const id = `${_.uniqueId()}.${graphId}`;
      nodesIdRelation[d.Id] = id;
      nodesIdRelation[d.Name] = id;
      let customFields;
      try {
        console.log(d);
        customFields = JSON.parse(d.Tabs) || [];
      } catch (e) {
        customFields = [];
      }
      return {
        id,
        name: d.Name,
        type: d.Type,
        description: d.Description,
        nodeType: d['Node Type'] || d.NodeType,
        icon: d.Icon || '',
        link: d.Link || d.Reference,
        keywords: (d.Keywords || '').split(', ').map((k) => k.trim()).filter((k) => k),
        // location: JSON.parse(d.Location)  || [],
        location: d.Location || undefined,
        fx: +d.Fx || 0,
        fy: +d.Fy || 0,
        customFields,
        d: d.D ? d.D.split('|').map((p) => p.split(',').map((n) => +n)) : undefined,
      };
    }).filter((d) => d.name);

    nodes = await Promise.object(nodes, 'icon', 20);
    nodes = await Promise.object(nodes, 'description', 20);

    // links
    let links = xlsx.utils.sheet_to_json(linksSheet);
    if (_.isEmpty(links)) {
      links = xlsx.utils.sheet_to_json(linksSheetAlt);
    }

    links = links.map((d) => {
      const source = d.Source || d['From Name'];
      const target = d.Target || d['To Name'];
      return {
        type: d.Type || d.Edge,
        source: nodesIdRelation[source],
        target: nodesIdRelation[target],
        value: d.Value || d.Weight,
        linkType: d['Link Type'] || d.LinkType,
        direction: d.direction ? 1 : 0,
      };
    });
    // labels
    let labels = xlsx.utils.sheet_to_json(labelsSheet, { defval: '' });
    labels = labels.map((d) => {
      if (d.Size.includes('width')) {
        return {
          name: d.Name,
          color: d.Color,
          type: d.Type,
          size: JSON.parse(d.Size),
        };
      }
      return {
        name: d.Name,
        color: d.Color,
        d: d.Size.split('|').map((i) => i.split(',').map((n) => +n)),
      };
    });

    const warnings = [];
    links = links.filter((l, i) => {
      if (!nodes.some((n) => n.id === l.source)) {
        warnings.push({
          name: l.source,
          message: 'Source is missing',
          line: i,
          fileLine: i + 2,
        });
        return false;
      }
      if (!nodes.some((n) => n.id === l.target)) {
        warnings.push({
          name: l.target,
          message: 'Target is missing',
          line: i,
          fileLine: i + 2,
        });
        return false;
      }
      return true;
    });
    return {
      nodes,
      links,
      warnings,
      labels,
    };
  }

  static graphToXlsx(nodes, links, labels = [], customFields = {}) {
    const workBook = xlsx.utils.book_new();

    // nodes
    const nodesArr = nodes.map((d) => Csv.nodesObjToArray(d));
    nodesArr.unshift(Csv.nodesHeader());

    const nodesWorkSheet = xlsx.utils.aoa_to_sheet(nodesArr);
    xlsx.utils.book_append_sheet(workBook, nodesWorkSheet, 'Nodes');

    // links
    const linksArr = links.map((d) => Csv.linksObjToArray(d));
    linksArr.unshift(Csv.linksHeader());

    const linksWorkSheet = xlsx.utils.aoa_to_sheet(linksArr);
    xlsx.utils.book_append_sheet(workBook, linksWorkSheet, 'Links');

    // labels
    const labelsArr = labels.map((d) => Csv.labelsObjToArray(d));
    labelsArr.unshift(Csv.labelsHeader());
    const labelsWorkSheet = xlsx.utils.aoa_to_sheet(labelsArr);
    xlsx.utils.book_append_sheet(workBook, labelsWorkSheet, 'Labels');

    // customFields
    // const customFieldsArr = [];
    // _.map(customFields, (d, type) => {
    //   const arr = Csv.customFieldsObjToArray(d, type);
    //   customFieldsArr.push(...arr);
    // });
    // const customFieldsWorkSheet = xlsx.utils.aoa_to_sheet(customFieldsArr);
    // Csv.customFieldsWithStyle(customFieldsWorkSheet);
    // xlsx.utils.book_append_sheet(workBook, customFieldsWorkSheet, 'Tabs');

    const data = xlsxStyle.write(workBook, {
      bookType: 'xlsx',
      type: 'buffer',
    });
    return data;
  }

  static async screenshot(url, type = 'buffer', options = {}) {
    const file = Directories.TEMP_DIR(`screenshot-${uuidv4()}.jpg`);
    await captureWebsite.file(url, file, {
      quality: 0.9,
      type: 'jpeg',
      scaleFactor: 0.5,
      ...options,
      // userAgent: req.headers['user-agent'],
      launchOptions: {
        args: [
          '--no-sandbox',
          '--disable-setuid-sandbox',
        ],
      },
    });
    if (type === 'file') {
      return file;
    }
    const buffer = fs.readFileSync(file, type);
    fs.unlinkSync(file);
    return buffer;
  }
}

export default Converter;
