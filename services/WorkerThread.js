import { workerData, isMainThread, Worker } from 'worker_threads';
import Utils from './Utils';
import Socket from './Socket';

class WorkerThread {
  static path = __filename;

  static run() {
    const { call } = workerData;
    if (WorkerThread[call]) {
      WorkerThread[call]();
    }
  }

  static call = (method, props = {}, events = {}) => {
    const worker = new Worker(__filename, {
      workerData: {
        call: method,
        props,
      },
    });
    worker.on('exit', (code) => {
      if (events.exit) {
        events.exit(code);
      }
      if (code !== 0) {
        console.error(`Worker stopped with exit code ${code}`);
      }
    });
    return worker;
  };

  static generateThumbnail = async () => {
    const { svg, thumbnail, size } = workerData.props;
    await Utils.generateThumbnail(svg, thumbnail, size);
  };
}

if (isMainThread) {
  WorkerThread.run = undefined;
} else {
  WorkerThread.run();
}

export default WorkerThread;
