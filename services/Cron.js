import { CronJob } from 'cron';
import fs from 'fs';
import moment from 'moment';
import path from 'path';
import Utils from './Utils';

class Cron {
  constructor() {
    // every hour
    new CronJob('0 0 */1 * * *', this.deleteTempDirectories, null, true);
  }

  deleteTempDirectories = () => {
    const date = moment.unix() / 60 / 60;
    const tempDir = path.join(__dirname, '../public/temp');
    if (fs.existsSync(tempDir)) {
      fs.readdirSync(tempDir).forEach((dir) => {
        if (+dir + 24 < date) {
          Utils.unlinkDirSync(path.join(tempDir, dir));
        }
      });
    }
  };
}

export default Cron;
