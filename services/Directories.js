import fs from 'fs';
import path from 'path';
import moment from 'moment';

class Directories {
  static dirToUri(dir) {
    return dir.replace(path.join(__dirname, '..'), global.uri);
  }

  static uriToDir(url) {
    if (!url || !url.startsWith(global.uri)) {
      return url;
    }
    const imgPath = url.replace(global.uri, '');
    return path.join(__dirname, '..', imgPath);
  }

  static ICONS_DIR(...params) {
    return path.join(__dirname, '../public/uploads/icons', ...params);
  }

  static THUMBNAILS_DIR(...params) {
    return path.join(__dirname, '../public/uploads/thumbnails', ...params);
  }

  static FILES_DIR(...params) {
    return path.join(__dirname, '../public/uploads/files', ...params);
  }

  static TEMP_DIR(...params) {
    return path.join(__dirname, '../temp', ...params);
  }

  static PUBLIC_TEMP_DIR(...params) {
    const date = (new Date().getTime() / 1000 / 60 / 60).toFixed();
    const dir = path.join(__dirname, '../public/temp', date);
    if (!fs.existsSync(dir)) {
      fs.mkdirSync(dir, { recursive: true });
    }
    return path.join(dir, ...params);
  }

  static PUBLIC_DIR(...params) {
    return path.join(__dirname, '../public', ...params);
  }
}

export default Directories;
