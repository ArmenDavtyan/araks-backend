import _ from 'lodash';

class Csv {
  static parse(strData) {
    const strDelimiter = ',';
    // Create a regular expression to parse the CSV values.
    const objPattern = new RegExp(
      `(\\${strDelimiter}|\\r?\\n|\\r|^)(?:"([^"]*(?:""[^"]*)*)"|([^"\\\\${strDelimiter}\\\\r\\\\n]*))`,
      'gi',
    );

    const arrData = [[]];

    let arrMatches = null;

    // eslint-disable-next-line no-cond-assign
    while (arrMatches = objPattern.exec(strData)) {
      const strMatchedDelimiter = arrMatches[1];
      if (strMatchedDelimiter.length && strMatchedDelimiter !== strDelimiter) {
        arrData.push([]);
      }

      let strMatchedValue;

      if (arrMatches[2]) {
        strMatchedValue = arrMatches[2].replace(
          new RegExp('""', 'g'),
          '"',
        );
      } else {
        // eslint-disable-next-line prefer-destructuring
        strMatchedValue = arrMatches[3];
      }

      arrData[arrData.length - 1].push(strMatchedValue);
    }
    const header = arrData.shift();
    const data = [];
    arrData.forEach((row) => {
      data.push({});
      row.forEach((val, i) => {
        const key = header[i].toLowerCase();
        data[data.length - 1][key] = val;
      });
    });
    return data;
  }

  static stringify(arr) {
    return arr.map((v) => v.map((d) => (
      `"${d.toString().replace(/"/g, '\\"')}"`
    )).join(',')).join('\n');
  }

  static nodesObjToArray(d) {
    return [
      d.type, d.name, d.id, d.description, d.nodeType, d.icon, d.link, d.keywords.join(', '),
      d.location, d.color, d.fx, d.fy,
      d.d ? d.d.map((c) => c.join(',')).join('|') : '',
      d.scale ? d.scale.join(',') : '',
      JSON.stringify(d.customFields),
    ];
  }

  static linksObjToArray(d) {
    return [
      d.type, d.source, d.target, d.value, d.linkType, (d.direction ? 1 : 0), d.color,
    ];
  }

  static labelsObjToArray(d) {
    if (d.type === 'ellipse' || d.type === 'square') {
      return [
        d.id, d.type, d.name, d.color, JSON.stringify(d.size),
      ];
    }

    return [
      d.id, d.type, d.name, d.color, d.d.map((c) => c.join(',')).join('|'),
    ];
  }

  static customFieldsObjToArray(d, type) {
    const tabs = _.map(d, (val, tab) => {
      if (val.subtitle) {
        return `${tab} (${val.subtitle})`;
      }
      return tab;
    });
    const arr = [
      ['Type', 'Node', ...tabs],
    ];
    let nodesArr = [];
    _.forEach(d, (item) => {
      _.forEach(item.values, (value, node) => {
        nodesArr.push(node);
      });
    });
    nodesArr = _.uniq(nodesArr);
    nodesArr.forEach((node) => {
      const data = [type, node];
      _.forEach(d, (item) => {
        data.push(item.values[node] || '');
      });
      arr.push(data);
    });
    arr.push(['']);
    return arr;
  }

  static nodesHeader() {
    return ['Type', 'Name', 'Id', 'Description', 'Node Type', 'Icon', 'Link',
      'Keywords', 'Location', 'Color', 'Fx', 'Fy', 'D', 'Scale', 'Tabs'];
  }

  static linksHeader() {
    return ['Type', 'Source', 'Target', 'Value', 'Link Type', 'Direction', 'Color'];
  }

  static labelsHeader() {
    return ['Id', 'Type', 'Name', 'Color', 'Size'];
  }

  static customFieldsWithStyle(customFieldsWorkSheet) {
    let headerRow = 1;
    _.forEach(customFieldsWorkSheet, (d, pos) => {
      const [, col, row] = pos.match(/^([A-Z]+)(\d+)$/) || [];
      if (col === 'A' && customFieldsWorkSheet[pos].v === '') {
        headerRow = +row + 1;
      }
      if (+row === headerRow) {
        customFieldsWorkSheet[pos].s = {
          fill: {
            patternType: 'solid',
            fgColor: { rgb: '00DDDDDD' },
            bgColor: { rgb: '00DDDDDD' },
          },
        };
      }
    });
  }
}

export default Csv;
