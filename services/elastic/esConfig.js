'use strict'

const { Client } = require('@elastic/elasticsearch');
const config = require('./config');
const client = new Client({ node: config.es_host});

module.exports.esClient= client;
// run();
// async function run() {
//   try {
//     await client.indices.create({
//       index: 'tweets1',
     
//       body: {
//         mappings: {
//           properties: {
//             id: { type: 'integer' },
//             text: { type: 'text' },
//             user: { type: 'text' },
//             time: { type: 'date' },
//           },
//         },
//       },
//     }, { ignore: [400] });
//     const dataset = [{
//       id: 1,
//       text: 'If I fall, don\'t bring me back.',
//       user: 'jon',
//       date: new Date(),
//     }, {
//       id: 2,
//       text: 'Winter is coming',
//       user: 'ned',
//       date: new Date(),
//     }, {
//       id: 3,
//       text: 'A Lannister always pays his debts.',
//       user: 'tyrion',
//       date: new Date(),
//     }, {
//       id: 4,
//       text: 'I am the blood of the dragon.',
//       user: 'daenerys',
//       date: new Date(),
//     }, {
//       id: 5, // change this value to a string to see the bulk response with errors
//       text: 'A girl is Arya Stark of Winterfell. And I\'m going home.',
//       user: 'arya',
//       date: new Date(),
//     }];
//     const body = dataset.flatMap((doc) => [{ index: { _index: 'tweets1' } }, doc]);
//     const { body: bulkResponse } = await client.bulk({ refresh: true, body });
//     // console.log(bulkResponse);
//     const { body: data } = await client.search({ index: 'tweets1' });
//     console.log(data.hits, 'ssssssssss');
//   } catch (e) {
//     console.log(e);
//   }
//   process.exit(0);
// }