const fs = require('fs');
const esconfig = require('./esConfig');
const client = esconfig.esClient;
 const config = require('./config');

const index= config.es_index;
const type = config.es_type;

 
async function createGraphHistoryMapping (index, type) {
    // const graphsHistorySchema  =   {
    //     "graphsId": {
    //       "type": "integer" //"long"
    //     },
    //     "eventType": {
    //       "type": "text"
    //     },
    //     "userId": {
    //       "type": "integer", // "long"
    //     },
    //     "eventDate": {
    //       "type": "date"
    //     },
    //     "groupId": {
    //       "type": "integer", 
    //     },  
    //   }
  // return client.indices.putMapping({index, type, body:{properties:graphsHistorySchema}});
}
module.exports = {
// async resetIndex(){
//  if (client.indices.exists({ index })) {
//       client.indices.delete({ index });
//    }
//  client.indices.create({ index });
//  createGraphHistoryMapping(client, index, type); 
// }
async createIndex(){ 
   //client.indices.delete({ index }); 
    await client.indices.create({
        index: index,       
        type: type,       
        body: {
          mappings: {
            properties: {
              "_id" : {
                  "path" : "graphsId"
              },
              "graphsId": {
                "type": "integer" //"long"
              }, 
              "eventType": {
                "type": "text"
              },
              "userId": {
                "type": "integer", // "long"
              },
              "data": {
                "type": "text", // "long"
              },
              "eventDate": {
                "type": "date", 
    
              },
              "groupId": {
                "type": "integer", 
              },  
              "dataDetails": {
                "type": "text", 
              },  
            },
         },
         
         "settings" : {
          "number_of_shards" : 1,
          "number_of_replicas" : 0
      }
      },
   }, { ignore: [400] });
}

};



