import HttpErrors from 'http-errors';
import { AccessTokens } from '../models';

async function accessTokenAuth(req, res, next) {
  try {
    const { method } = req;
    const reqData = { ...req.params, ...req.body, ...req.query };
    const { token, userId } = reqData;
    const graphId = reqData.graphId || reqData.id;
    if (method === 'OPTIONS') {
      next();
      return;
    }
    const accessToken = await AccessTokens.findOne({
      userId,
      graphId,
      token,
    });
    if (!accessToken) {
      throw new HttpErrors(403);
    }

    req.userId = userId;
    global.userId = userId;

    next();
  } catch (e) {
    next(e);
  }
}

export default accessTokenAuth;
