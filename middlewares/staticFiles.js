import express from 'express';
import path from 'path';

export default function staticFiles() {
  return express.static(path.join(__dirname, '../public'), {
    maxAge: '1d',
    setHeaders: (res, filePath) => {
      if (filePath.endsWith('.png.large')) {
        res.type('image/jpeg');
      }
    },
  });
}
