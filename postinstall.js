import fs from 'fs';
import path from 'path';

const file = path.join(__dirname, '/node_modules/svg-to-pdfkit/source.js');
let data = fs.readFileSync(file, 'utf8');

data = data.replace('pathObject.getPointAtLength(charMidX * pathScale)', 'pathObject.getPointAtLength(charMidX * pathScale) || [0,0,0]');

fs.writeFileSync(file, data);
