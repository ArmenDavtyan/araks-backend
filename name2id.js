import _ from 'lodash';
import { literal } from 'sequelize';
import { Graphs } from './models';

async function main() {
  const graphs = await Graphs.findAll({
    where: {
      $and: literal('nodes->"$[*].id" is null'),
    },
  });
  for (const graph of graphs) {
    const { id: graphId } = graph;
    let i = 1;
    const idNameRelation = {};
    graph.nodes = _.cloneDeep(graph.nodes).map((n) => {
      n.id = `${i}.${graphId}`;
      idNameRelation[n.name] = n.id;
      i += 1;
      return n;
    });
    graph.links = _.cloneDeep(graph.links).map((l) => {
      l.source = idNameRelation[l.source];
      l.target = idNameRelation[l.target];
      return l;
    });
    graph.labels = _.cloneDeep(graph.labels).map((l) => {
      l.id = `${i}.${graphId}`;
      i += 1;
      return l;
    });
    await graph.save({ silent: true });
    console.log(graphId, '\n');
  }
  process.exit(0);
}

main();
