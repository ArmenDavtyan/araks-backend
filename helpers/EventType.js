
//Graphs  history event type list
  // node event 
const NodeCreate = 'node.create';
const NodeUpdate = 'node.update';
const NodeDelete = 'node.delete';
const updatePositions = 'node.updatePositions';
const NodeUpdateCustomFields = 'node.updateCustomFields';
const NodeUpdateCustomFieldsView = 'node.updateCustomFieldsView';
  //link event
const LinkCreate = 'link.create';
const LinkUpdate = 'link.update';
const LinkDelete = 'link.delete';
  //lable event
const LabelCreate = 'label.create';
const LabelUpdate = 'label.update';
const LabelDelete = 'label.delete';

export default {
    NodeCreate,
    NodeUpdate,
    NodeDelete,
    updatePositions,
    NodeUpdateCustomFields,
    NodeUpdateCustomFieldsView,
    LinkCreate,
    LinkUpdate,
    LinkDelete,
    LabelCreate,
    LabelUpdate,
    LabelDelete,
  
    // event name
    getTypeShortName(name){

       switch(name){
        case  NodeCreate : 
           return 'created node' ; 
        case  NodeUpdate : 
          return  'edited node' ; 
        case  NodeDelete : 
          return  'deleted node' ; 
        case  NodeUpdateCustomFields : 
          return  'Node update tabs' ; 
        case  NodeUpdateCustomFieldsView : 
          return  'Node tabs data view' ; 
        case  updatePositions : 
          return  'repositioned node' ; 
        case  LinkCreate : 
          return  'Link create' ; 
        case  LinkUpdate : 
          return  'Link update' ; 
        case  LinkDelete : 
          return  'Link delete' ; 
        case  LabelCreate : 
          return  'Label create' ; 
        case  LabelUpdate : 
          return  'Label update' ; 
        case  LabelDelete : 
          return  'Label delete' ; 
        default:
          return null
  
      } 
    },
    getTypeName (name, data = []) {
      console.log(data.nodeName, 'datadatadata', name);
      switch(name){
        case  NodeCreate : 
           return `created node ${data.nodeName[0]}` ; 
        case  NodeUpdate : 
          return  `edited node  ${data.nodeName[1]}` ; 
        case  NodeDelete : 
          return  `deleted node  ${data.nodeName[0]}` ; 
        case  NodeUpdateCustomFields : 
          return  'Node update tabs' ; 
        case  NodeUpdateCustomFieldsView : 
          return  'Node tabs data view' ; 
        case  updatePositions : 
          return  `repositioned node ${data.nodeName[0]}` ; 
        case  LinkCreate : 
          return  'Link create' ; 
        case  LinkUpdate : 
          return  'Link update' ; 
        case  LinkDelete : 
          return  'Link delete' ; 
        case  LabelCreate : 
          return  'Label create' ; 
        case  LabelUpdate : 
          return  'Label update' ; 
        case  LabelDelete : 
          return  'Label delete' ; 
        default:
          return null
  
      }
    }
  }; 